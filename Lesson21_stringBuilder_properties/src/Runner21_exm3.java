import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

public class Runner21_exm3 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/it_class.properties";

        Properties properties = getPropirties(FILE_NAME);

        String value = properties.getProperty("it.login");
        System.out.println("value:" + value);

        properties.propertyNames();

        Enumeration<?> keys = properties.propertyNames();

        while (keys.hasMoreElements()) {
            //String key = (String) keys.nextElement();
            String key = keys.nextElement().toString();
            System.out.println("key:" + key);
            value = properties.getProperty(key);
            System.out.println("value:" + value);
        }

    }

    private static Properties getPropirties(final String fileName) {
        Properties properties = new Properties();
        try {
            properties.load(new FileReader((fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}
