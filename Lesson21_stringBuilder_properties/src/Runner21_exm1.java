import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Runner21_exm1 {
    public static void main(String[] args) {
        //Анализ подстрок с помощью регулярных выражений
        String str = "Java Web JavaScript and JavaCore";
        String regex = "Java\\w*";

        //Класс Pattern описывает объектно наше регулярное выражение
        //метод compile() возвращает объект Pattern на основе переданной строки
        Pattern pattern = Pattern.compile(regex);

        //Класс Mather позволяет анализировать подстроки некоторой строки на соответствие
        //регулярному выражению
        Matcher matcher = pattern.matcher(str);

        //Метод matches() проверяет на соответствие регулярному выражению объекта
        //Pattern переданной строки str
        boolean result = matcher.matches();

        System.out.println("matches: " + result);

        //Метод find() ищет подстроки, которые удовлетворяют регулярному выражению
        //метод имеет указатель, который после нахождения остается на индексе после найденного объекта,
        //поэтому, когда вся строка проанализирована указатель всегда будет находится в конце строки
        //и будет выдвать ошибку
        result = matcher.find();
        int start = matcher.start();
        int end = matcher.end();
        //Метод group() возвращает найденную подстроку в случае, если find()
        //вернул true
        String substr = matcher.group();
        System.out.println("find1: " + result);
        System.out.printf("start: %d, end: %d, substr: %s\n", start, end, substr);

        result = matcher.find();
        System.out.println("find2: " + result);

        result = matcher.find();
        start = matcher.start();
        end = matcher.end();
        substr = matcher.group();
        System.out.println("find3: " + result);
        System.out.printf("start: %d, end: %d, substr: %s\n", start, end, substr);

        /*
        result = matcher.find();
        start = matcher.start();
        end = matcher.end();
        substr = matcher.group();
        System.out.println("find4: " + result);
        System.out.printf("start: %d, end: %d, substr: %s\n", start, end, substr);
        */

        //
        //Метод reset() сбрасывает matcher в начало строки
        matcher.reset();

        //Метод reset() заменяет анализируемую строку для matcher
        matcher.reset("My email: test@gmail.com and it_classpo51@mail.ru");

        regex = "(\\w+)@(\\w+)\\.(\\w{2,5})";
        matcher.usePattern(Pattern.compile(regex));

        while (matcher.find()) {
            substr = matcher.group();
            String name = matcher.group(1);
            String host = matcher.group(2);
            String domname = matcher.group(3);
            System.out.println("substr:" + substr);

            System.out.printf("name: %s, host: %s, domname: %s\n", name, host, domname);
        }

        matcher.reset("@it_classpo51@mail.ru");
        result = matcher.lookingAt();
        System.out.println("lookingAt: " + result);




    }
}
