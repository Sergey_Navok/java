public class Runner21_exm2 {
    public static void main(String[] args) {
        String str = "Hello!";
        //String -> byte[]

        //StringBuilder // String -> char[]
        //StringBuffer

        StringBuilder builder = new StringBuilder();
        System.out.println("builder: " + builder);

        builder.append("Hello from IT CLass");
        System.out.println("builder: " + builder);

        builder.insert(6, "JAVA");
        System.out.println("builder: " + builder);

        builder.delete(6, 10);
        System.out.println("builder: " + builder);

        String newStr = builder.toString();
    }
}
