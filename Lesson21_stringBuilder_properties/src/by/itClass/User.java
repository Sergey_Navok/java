package by.itClass;

public class User {
    String login;
    String password;

    @Override
    public String toString() {
        return  new StringBuilder()
                    .append(login)
                    .append(password)
                    .toString();

        /*
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
        */
    }
}
