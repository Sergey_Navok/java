import by.itClass.Segments;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Runner15_task5 {
    public static void main(String[] args) throws FileNotFoundException {
        Segments[] segments = new Segments[10];
        int i = 0;
        final String DELIMETER = ";";

        final String FILE_NAME = "src/itClass.txt";
        InputStream in = new FileInputStream(FILE_NAME);

        Scanner sc = new Scanner(in);

        while (sc.hasNextLine()) {
            String[] values = sc.nextLine().split(DELIMETER);
            int a = Integer.valueOf(values[0]);
            int b = Integer.valueOf(values[1]);
            Segments segment = new Segments(a, b);
            segments[i++] = segment;
        }

        System.out.println(Arrays.toString(segments));
        Arrays.sort(segments);
        System.out.println(Arrays.toString(segments));

        sc.close();
    }
}
