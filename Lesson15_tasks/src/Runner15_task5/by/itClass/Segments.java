package by.itClass;

public class Segments implements Comparable<Segments> {
    private int leftPoint;
    private int rightPoint;

    public Segments() {
    }

    public Segments(int leftPoint, int rightPoint) {
        this.leftPoint = leftPoint;
        this.rightPoint = rightPoint;
    }

    public int getLeftPoint() {
        return leftPoint;
    }

    public void setLeftPoint(int leftPoint) {
        this.leftPoint = leftPoint;
    }

    public int getRightPoint() {
        return rightPoint;
    }

    public void setRightPoint(int rightPoint) {
        this.rightPoint = rightPoint;
    }

    @Override
    public String toString() {
        return leftPoint + ";" + rightPoint;
    }

    @Override
    public int compareTo(Segments obj) {
        return (obj.leftPoint - obj.rightPoint) - (leftPoint - rightPoint);//По возрастанию
        //return (leftPoint - rightPoint) - (obj.leftPoint - obj.rightPoint);//По убыванию
    }
}
