import java.io.*;

public class Runner15_task1_exm1 {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/itClass.txt";

        InputStream reader = new FileInputStream(FILE_NAME);

        int ch;
        while ((ch = reader.read()) > -1) {
            System.out.print((char) ch);
        }
        reader.close();
    }
}