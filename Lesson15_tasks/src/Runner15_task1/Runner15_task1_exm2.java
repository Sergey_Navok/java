import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Runner15_task1_exm2 {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/itClass.txt";

        Reader reader = new FileReader(FILE_NAME);

        int ch;
        while ((ch = reader.read()) > -1) {
            System.out.print((char) ch);
        }
        reader.close();
    }
}
