import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Runner15_task1_exm3 {
    public static void main(String[] args) throws FileNotFoundException {
        final String FILE_NAME = "src/itClass.txt";

        Scanner sc = new Scanner(new FileReader(FILE_NAME));

        String str = sc.nextLine();
        System.out.println(str);
        sc.close();
    }
}
