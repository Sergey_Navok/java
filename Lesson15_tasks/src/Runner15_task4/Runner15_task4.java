import by.itClass.Student;

import java.io.*;

public class Runner15_task4 {
    public static void main(String[] args) throws IOException {
        Student student = new Student("Иван", "Иванов", 20, "юридический");
        System.out.println(student);

        final String FILE_NAME = "src/itClass.txt";

        OutputStream messageByte = new FileOutputStream(FILE_NAME, true);
        byte[] studentBytes = student.toString().getBytes();
        String message = "Байтовая запись в файл\n";
        messageByte.write(message.getBytes());
        messageByte.write(studentBytes);

        Writer messageSymbol = new FileWriter(FILE_NAME, true);
        messageSymbol.write("\nСимвольная запись в файл\n");
        messageSymbol.write(student.toString());

        messageByte.close();
        messageSymbol.close();

    }
}
