package by.itClass;

public class Student {
    private String firstName;
    private String lastName;
    private int age;
    private String faculty;

    public Student() {
    }

    public Student(String firstName, String lastName, int age, String faculty) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.faculty = faculty;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return firstName + ";" + lastName + ";" + age + ";" + faculty;
    }
}
