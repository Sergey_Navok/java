import java.io.*;
import java.util.Arrays;

public class Runner15_task2 {
    public static void main(String[] args) throws IOException {
        int[] arr = new int[15];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * (10 - 1) + 1);
        }

        System.out.println(Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println(Arrays.toString(arr));

        final String FILE_NAME_SYMBOL = "src/itClass1.txt";
        Writer writer = new FileWriter(FILE_NAME_SYMBOL);
        writer.write(Arrays.toString(arr));
        writer.close();

        final String FILE_NAME_BYTE = "src/itClass2.txt";
        OutputStream outFile = new FileOutputStream(FILE_NAME_BYTE);
        outFile.write(Arrays.toString(arr).getBytes());
        outFile.close();
        
    }
}
