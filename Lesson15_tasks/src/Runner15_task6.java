import by.itClass.Purchase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

public class Runner15_task6 {
    public static void main(String[] args) throws FileNotFoundException {
        Purchase[] purchases = new Purchase[PURCHASES_NUMBER];
        int i = 0;

        final String FILE_NAME = "src/itClass.txt";
        InputStream in = new FileInputStream(FILE_NAME);

        Scanner sc = new Scanner(in);

        while (sc.hasNextLine()) {
            /*String[] values = sc.nextLine().split(DELIMETER);
            int a = Integer.valueOf(values[0]);
            int b = Integer.valueOf(values[1]);*/

            Purchase purchase = new Purchase(a, b);
            purchases[i++] = purchase;
        }

        System.out.println(Arrays.toString(purchases));


        sc.close();
    }
}
