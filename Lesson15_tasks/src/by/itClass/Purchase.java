package by.itClass;

import java.time.DayOfWeek;

public class Purchase implements Comparable<Purchase> {
    private String name;
    private int price;
    private int quantity;
    private DayOfWeek dayOfWeek;

    public Purchase() {
    }

    public Purchase(int quantity, DayOfWeek dayOfWeek) {
        this.quantity = quantity;
        this.dayOfWeek = dayOfWeek;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getCost() {
        return price * quantity;
    }

    @Override
    public String toString() {
        return name + ";" + price + ";" + quantity + ";" + dayOfWeek;
    }

    @Override
    public int compareTo(Purchase purchase) {
        return (int) (quantity - purchase.quantity);
    }
}
