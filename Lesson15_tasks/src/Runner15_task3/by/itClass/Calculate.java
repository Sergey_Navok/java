package by.itClass;

public class Calculate {
    public static int getResult(char operation, int a, int b) {
        int result;
        switch (operation) {
            case ('+'):
                result = a + b;
                return result;

            case ('-'):
                result = a - b;
                return result;

            case ('/'):
                result = a / b;
                return result;

            case ('*'):
                result = a * b;
                return result;

            default:
                return 0;
        }
    }
}
