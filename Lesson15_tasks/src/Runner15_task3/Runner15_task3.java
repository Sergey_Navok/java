import by.itClass.Calculate;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Runner15_task3 {
    public static void main(String[] args) throws FileNotFoundException {
        final String FILE_NAME = "src/itClass.txt";
        String[] strArr = {"sum", "diff", "div", "mult"};
        char[] charArr = {'+', '-', '/', '*'};

        Scanner sc = new Scanner(new FileReader(FILE_NAME));

        while (sc.hasNextLine()) {
            String operation = sc.next();

            char operationChar = '0';
            for (int i = 0; i < strArr.length; i++) {
                if (operation.equals(strArr[i])) {
                    operationChar = charArr[i];
                    break;
                }
            }

            int a = sc.nextInt();
            int b = sc.nextInt();
            int result = Calculate.getResult(operationChar, a, b);
            
            System.out.printf("%d %c %d = %d\n", a, operationChar, b, result);
        }
        sc.close();
    }
}
