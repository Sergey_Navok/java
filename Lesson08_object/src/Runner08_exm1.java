import by.itClass.User;

public class Runner08_exm1 {
    public static void main(String[] args) throws ClassNotFoundException {
        User user1 = new User("admin1900", "qw123");
        User user2 = new User("admin1900", "qw123");
        //В методе println() неявно вызывается метод toString() объекта
        System.out.println(user1);
        //Представление объекта в виде строки
        String objStr = user1.toString();

        boolean isEqual = user1.equals(user2);
        System.out.println("isEquals:" + isEqual);

        System.out.println("hash1:" + user1.hashCode());
        System.out.println("hash2:" + user2.hashCode());

        //Для работы с потоками используются методы у класса
        //Object - notify, wait

        //Метод getClass() возвращает объект класса Class
        //описывающий объекта класс, которому принадлежит
        //исходный наш объект
        Class clUser1 = user1.getClass();
        Class clUser2 = User.class;
        Class clUser3 = Class.forName("by.itClass.User");

        System.out.println(clUser1 == clUser2);
        System.out.println(clUser1 == clUser3);
    }
}
