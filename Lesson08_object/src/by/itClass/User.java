package by.itClass;

import java.util.Objects;

//Если явно не указывать extends, то лю,ой класс
//всегда наследует по умолчанию от Object
public class User {
    private String login;
    private String password;
    private int id;

    public User() {
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*@Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }*/

    @Override
    public boolean equals(Object obj) {
        //Сравнивает ссылки объекта на котором вызвали метод
        //с объектом, который передали в метод
        //Если ссылки равны - то очевидно работаем с одной
        //областью памяти, т.е. это один и тот же объект
        if (this == obj)
            return true;
        //Если переданный в метод объект null, то не имеет
        //смысла дальнейшее сравнение
        if (obj == null)
            return false;
        //Получаем классы соответствующих обектов и смотрим
        //равны ли эти классы, для дальнейшего сравнения
        //оба объекта должны принадлежать одному классу
        if (getClass() != obj.getClass())
            return false;
        User user = (User) obj;
        //Сравниваем соответствующие поля. При этом для ссылочных
        //типов полей вызываем реализацию метода equals() их класса,
        //определенную в их классе
        boolean result = (id == user.id && login.equals(user.login));
        return result;
    }

    //Понятие hashCode представляет собой int представление адреса
    //объекта в памяти, которая возвращает начальная нативная реализация
    //метода hashCode() определенная в классе Object
    //Этот метод желательно переопределять, если планируется работа с
    //hash ориентированными структурами данных, такие как hashTable,
    //hashMap и т.д. во избежание ситуации коллизии (разные объекты имеют
    //одинаковый hashCode)
    @Override
    public int hashCode() {
        int result = login.hashCode();
        result = 31 * result + id;
        return result;
    }

    //Метод преставляет объект в виде строки
    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }




}
