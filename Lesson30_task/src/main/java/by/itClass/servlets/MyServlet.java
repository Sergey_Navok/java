package by.itClass.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet(name = "MyServlet", urlPatterns = "/post")
public class MyServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String[]> params = request.getParameterMap();
        PrintWriter writer = response.getWriter();
        String is = "=";
        String paragraph = "\n";

        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            String key = entry.getKey();
            String value = Arrays.toString(entry.getValue());

            writer.append(key + is + value);
            writer.append(paragraph);
        }

        if (writer != null) {
            writer.close();
        }
    }
}