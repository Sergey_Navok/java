public class Runner01_task3 {
    public static void main(String[] args) {
        /*
        В настройках конфигуатора в качестве аргумента была передана строка,
        сама строка Program arguments выглядит так:
        "Hello Alex from Java courses" "Hello Max from Java courses"
         */
        System.out.println(args[0]);
        System.out.println(args[1]);
        System.out.println(args[2]);
        System.out.printf(args[0]);
        System.out.printf(args[1]);
        System.out.printf(args[2]);
    }
}
