public class Runner01_task2 {
    public static void main(String[] args) {
        /*
        В настройках конфигуатора в качестве аргумента была передана строка,
        сама строка Program arguments выглядит так:
        Java courses in ITClass
         */
        System.out.println(args[0] + " " + args[1] + " " + args[2] + " " + args[3]);
        System.out.println(args[3] + " " + args[2] + " " + args[1] + " " + args[0]);
    }
}
