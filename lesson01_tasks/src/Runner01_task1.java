public class Runner01_task1 {
    public static void main(String[] args) {
        /*
        В настройках конфигуатора в качестве аргумента были переданы две строки,
        сама строка Program arguments выглядит так:
        "Hello Alex from Java courses" "Hello Max from Java courses"
         */
        System.out.println(args[0]);
        System.out.println(args[1]);
    }
}
