public class Runner01_task5 {
    public static void main(String[] args) {
        int z = 0;
        int x1 = 1;
        int y1 = 2;

        z = 7 * x1 * x1 + 3 * y1 * y1 - ((3 * x1) * (2 * y1)) + 6 * x1 - 8 * y1;
        System.out.printf("Variables: x1 = %d, y1 = %d", x1, y1);
        System.out.println("\nPrintln method\nz = 7 * " + x1 + "^2 + 3 * " + y1 + "^2 - 3 * " + x1 + " * 2 * " + y1 + " + 6 * " + x1 + " - 8 * " + y1 + " = " + z);
        System.out.printf("Printf method\nz = 7 * %d^2 + 3 * %d^2 - 3 * %d * 2 * %d + 6 * %d - 8 * %d = %d", x1, y1, x1, y1, x1, y1, z);

        int x2 = 3;
        int y2 = 4;
        z = 7 * x2 * x2 + 3 * y2 * y2 - ((3 * x2) * (2 * y2)) + 6 * x2 - 8 * y2;
        System.out.printf("\n\nVariables: x1 = %d, y1 = %d", x2, y2);
        System.out.println("\nPrintln method\nz = 7 * " + x2 + "^2 + 3 * " + y2 + "^2 - 3 * " + x2 + " * 2 * " + y2 + " + 6 * " + x2 + " - 8 * " + y2 + " = " + z);
        System.out.printf("Printf method\nz = 7 * %d^2 + 3 * %d^2 - 3 * %d * 2 * %d + 6 * %d - 8 * %d = %d", x2, y2, x2, y2, x2, y2, z);

        int x3 = 5;
        int y3 = 6;
        z = 7 * x3 * x3 + 3 * y3 * y3 - ((3 * x3) * (2 * y3)) + 6 * x3 - 8 * y3;
        System.out.printf("\n\nVariables: x1 = %d, y1 = %d", x3, y3);
        System.out.println("\nPrintln method\nz = 7 * " + x3 + "^2 + 3 * " + y3 + "^2 - 3 * " + x3 + " * 2 * " + y3 + " + 6 * " + x3 + " - 8 * " + y3 + " = " + z);
        System.out.printf("Printf method\nz = 7 * %d^2 + 3 * %d^2 - 3 * %d * 2 * %d + 6 * %d - 8 * %d = %d", x3, y3, x3, y3, x3, y3, z);
    }
}
