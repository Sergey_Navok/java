public class Runner01_task6 {
    public static void main(String[] args) {
        int ageTanya = 22;
        int ageMisha = 35;
        double avg = (double) (ageTanya + ageMisha) / 2;
        double difference = 0;

        System.out.println("Средний возраст: " + avg + " лет");

        if (ageTanya > avg) {
            difference = ageTanya - avg;
            System.out.println("Возраст Тани больше среднего возраста на: " + difference + " лет.");
        } else {
            difference = avg - ageTanya;
            System.out.println("Возраст Тани меньше среднего возраста на: " + difference + " лет.");
        }

        if (ageMisha > avg) {
            difference = ageMisha - avg;
            System.out.println("Возраст Миши больше среднего возраста на: " + difference + " лет.");
        } else {
            difference = avg - ageMisha;
            System.out.println("Возраст Миши меньше среднего возраста на: " + difference + " лет.");
        }

    }
}
