import java.util.Scanner;

public class Runner01_task7 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Введите массу конфет в кг (граммы через запятую): ");
        double candy = in.nextDouble();

        System.out.print("Введите стоимость конфет в руб. (копейки через запятую): ");
        double candyCost = in.nextDouble();

        System.out.print("Введите массу печенья в кг (граммы через запятую): ");
        double coocies = in.nextDouble();

        System.out.print("Введите стоимость печенья в руб. (копейки через запятую): ");
        double coociesCost = in.nextDouble();

        System.out.print("Введите массу яблок в кг (граммы через запятую): ");
        double apple = in.nextDouble();

        System.out.print("Введите стоимость яблок в руб. (копейки через запятую): ");
        double appleCost = in.nextDouble();

        double purchase = candy * candyCost + coocies * coociesCost + apple * appleCost;

        System.out.printf("Сумма всех покупок составила: %.2f руб.", purchase);
        in.close();

    }
}
