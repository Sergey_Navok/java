import by.itClass.entity.Car;
import by.itClass.entity.Person;
import by.itClass.interfaces.Action;

public class Runner10_exm1 {
    public static void main(String[] args) {
        //Интерфейсы
        //1.8 ->

        Person person = new Person();
        System.out.println("person voice: " + person.getVoice());

        int a = Person.SERIAL_ID_1;
        int b = person.SERIAL_ID_2;
        int c = Action.SERIAL_ID_1;

        Car car = new Car();
        System.out.println("car voice: " + car.getVoice());

        //Нельзя создать объект интерфейса, но можно определить ссылочную переменную
        Action action  = null;
        action = person;

        action = car;

        Action[] actions1 = {person, car};

        for (int i = 0; i < actions1.length; i++) {
            System.out.println("voice " + actions1[i].getVoice());
        }

        car = (Car) action;
    }
}
