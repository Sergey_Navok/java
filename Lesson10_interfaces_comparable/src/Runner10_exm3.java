import by.itClass.comparators.SortedByLoginComparator;
import by.itClass.entity.User;

import java.util.Arrays;
import java.util.Comparator;

public class Runner10_exm3 {
    public static void main(String[] args) {
        User[] users = {new User (8, "Guili"),
                new User(3 , "moderator"),
                new User(10, "admin"),
                new User(1, "Kate")};

        //Если класс реализует интрефейс (в общем случае), то ссылочную
        //переменную правильно делать типа реализуемого интерфейса
        Comparator comparatorByLogin = new SortedByLoginComparator();

        System.out.println("Before sort: ");
        System.out.println(Arrays.toString(users));

        Arrays.sort(users, comparatorByLogin);

        System.out.println("After sort: ");
        System.out.println(Arrays.toString(users));
    }
}
