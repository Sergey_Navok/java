import by.itClass.entity.User;

import java.util.Arrays;

public class Runner10_exm2 {
    public static void main(String[] args) {
        User[] users = {new User (8),
                        new User(3),
                        new User(10),
                        new User(1)};

        int result = users[0].compareTo(users[1]);
        System.out.println("result: " + result);

        System.out.println("Before sort: ");
        System.out.println(Arrays.toString(users));

        Arrays.sort(users);

        System.out.println("After sort: ");
        System.out.println(Arrays.toString(users));
    }
}
