package by.itClass.comparators;

import by.itClass.entity.User;

import java.util.Comparator;

public class SortedByLoginComparator implements Comparator<User> {
    @Override
    public int compare(User user1, User user2) {
        //Равные объекты - возвращает 0
        //user1 > user2 - возвращает > 0
        //user1 < user2 - возвращает < 0
        String login1 = user1.getLogin();
        String login2 = user2.getLogin();

        return login1.compareTo(login2);
    }
}
