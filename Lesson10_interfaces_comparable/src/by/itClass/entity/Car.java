package by.itClass.entity;

import by.itClass.interfaces.Action;

public class Car implements Action {

    @Override
    public String getVoice() {
        return "Би-Би!";
    }

    @Override
    public String doSmt() {
        return "Метод интерфейса Car";
    }
}
