package by.itClass.entity;

//Copmarable задает естсесственную сортировку объектов
public class User implements Comparable<User> {
    private int id;
    private String login;

    public User() {
    }

    public User(int id) {
        this.id = id;
    }

    public User(int id, String login) {
        this.id = id;
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                '}';
    }

    @Override
    public int compareTo(User user) {
        return id - user.id;
    }

    //Равные объекты - возвращает 0
    //this > параметра - возвращает > 0
    //this < параметра - возвращает < 0
    /*@Override
    public int compareTo(Object user) {
        //this, user
        int result = 0;
        User us = (User) user;
        if (this.id < us.id) {
            result = 1;
        } else if (this.id > us.id) {
            result = -1;
        }
        return result;
        }*/
}
