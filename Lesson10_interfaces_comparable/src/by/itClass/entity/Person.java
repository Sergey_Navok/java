package by.itClass.entity;

import by.itClass.interfaces.Action;

public class Person extends Object implements Action, Cloneable {
    @Override
    public String getVoice() {
        return "Привет!";
    }

    @Override
    public String doSmt() {
        return "Метод интерфейса Person";
    }
}
