package by.itClass.abstracts;

public abstract class Figure {
    private String color;

    public Figure() {
    }

    public Figure(String color) {
        this.color = color;
    }

    public void doSmth() {
        System.out.println("doSmth");
    }

    public abstract double getPerimeter();
}
