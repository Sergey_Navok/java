import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Runner17_exm4 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass.txt";

        //try-catch-resources

        try (Reader reader1 = new FileReader(FILE_NAME);
             Reader reader2 = new FileReader(FILE_NAME);
             Reader reader3 = new FileReader(FILE_NAME)) {

            reader1.read();
            //Работа с ресурсами

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*mq throws E1

        try {
            E1
        } finally {k

        }*/


    }
}
