import by.itClass.Box;
import by.itClass.exceptions.IncorrectValueException;

public class Runner17_exm1 {
    public static void main(String[] args) {
        //Exception
        //throw new
        //throws - ключевое слово, позволяет отказаться от обработки ошибки
        //в текущей области видимости и передать ошибку выше

        //Создание своих exception

        Box box = new Box();
        try {
            box.setValue("Hello from IT CLASS!");
            System.out.println(box.getValue());
        } catch (IncorrectValueException e) {
            e.printStackTrace();
        }


        try {
            box.setValue(null);
        } catch (IncorrectValueException e) {
            System.err.println(e.getMessage());
            Throwable cause = e.getCause();
            cause.printStackTrace();
        }

        System.out.println("1");

    }
}
