import by.itClass.Box;
import by.itClass.exceptions.IncorrectValueException;

public class Runner17_exm2 {
    public static void main(String[] args) {
        Box box = new Box();

        try {
            String str = null;
            str.isEmpty();//NullPointerException -> null
            box.setValue("");//IncorrectValueException
        } catch (IncorrectValueException e) {
            //Обработка конкретно IncorrectValueException
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("Exception");
        }

        System.out.println("1");
    }
}
