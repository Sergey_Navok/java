package by.itClass;

import by.itClass.exceptions.IncorrectValueException;

public class Box {
    private String value;

    public Box() {
    }

    public String getValue() {
        return value;
    }

    //Не можем хранить пустые строки
    public void setValue(String value) throws IncorrectValueException {
        try {
            if (!value.isEmpty()) {
                this.value = value;
            } else {
                throw new IncorrectValueException("Value is empty");
            }
        } catch (NullPointerException e) {
            throw new IncorrectValueException("Incorrect value", e);
        }
    }


}
