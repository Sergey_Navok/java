import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Runner17_exm3 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass.txt";

        Reader reader = null;

        //try-catch-finally
        try {
            System.out.println("try-1");
            reader = new FileReader(FILE_NAME);
            System.out.println("try-2");
            //Работа с файлом
        } catch (FileNotFoundException e) {
            System.out.println("catch");
            e.printStackTrace();
        } finally {
            System.out.println("finally");
            //Используется для освобождения занятых ресурсов
            //Он отрабатывает независимо от того была ли ошибка или нет
            if (reader != null) {
                System.out.println("if-null");
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
