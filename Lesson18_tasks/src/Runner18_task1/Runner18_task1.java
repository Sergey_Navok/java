import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Runner18_task1 {
    public static void main(String[] args) {
        List<String> cities = new LinkedList<>();
        cities.add("Брест");
        cities.add("Витебск");
        cities.add("Гомель");
        cities.add("Гродно");
        cities.add("Могилев");
        cities.add("Минск");

        //Вывести размер списка в консоли
        System.out.println(cities.size());
        //Вывести в консоли элемент списка "Минск"
        System.out.println(cities.indexOf("Минск"));
        //Вывести список в консоли
        System.out.println(cities);
        //Удалить элемент с индексом 5
        cities.remove(5);
        //Удалить элемент со значением "Брест"
        cities.remove("Брест");
        //Вывести список в консоли
        System.out.println(cities);

        //Получить итератор списка
        ListIterator<String> listIter = cities.listIterator();

        //Вывести список в консоли, используя итератор
        while(listIter.hasNext()){
            System.out.println(listIter.next());
        }


    }
}
