package by.itClass;

public class Department{
    String name;
    Integer quantityFirstShift;
    Integer quantitySecondShift;

    public Department() {
    }

    public Department(String name, Integer quantityFirstShift, Integer quantitySecondShift) {
        this.name = name;
        this.quantityFirstShift = quantityFirstShift;
        this.quantitySecondShift = quantitySecondShift;

        /*try {
            if (quantityFirstShift >= 0) {
                this.quantityFirstShift = quantityFirstShift;
            } else {
                throw new TxtLineException("Value is empty", e);
            }
        } catch (NullPointerException e) {
            throw new TxtLineException("Incorrect value", e);
        }*/
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Integer getQuantityFirstShift() {
        return quantityFirstShift;
    }

    public void setQuantityFirstShift(Integer quantityFirstShift) {
        this.quantityFirstShift = quantityFirstShift;
    }

    public Integer getQuantitySecondShift() {
        return quantitySecondShift;
    }

    public void setQuantitySecondShift(Integer quantitySecondShift) {
        this.quantitySecondShift = quantitySecondShift;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                    .append("Department{name=")
                    .append(name)
                    .append("; quantityFirstShift=")
                    .append(quantityFirstShift)
                    .append("; quantitySecondShift=")
                    .append(quantitySecondShift)
                    .append("}")
                    .toString();
    }
}
