package by.itClass.exception;

public class TxtLineException extends Exception {
    private String errString;

    public TxtLineException() {
    }

    public TxtLineException(String message, String errString) {
        super(message);
        this.errString = errString;
    }

    public String getErrString() {
        return errString;
    }
}
