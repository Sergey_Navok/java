package by.itClass.comparators;

import by.itClass.Department;

import java.util.Comparator;

public class DepartmentSortedByFirstShift implements Comparator<Department> {
    @Override
    public int compare(Department department1, Department department2) {
        return department1.getQuantityFirstShift() - department2.getQuantityFirstShift();
    }
}
