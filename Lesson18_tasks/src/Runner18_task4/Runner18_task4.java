import by.itClass.Department;
import by.itClass.comparators.DepartmentSortedByFirstShift;
import by.itClass.comparators.DepartmentSortedBySecondShift;
import by.itClass.exception.TxtLineException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;

public class Runner18_task4 {
    static final String DELIMETER = ";";
    static final String FILE_NAME = "src/itclass.txt";

    public static void main(String[] args) {
        List<Department> departments = getDeparts(FILE_NAME);

        ListIterator<Department> listIterator = departments.listIterator();

        //Вывести элементы списка в консоль, используя итератор
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }

        System.out.println("-----Сортировка первой смены-----");
        Comparator sortedFirstShift = new DepartmentSortedByFirstShift();
        Collections.sort(departments, sortedFirstShift);

        //Вывести элементы списка в консоль, используя итератор
        listIterator = departments.listIterator();//Можно как-то сбросить итератор???
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }

        System.out.println("-----Сортировка второй смены-----");
        Comparator sortedSecondShift = new DepartmentSortedBySecondShift();
        Collections.sort(departments, sortedSecondShift);

        //Вывести элементы списка в консоль, используя итератор
        listIterator = departments.listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }
    }



    private static List<Department> getDeparts(String filename) {
        List<Department> departments = new ArrayList<>();
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Scanner scanner = new Scanner(inputStream);

        while (scanner.hasNextLine()) {
            String strLine = scanner.nextLine();

            try {
                checkLine(strLine);

                String[] values = strLine.split(DELIMETER);
                String name = values[0];
                Integer quantityFirstShift = Integer.valueOf(values[1]);
                Integer quantitySecondShift = Integer.valueOf(values[2]);

                departments.add(new Department(name, quantityFirstShift, quantitySecondShift));
            } catch (TxtLineException e) {
                e.printStackTrace();
                System.err.println(e.getErrString());
            }
        }

        scanner.close();
        return departments;
    }



    private static void checkLine(String str) throws TxtLineException{
        String[] values = str.split(DELIMETER);

        for (String item : values) {
            if (item.isEmpty() || item == null || item == "") {
                throw new TxtLineException("В строке пустые данные", str);
            }
        }

        if (values[0].isEmpty() || values[0] == "") {
            throw new TxtLineException("Неверно заполено название", str);
        }

        if (values.length < 2) {
            throw new TxtLineException("В строке недостаточно данных", str);
        }

        if (Integer.valueOf(values[1]) < 0 || Integer.valueOf(values[2]) < 0) {
            throw new TxtLineException("Отрицательно число рабочих", str);
        }
    }


}