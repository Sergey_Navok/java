import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Runner18_task2 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/it_Class.txt";
        InputStream inputStream = null;
        List<Integer> arrayList = new ArrayList<Integer>();
        int i = 0;
        int positiveInt = 0;
        int negativeInt = 0;

        try {
            inputStream = new FileInputStream(FILE_NAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Scanner scanner = new Scanner(inputStream);

        while (scanner.hasNextLine()) {
            arrayList.add(Integer.valueOf(scanner.nextInt()));
        }

        //Вывести список в консоль
        System.out.println(arrayList.toString());

        //Получить итератор списка
        ListIterator<Integer> listIterator = arrayList.listIterator();


        //Подсчитать положительные и отрицательные значения, используя итератор
        //удалить отрицательные и вывести измененный список
        while (listIterator.hasNext()) {
            i = listIterator.next();
            //Почему в if нельзя отдать сразу listIterator.hasNext???
            if (i > 0) {
                positiveInt++;
                System.out.println(i);
            }

            if (i < 0) {
                negativeInt++;
                listIterator.remove();
            }

        }

        System.out.println("Всего положительных чисел: " + positiveInt);
        System.out.println("Всего отрицательных чисел: " + negativeInt);

        scanner.close();
    }
}
