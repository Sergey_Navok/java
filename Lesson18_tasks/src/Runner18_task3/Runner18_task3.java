import by.itClass.Trial;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class Runner18_task3 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itclass.txt";
        List<Trial> trials = getTrials(FILE_NAME);

        ListIterator<Trial> listIterator = trials.listIterator();

        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }
    }



    private static List<Trial> getTrials(String filename){
        List<Trial> trials = new LinkedList<>();
        final String DELIMETER = ";";
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Scanner scanner = new Scanner(inputStream);

        while (scanner.hasNextLine()) {
            String[] values = scanner.nextLine().split(DELIMETER);
            String name = values[0];
            Integer firstAppraisal = Integer.valueOf(values[1]);
            Integer secondAppraisal = Integer.valueOf(values[2]);

            trials.add(new Trial(name, firstAppraisal, secondAppraisal));
        }

        scanner.close();

        return trials;
    }
}
