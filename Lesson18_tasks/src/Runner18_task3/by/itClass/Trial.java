package by.itClass;

public class Trial {
    String name;
    Integer firstAppraisal;
    Integer secondAppraisal;

    public Trial() {
    }

    public Trial(String name, Integer firstAppraisal, Integer secondAppraisal) {
        this.name = name;
        this.firstAppraisal = firstAppraisal;
        this.secondAppraisal = secondAppraisal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFirstAppraisal() {
        return firstAppraisal;
    }

    public void setFirstAppraisal(Integer firstAppraisal) {
        this.firstAppraisal = firstAppraisal;
    }

    public Integer getSecondAppraisal() {
        return secondAppraisal;
    }

    public void setSecondAppraisal(Integer secondAppraisal) {
        this.secondAppraisal = secondAppraisal;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                    .append("Trail{name=")
                    .append(name)
                    .append("; firstAppraisal=")
                    .append(firstAppraisal)
                    .append("; secondAppraisal=")
                    .append(secondAppraisal)
                    .append("}")
                    .toString();
    }
}