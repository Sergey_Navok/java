package by.itClass.interfaces;

public interface Massable {
    double getMass();
}
