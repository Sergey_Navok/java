package by.itClass.abstracts;

public abstract class AbstractMaterial extends AbstractCargo{
    //name
    private double density;

    public AbstractMaterial() {
    }

    public AbstractMaterial(String name, double density) {
        super(name);
        this.density = density;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }

    @Override
    public String toString() {
        return super.toString() + ", density=" + density;
    }
}
