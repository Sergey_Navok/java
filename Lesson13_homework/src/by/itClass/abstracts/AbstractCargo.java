package by.itClass.abstracts;

public abstract class AbstractCargo {
    private String name;

    public AbstractCargo() {
    }

    public AbstractCargo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name=" + name;
    }
}
