package by.itClass.abstracts;

import by.itClass.interfaces.Massable;

public abstract class AbstractTare implements Massable {
    private double ownMass;
    private AbstractMaterial material;

    public AbstractTare() {
    }

    public AbstractTare(double ownMass, AbstractMaterial material) {
        this.ownMass = ownMass;
        this.material = material;
    }

    public AbstractMaterial getMaterial() {
        return material;
    }

    public abstract double getVolume();

    @Override
    public double getMass() {
        return ownMass + material.getDensity() * getVolume();
    }
}