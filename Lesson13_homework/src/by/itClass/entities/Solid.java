package by.itClass.entities;

import by.itClass.abstracts.AbstractMaterial;

public class Solid extends AbstractMaterial {
    public Solid() {
    }

    public Solid(String name, double density) {
        super(name, density);
    }

    @Override
    public String toString() {
        return "solid: " + super.toString();
    }
}
