package by.itClass.entities;

import by.itClass.abstracts.AbstractTare;

public class Container extends AbstractTare {
    public final static double LENGTH = 5.5;
    public final static double HEIGHT = 2.2;
    public final static double WIDTH = 1.8;
    public final static double MASS = 900;

    public Container(Solid solid) {
        super(MASS, solid);
    }

    @Override
    public double getVolume() {
        return LENGTH * HEIGHT * WIDTH;
    }
}
