package by.itClass.entities;

import by.itClass.abstracts.AbstractMaterial;

public class Liquid extends AbstractMaterial {
    public Liquid() {
    }

    public Liquid(String name, double density) {
        super(name, density);
    }

    @Override
    public String toString() {
        return "liquid: " + super.toString();
    }
}
