package by.itClass.entities;

import by.itClass.abstracts.AbstractCargo;
import by.itClass.interfaces.Massable;

public class Cargo extends AbstractCargo implements Massable {
    private double mass;

    public Cargo() {
    }

    public Cargo(String name, double mass) {
        super(name);
        this.mass = mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public double getMass() {
        return mass;
    }

    @Override
    public String toString() {
        return "cargo: " + super.toString() + ", mass" + mass;
    }
}
