package by.itClass.entities;

import by.itClass.interfaces.Massable;

public class Person implements Massable {
    private String name;
    private double mass;

    public Person() {
    }

    public Person(String name, double mass) {
        this.name = name;
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public double getMass() {
        return mass;
    }

    @Override
    public String toString() {
        return "person: " + "name=" + name + ", mass=" + mass;
    }
}
