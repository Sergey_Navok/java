package by.itClass.entities;

import by.itClass.abstracts.AbstractTare;

public class Cistern extends AbstractTare {
    public final static double LENGTH = 5.8;
    private final static double RADIUS = 0.8;
    private final static double MASS = 1200;

    public Cistern(Liquid liquid) {
        super(MASS, liquid);
    }

    @Override
    public double getVolume() {
        return Math.PI * Math.pow(RADIUS, 2) * LENGTH;
    }
}
