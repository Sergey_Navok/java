import by.itClass.Student;

import java.util.Arrays;

public class Runner10_task4 {
    public static void showStudents(Student[] students) {
        for (int i = 0; i < students.length; i++) {
            System.out.println(students[i].toString());
        }
    }

    public static void main(String[] args) {
        Student[] students = {new Student("Петров", 20, "Экономический"),
                                new Student("Иванов", 22, "Педагогический"),
                                new Student("Сидоров", 18, "Юридический")};

        showStudents(students);

        Arrays.sort(students);

        showStudents(students);
    }
}