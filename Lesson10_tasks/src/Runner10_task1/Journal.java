import by.itClass.intarfaces.Printable;

public class Journal implements Printable {
    private String name;
    private String releaseDate;

    public Journal() {
    }

    public Journal(String name, String releaseDate) {
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public void print() {
        System.out.println("Journal: " + name + "; " + releaseDate);
    }
}
