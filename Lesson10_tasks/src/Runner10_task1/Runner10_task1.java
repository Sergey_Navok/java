import by.itClass.intarfaces.Printable;

public class Runner10_task1 {
    public static void perform(Printable obj) {
        obj.print();
    }

    public static void main(String[] args) {
        Book book = new Book("Капитанская дочка", "А.С. Пушкин");
        Journal journal = new Journal("Forbes", "15.05.2021");

        perform(book);
        perform(journal);
    }
}
