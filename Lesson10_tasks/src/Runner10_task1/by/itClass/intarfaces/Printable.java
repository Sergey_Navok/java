package by.itClass.intarfaces;

public interface Printable {
    void print();
}
