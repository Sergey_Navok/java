package by.itClass;

public class BusinessTrip implements Comparable<BusinessTrip>{
    private String name;
    private double transportExpenses;
    private int day;
    private double dailyAllowance;

    public BusinessTrip() {
    }

    public BusinessTrip(String name, double transportExpenses, int day, double dailyAllowance) {
        this.name = name;
        transportExpenses = Math.floor(transportExpenses * 100.0) / 100.0;
        this.transportExpenses = transportExpenses;
        this.day = day;
        dailyAllowance = Math.floor(dailyAllowance * 100.0) / 100.0;
        this.dailyAllowance = dailyAllowance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getExpenses() {
        return transportExpenses;
    }

    public void setExpenses(double transportExpenses) {
        transportExpenses = Math.floor(transportExpenses * 100.0) / 100.0;
        this.transportExpenses = transportExpenses;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public double getAllowance() {
        return dailyAllowance;
    }

    public void setAllowance(double dailyAllowance) {
        dailyAllowance = Math.floor(dailyAllowance * 100.0) / 100.0;
        this.dailyAllowance = dailyAllowance;
    }

    public double getTotal() {
        double result = transportExpenses + dailyAllowance * day;
        result = Math.floor(result * 100.0) / 100.0;
        return result;
    }

    @Override
    public int compareTo(BusinessTrip businesTrip) {
        int result = (int) (transportExpenses * 100 - businesTrip.transportExpenses * 100);
        return result;
    }

    @Override
    public String toString() {
        return "BusinesTrip{" +
                "name='" + name + '\'' +
                "; transportExpenses=" + transportExpenses +
                "; day=" + day +
                "; dailyAllowance=" + dailyAllowance +
                '}';
    }
}
