import by.itClass.BusinessTrip;

import java.util.Arrays;

public class Runner10_task6 {
    public static void showBusinessTrip (BusinessTrip[] businesTrip) {
        for (int i = 0; i < businesTrip.length; i++) {
            System.out.println(businesTrip[i]);
        }
    }

    public static void main(String[] args) {
        BusinessTrip[] businesTrips = {new BusinessTrip("Сидоров", 2.55, 2, 5.51),
                                    new BusinessTrip("Иванов", 15.99, 5, 5.51),
                                    new BusinessTrip(null, 0, 0, 0),
                                    new BusinessTrip("Петров", 10.22222, 12, 55.1),
                                    new BusinessTrip()};

        showBusinessTrip(businesTrips);

        int firstTwoTrips = businesTrips[0].getDay() + businesTrips[1].getDay();
        System.out.println("Первые две поездки заняли " + firstTwoTrips + " дней");

        businesTrips[2] = new BusinessTrip("Александров", 5, 5, 5);

        System.out.println("\nСортировка по транспортным расходам:");
        Arrays.sort(businesTrips);
        showBusinessTrip(businesTrips);

        System.out.println("\nСортировка по общим затратам:");
        //В данном случае не сказано, что можно использовать компарторы, поэтому пришлось сделать так
        double[] getTotal = new double[businesTrips.length];

        for (int i = 0; i < businesTrips.length; i++) {
            getTotal[i] = businesTrips[i].getTotal();
        }

        Arrays.sort(getTotal);
        for (int i = 0; i < businesTrips.length; i++) {
            System.out.println(getTotal[i]);
        }

        
    }
}
