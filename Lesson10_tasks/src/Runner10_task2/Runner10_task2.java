import by.IitClass.Circle;
import by.IitClass.Rectangle;
import by.itClass.interfaces.Actions;

public class Runner10_task2 {
    public static void info(Actions obj) {
        System.out.printf("Square: %.2f\n", obj.getSquare());;
        System.out.printf("Perimeter: %.2f\n", obj.getPerimeter());;
    }

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2,5);
        Circle circle = new Circle(2.5);

        info(rectangle);
        info(circle);
    }
}
