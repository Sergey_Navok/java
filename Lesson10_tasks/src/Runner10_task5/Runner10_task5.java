import by.itClass.Product;
import by.itClass.comparators.SortedByNameComparator;
import by.itClass.comparators.SortedByPriceComparator;

import java.util.Arrays;
import java.util.Comparator;

public class Runner10_task5 {
    public static void showProduct(Product[] products) {
        for (int i = 0; i < products.length; i++) {
            System.out.println(products[i].toString());
        }
    }

    public static void main(String[] args) {
        Product[] products = {new Product("Молоко", 2.459, 2),
                        new Product("Яблоко", 1.999, 10),
                        new Product("Апельсин", 2.199990, 5)};

        showProduct(products);

        Comparator sortedByPrice = new SortedByPriceComparator();
        Arrays.sort(products, sortedByPrice);
        showProduct(products);

        Comparator sortedByName = new SortedByNameComparator();
        Arrays.sort(products, sortedByName);
        showProduct(products);
    }
}