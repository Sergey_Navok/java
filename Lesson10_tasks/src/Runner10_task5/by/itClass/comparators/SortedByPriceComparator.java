package by.itClass.comparators;

import by.itClass.Product;

import java.util.Comparator;

public class SortedByPriceComparator implements Comparator<Product> {
    @Override
    public int compare(Product product1, Product product2) {
        int result = (int) (product1.getPrice() * 100 - product2.getPrice() * 100);
        return result;
    }
}
