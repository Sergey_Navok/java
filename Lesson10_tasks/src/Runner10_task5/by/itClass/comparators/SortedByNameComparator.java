package by.itClass.comparators;

import by.itClass.Product;

import java.util.Comparator;

public class SortedByNameComparator implements Comparator<Product> {
    @Override
    public int compare(Product product1, Product product2) {
        return product1.getName().compareTo(product2.getName());
    }
}
