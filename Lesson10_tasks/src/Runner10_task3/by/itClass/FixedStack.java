package by.itClass;

import by.itClass.interfaces.IStack;

public class FixedStack implements IStack {
    private int[] myFixedStack;
    private int height;

    public FixedStack(int myFixedStack) {
        if (myFixedStack > 0) {
            this.myFixedStack = new int[myFixedStack];
            height = 0;
        } else {
            height = -1;
        }
    }

    @Override
    public void push(int item) {
        if (height < myFixedStack.length) {
            myFixedStack[height] = item;
            height++;
        } else {
            System.out.println("Стек заполнен! Необходимо удалить элемент!");
        }
    }

    @Override
    public int pop() {
        if (height == myFixedStack.length) {
            height--;
        }

        if (height > -1) {
            int result = myFixedStack[height];
            height--;
            return result;
        } else {
            System.out.println("Стек пуст! Необходимо добавить элемент!");
            return -999;
        }
    }
}
