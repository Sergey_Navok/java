package by.itClass;

import by.itClass.interfaces.IStack;

import java.util.Arrays;

public class DynamicStack implements IStack {
    private int[] dynamicStack;
    private int height;

    public DynamicStack(int dynamicStack) {
        if (dynamicStack > 0) {
            this.dynamicStack = new int[dynamicStack];
            height = 0;
        } else {
            height = -1;
        }
    }

    @Override
    public void push(int item) {
        if (height == dynamicStack.length) {
            dynamicStack = Arrays.copyOf(dynamicStack, dynamicStack.length +1);
        }

        dynamicStack[height] = item;
        height++;
    }

    @Override
    public int pop() {
        if (height == dynamicStack.length) {
            height--;
        }

        if (height > -1) {
            int result = dynamicStack[height];
            height--;
            return result;
        } else {
            System.out.println("Стек пуст! Необходимо добавить элемент!");
            return -999;
        }
    }
}
