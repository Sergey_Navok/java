import by.itClass.DynamicStack;
import by.itClass.FixedStack;
import by.itClass.interfaces.IStack;

public class Runner10_task3 {
    public static void loadValues(IStack obj, int value) {
        obj.push(value);
    }

    public static void printStack(IStack obj) {
        System.out.println("Значение из стека: " + obj.pop());
    }

    public static void main(String[] args) {
        FixedStack fixedStack = new FixedStack(3);
        DynamicStack dynamicStack = new DynamicStack(3);

        System.out.println("Заполняем фиксированный стек на 3 элемента 4мя значениями...");
        for (int i = 0; i < 4; i++) {
            loadValues(fixedStack, i);
        }

        System.out.println("\nЗаполняем динмический стек на 3 элемента 4мя значениями...");
        for (int i = 0; i < 4; i++) {
            loadValues(dynamicStack, i);
        }

        System.out.println("\nВыводим 4 значения из фиксированного стека на 3 элемента");
        for (int i = 0; i < 4; i++) {
            printStack(fixedStack);
        }

        System.out.println("\nВыводим 5 значений из динамического стека на 3+1 элемента");
        for (int i = 0; i < 5; i++) {
            printStack(dynamicStack);
        }
    }
}
