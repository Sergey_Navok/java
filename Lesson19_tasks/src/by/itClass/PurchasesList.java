package by.itClass;

import by.itClass.comparators.SortedByHighCost;
import by.itClass.comparators.SortedByLowCost;
import by.itClass.exception.TxtLineException;

import java.io.*;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class PurchasesList {
    final String DELIMETER = ";";
    private List<Purchase> purchasesList = new LinkedList<>();

    public PurchasesList() {
    }

    public PurchasesList(String filename) {
        try (InputStream inputStream = new FileInputStream(filename);
             Scanner scanner = new Scanner(inputStream)) {

            while (scanner.hasNextLine()) {
                scanLine(scanner.nextLine());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addPurchase () {
        System.err.println("\nВведите через ';' название покупки, цену и количество:");
        System.err.println("ПРИМЕР: name;price;quantity");

        try (Scanner scanner = new Scanner(System.in)) {
            scanLine(scanner.nextLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("\n");
    }

    //Уже лень было переписывать метод, поэтому костыль на основе предыдущего
    public void addPurchaseByIndex () {
        System.err.println("\nВведите через ';' название покупки (String), цену (int) и количество (double):");
        System.err.println("ПРИМЕР: name;price;quantity");

        try (Scanner scanner = new Scanner(System.in)) {
            String inputStr = scanner.nextLine();

            //Проверяем строку в методе, но метод сам добавляет ее в конец,
            //если индекс не правильный, объект уже находится в конце
            scanLine(inputStr);

            System.err.println("Введите индекс куда надо дабавить покупку: ");
            int index = scanner.nextInt();

            if ((index - 1) >= 0 && (index - 1) < purchasesList.size()) {
                purchasesList.remove(purchasesList.size() - 1);//Удаляем уже добавленный объект

                String[] values = inputStr.split(DELIMETER);
                String name = values[0];
                Integer price = Integer.valueOf(values[1]);
                Double quantity = Double.valueOf(values[2]);
                purchasesList.add(index, new Purchase(name, price, quantity));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("\n");
    }

    public void deleteByIndex(int index) {
        try {
            purchasesList.remove(index);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public double getTotalCost() {
        double totalCost = 0;

        for (Purchase purchase : purchasesList) {
            totalCost += purchase.getPrice() * purchase.getQuantity();
        }

        System.out.println("Общая сумма покупок: " + totalCost);
        return totalCost;
    }

    public void sortedByHighCost() {
        Comparator sortedByHighCost = new SortedByHighCost();
        purchasesList.sort(sortedByHighCost);
    }

    public void sortedByLowCost() {
        Comparator sortedByLowCost = new SortedByLowCost();
        purchasesList.sort(sortedByLowCost);
    }

    public void printCheckToConsole() {
        for (Purchase purchase : purchasesList) {
            System.out.println(purchase.toString());
        }
    }

    public void printChekToFile() {
        final String FILE_NAME = "src/check.txt";

        try (Writer writer = new FileWriter(FILE_NAME)) {
            for (Purchase purchase : purchasesList) {
                writer.write(purchase.toString());
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void scanLine(String str) throws FileNotFoundException{
        try {
            checkLine(str);

            String[] values = str.split(DELIMETER);
            String name = values[0];
            Integer price = Integer.valueOf(values[1]);
            Double quantity = Double.valueOf(values[2]);

            purchasesList.add(new Purchase(name, price, quantity));
        } catch (TxtLineException e) {
            e.printStackTrace();
            System.err.println(e.getErrString());
        }
    }



    private void checkLine(String str) throws TxtLineException{
        String[] values = str.split(DELIMETER);

        for (String item : values) {
            if (item.isEmpty() || item == null || item == "") {
                throw new TxtLineException("В строке пустые данные", str);
            }
        }

        if (values[0].isEmpty() || values[0] == "") {
            throw new TxtLineException("Неверно заполено название", str);
        }

        if (values.length < 2) {
            throw new TxtLineException("В строке недостаточно данных", str);
        }

        if (Integer.valueOf(values[1]) < 0 || Double.valueOf(values[2]) < 0) {
            throw new TxtLineException("Отрицательно число", str);
        }
    }



}
