package by.itClass;

public class Purchase {
    private String name;
    private int price;
    private double quantity;

    public Purchase() {
    }

    public Purchase(String name, int price, double quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public int getCost () {
        double result = price * quantity;
        return (int) result;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Name=")
                .append(name)
                .append("; Price=")
                .append(price)
                .append("; quantity=")
                .append(quantity)
                .toString();
    }
}