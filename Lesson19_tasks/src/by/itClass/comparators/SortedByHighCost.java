package by.itClass.comparators;

import by.itClass.Purchase;

import java.util.Comparator;

public class SortedByHighCost implements Comparator<Purchase> {
    @Override
    public int compare(Purchase purchase1, Purchase purchase2) {
        return purchase1.getCost() - purchase2.getCost();
    }
}
