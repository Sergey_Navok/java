package by.itClass.comparators;

import by.itClass.Purchase;

import java.util.Comparator;

public class SortedByLowCost implements Comparator<Purchase> {
    @Override
    public int compare(Purchase purchase1, Purchase purchase2) {
        return purchase2.getCost() - purchase1.getCost();
    }
}
