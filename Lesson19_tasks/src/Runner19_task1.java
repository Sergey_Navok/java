import by.itClass.PurchasesList;

public class Runner19_task1 {
    static final String FILE_NAME1 = "src/itClass1.txt";//Валидные данные
    static final String FILE_NAME2 = "src/itClass2.txt";//Не валидные данные

    public static void main(String[] args) {
        PurchasesList purchasesList = new PurchasesList(FILE_NAME2);

        System.out.println("Вывести в консоль список покупок:");
        purchasesList.printCheckToConsole();
        System.out.println("---------------");

        System.out.println("Добавить покупку из консоли:");
        purchasesList.addPurchase();
        purchasesList.printCheckToConsole();
        System.out.println("---------------");

        /*
        //Почему-то работает только один из этих методов добавления,
        //я так понял ошибка где-то в потоке
        System.out.println("Добавить покупку из консоли по индексу:");
        purchasesList.addPurchaseByIndex();
        purchasesList.printCheckToConsole();
        System.out.println("---------------");*/

        System.out.println("Удаление индекса, который есть:");
        purchasesList.deleteByIndex(2);
        purchasesList.printCheckToConsole();
        System.out.println("---------------");

        System.out.println("Удаление индекса, которого нет:");
        purchasesList.deleteByIndex(-2);
        purchasesList.printCheckToConsole();
        System.out.println("---------------");

        System.out.println("Сортировка по стоимости товаров по возрастанию:");
        purchasesList.sortedByHighCost();
        purchasesList.printCheckToConsole();
        System.out.println("---------------");

        System.out.println("Сортировка по стоимости товаров по убыванию:");
        purchasesList.sortedByLowCost();
        purchasesList.printCheckToConsole();
        System.out.println("---------------");

        //Общая сумма всех покупок
        purchasesList.getTotalCost();

        System.out.println("Записать чека в файл...");
        purchasesList.printChekToFile();
        System.out.println("Чек сохранен!!!");
    }
}