//к внешним классам, может быть применен только public или default
public class Cup {
    public String color;//public - доступ во всем проекте
    double volume;//default - доступ в рамках папки
    protected double radius;//protected - доступ в рамках папки и всем классам наследникам в других папках
    private int height;//private - доступ только в рамках класса

    //Конструктор по умолчанию
    public Cup () {
        radius = -1;
    }

    //Конструктор с параметром
    public Cup (String color) {
        this.color = color;
    }

    public Cup(String color, double volume) {
        this.color = color;
        this.volume = volume;
    }

    public Cup(double volume, String color) {
        this.color = color;
        this.volume = volume;
    }

    //метод getHeight() - используется для получения значения
    //поля метода
    public int getHeight () {
        return height;
    }

    //метод setHeight() используется для установки значения
    //в поле метода
    public void setHeight (int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    //Если метод ничего не возвращает, значит он
    //должен быть void
    // методу может быть применен любой модификатор доступач
    public String doSmth() {
        return "Hello from Cup!";
    }

    public int test () {
        return 0;
    }

    int test (int p1) {
        return 0;
    }

    private long test (int p1, byte p2) {
        return 0;
    }

    protected byte test (byte p2, int p1) {
        return 0;
    }



    public void info () {
        System.out.println("Color: " + color + "; height: " + height + "; radius: " + radius);
    }

    public String toStr () {
        return "Color: " + color + "; height: " + height + "; radius: " + radius;
    }

    @Override
    public String toString() {
        return "Cup{" +
                "color='" + color + '\'' +
                ", volume=" + volume +
                ", radius=" + radius +
                ", height=" + height +
                '}';
    }
}




