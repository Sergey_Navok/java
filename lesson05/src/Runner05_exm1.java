public class Runner05_exm1 {
    public static void main(String[] args) {
        //Создание объекта
        Cup cup1 = new Cup();
        String color = cup1.color;
        System.out.println("color: " + color);

        cup1.color = "Black";
        cup1.volume = 15.5;
        cup1.radius = 12.0;

        //вызов метода класса
        cup1.setHeight(100);
        String str = cup1.doSmth();
        System.out.println("str: " + str);
        System.out.println("doSmth: " + cup1.doSmth());
        System.out.println("get: " + cup1.getHeight());

        String cupStr = cup1.toStr();
        System.out.println(cupStr);

        //неявно java вызывает метод toString()
        System.out.println(cup1);
    }
}
