public class Runner02_exm2 {
    public static void main(String[] args) {
        //Приведение/преобразование примитивных типов

        byte ageAlex = 19;
        int ageKate = ageAlex;
        ageAlex = (byte) ageKate;

        //Неявное приведение типов происходит, когда в присвоении участвуют переменные ьтипа одинаковой размерности
        //(исключение тип с плавающей точкой в целочисленные)
        ageKate = 18;
        //Если происходит приведение целочисленного типа к типу с плавающей точкой, то оно происходит не явно и
        //добавляется ноль в конце запятой
        double ageBob = ageKate;
        float ageMax = ageKate;
        System.out.printf("double: %f", ageBob);
        ageMax = 18.81928f;
        //Если происходит присвоение/приведение типа с плавающей точкой к целочисленному типу, то отбрасывается часть
        //после запятой
        ageKate = (int) ageMax;
        System.out.printf("\nint: %d", ageKate);

        //При присвоении типов разной размерности, либо типа большей размерности к типу меньшей - нужно указать явно
        //тип к которому приводим. При этом то, что не влазит в тип - теряется.
        ageKate = 123;
        ageAlex = (byte) ageKate;
        System.out.printf("\nbyte: %d", ageAlex);

        ageBob = 123456789123456789.9123456789;
        ageMax = (float) ageBob;
        System.out.printf("\nfloat: %f", ageMax);
    }
}
