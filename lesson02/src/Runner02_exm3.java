public class Runner02_exm3 {
    public static void main(String[] args) {
        int a = 8 + 8;
        int b = a * 2;
        int c = a * b;

        //Тернарный оператор имеет слеующую записть -> (логическое выражение) ? истина : ложь
        int index = (1 > 0) ? 100 : -100;
        System.out.printf("index: %d", index);

    }
}
