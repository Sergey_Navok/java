public class Runner02_exm5 {
    public static void main(String[] args) {
        int[] arr = {1, 6, 2, 8, 9, 10, 5};
        //Цикл for(...)
        for (int i = 0; i<arr.length; i++) {
            int value = arr[i];
            System.out.println(value);
            //System.out.println(arr[i]);
        }

        //Цикл forEach
        //В переменную итерированную value записывается значение элемента массива для n-ной итерации
        //Для этого цикла индексы не ведутся
        for (int value : arr) {
            System.out.println(value);
        }

        //Цикл while если логическое выражение верно
        int i = 0;
        while (i>arr.length) {
            //действие
        }

        //Цикл do..while выполняется если логическое выражение верно, при этом проверка условия осуществляется после
        //одного блока кода, поэтому блок, как минимум, 1 раз будет исполнен
        do {
            //действие
        } while (i > arr.length);

        //Операторы выбора
        if (1 > 0) {

        }

        //------------------//

        if (1 > 0) {

        } else {

        }

        //------------------//

        if (1 > 0) {

        } else if (2 > 8) {

        } else {

        }

        //------------------//
        //Switch в качестве аргумента может принимать может принимать примтивные типы данных, строки и перечисления (Enum)
        switch (2) {
            case 1: //
                break;
            case 2: //
            default:
        }

    }
}
