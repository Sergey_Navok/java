public class Runner02_exm1 {
    public static void main(String[] args) {
        //Локальные переменные
        int age = 18;
        System.out.printf("age=%d", age);

        byte index = 1;
        //Определение переменных с плавающей точкой
        double width = 7.9574; //8 байт в памяти

        float height = (float) 82.947; //double -> float явное преобразование
        float height2 = 82.947f; //float явное преобразование

        char ch = 'A';

        boolean isEmpty = true;
        //isEmpty = (boolean) index;


    }
}

