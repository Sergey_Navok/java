public class Runner02_exm4 {
    public static void main(String[] args) {
        //Массивы - это объекты
        int[] arr1;
        arr1 = new int[5];
        int[] arr2 = new int[5];
        int[] arr3 = new int[] {1, 6, 8, 9};
        int[] arr4 = {1, 6, 7, 8};
        System.out.println(arr3);
        //Обращение к элементам массива происходит по индексу, индексия начинается с 0

        int a = arr4[3];
        arr4[1] = 100500;

        //Создание двумерных и n-мерных массивов
        int[][] arr5 = new int[5][8];
        int b = arr5[0][1];
        arr5[2][5] = 800;

        //int arr6[] = new int[2];
    }
}
