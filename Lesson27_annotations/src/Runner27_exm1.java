import by.itClass.Person;
import by.itClass.analyzis.ViewAnalyzer;

public class Runner27_exm1 {
    public static void main(String[] args) {
        Person person = new Person("Bob", 20);

        Object value = ViewAnalyzer.getFieldValue(person,"name");
        System.out.println(value);

        Object value2 = ViewAnalyzer.getFieldValue(person,"age");
        System.out.println(value2);

        String res = ViewAnalyzer.analyzeRefactorCaseAnnotation(person, "name");
        System.out.println(res);
    }
}