package by.itClass.analyzis;

import by.itClass.annotations.MyAnn;
import by.itClass.annotations.RefactorCase;
import by.itClass.annotations.View;

import java.lang.reflect.Field;
import java.util.Locale;

public class ViewAnalyzer {
    public static Object getFieldValue (Object obj, String fieldName) {
        //Понять если ли аннотация View над этим полем
        //Объекты Class описывают объектно созданные классы
        Class cl = obj.getClass();

        Object res = null;

        try {
            Field field = cl.getDeclaredField(fieldName);

            if (field.isAnnotationPresent(View.class)) {
                field.setAccessible(true);
                res = field.get(obj);
            } else {
                System.err.println("@View not present under field");
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            System.err.println("no such field: " + fieldName);
        }

        return res;
    }

    public static String analyzeRefactorCaseAnnotation(Object obj, String fieldName) {
        Class cl = obj.getClass();

        String res = null;

        try {
            Field field = cl.getDeclaredField(fieldName);

            if (field.getType() != String.class) {
                //Проверка, что поле не является типом строки
            }

            if (field.isAnnotationPresent(RefactorCase.class)) {
                RefactorCase ann = field.getAnnotation(RefactorCase.class);
                String typeCase = ann.to();
                System.out.println("typeCase:" + typeCase);

                field.setAccessible(true);
                res = (String) field.get(obj);

                switch (typeCase) {
                    case "lower":
                        res = res.toLowerCase();
                        break;
                    case "upper":
                        res = res.toUpperCase();
                        break;
                }
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return res;
    }
}
