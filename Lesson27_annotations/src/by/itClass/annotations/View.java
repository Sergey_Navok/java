package by.itClass.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Если аннотация стоит над полем, то мы можем получить всегда
//значение этого поля вне зависимости от модификатора доступа
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface View {

}
