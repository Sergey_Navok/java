package by.itClass;

import by.itClass.annotations.MyAnn;
import by.itClass.annotations.RefactorCase;
import by.itClass.annotations.View;

//@MyAnn
public class Person {
    @MyAnn("IT CLASS")
    @Deprecated
    @RefactorCase(to = "lower", doSmth = "TEST")
    private String name;
    @View
    private int age;

    //@MyAnn
    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //@MyAnn
    public void doSmth() {
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}