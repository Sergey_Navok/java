package by.itClass.Servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;

@WebServlet(name = "PostParamsServlet", urlPatterns = "/post")
public class PostParamsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //doPost(request, response);

        //Метод getParameterMap() возвращает имена параметров и соответствующие им значения в виде Map
        Map<String, String[]> params = request.getParameterMap();

        for (Map.Entry<String, String[]> entry : params.entrySet()) {
            System.out.println("key:" + entry.getKey());
            System.out.println("values:" + Arrays.toString(entry.getValue()));
        }

        //Метод getParameterNames() возвращает имена паратемтров, которые были переданы с запросом
        Enumeration<String> names = request.getParameterNames();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
