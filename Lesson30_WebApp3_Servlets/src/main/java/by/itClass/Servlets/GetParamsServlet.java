package by.itClass.Servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "GetParamsServlet", urlPatterns = "/get", loadOnStartup = 0)
public class GetParamsServlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        System.out.println("Method init");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Method doGet");
        String param1 = request.getParameter("param1");

        String date = request.getParameter("date");
        String time = request.getParameter("time");
        String password = request.getParameter("password");

        System.out.println("param1:" + param1);

        //Поток PrintWriter позволяет сформировать текстовый ответ клиенту
        PrintWriter writer = response.getWriter();
        writer.append("<html>");
        writer.append("<body>");
        writer.append("<h2>");
        writer.append("param1:" + param1);
        writer.append("pass:" + password);
        writer.append("date:" + date);
        writer.append("time:" + time);
        writer.append("</h2>");
        writer.append("</body>");
        writer.append("</html>");

        writer.close();


    }

    @Override
    public void destroy() {
        System.out.println("Method destroy");
    }
}
