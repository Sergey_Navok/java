import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;

public class Runner24_exm1 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_1.xml";

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new FileInputStream(FILE_NAME));

            analyzes(document);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }



    }

    private static void analyzes(Document document) {
        //Получаем рутовский тег
        Node rootNode = document.getDocumentElement();
        NodeList childNodes = rootNode.getChildNodes();

        for (int i = 0; i < childNodes.getLength(); i++) {
            Node node = childNodes.item(i);
            System.out.println(node.getNodeName());

            //Получает тип элемента (тег, коммент, текст...),
            //который описывается целым числом
            int typeNode = node.getNodeType();

            switch (typeNode) {
                case Node.ELEMENT_NODE:
                    String tagName = node.getNodeName();
                    String tagValue1 = node.getTextContent();
                    //Получение значение тега через дочерний текстовый Node
                    Text nodeText = (Text) node.getFirstChild();
                    String textValue = nodeText.getNodeValue();
            }
        }

        Element root = (Element) rootNode;//TAG
        //Метод возвращает значение тега
        String valueTag =  root.getTextContent();

    }

}
