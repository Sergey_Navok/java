import by.itClass.Person;

public class Runner07_exm1 {
    public static void main(String[] args) {
        Person person1 = new Person("Alex", "Blinov");
        Person person2 = new Person("Ivan", "Ivanov");

        //Мето getName - является методом объекта
        String name = person1.getName();

        //Обращаться (вызывать static методы через объект - плохо
        person1.doSmt();
        //static методы должны вызываться только через имя класса
        //для их вызова не нужно создавать объекты этого класса
        System.out.println(Person.doSmt());

        person1.info();
        person2.info();

        //Поскольку static поле является поле класса, т.е. глобальной перменной
        //доступной для всех объектов этого класса, то любое изменение этого
        //поля видят все объекты
        Person.setKey(100);
        person1.info();
        person2.info();

        person2.setKey(500);
        person1.info();
        person2.info();
    }
}
