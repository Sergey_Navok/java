package by.itClass;

public class Person {
    private static int key;
    private final int SERIAL_ID_1 = 300;
    private final int SERIAL_ID_2;
    private final int SERIAL_ID_3;
    private static final int ID_1 = 100;
    private static final int ID_2;
    private String name;
    private String surname;

    static {
        System.out.println("Static block #3");
    }

    static {
        ID_2 = 200;
        System.out.println("Static block #1");
    }

    static {
        System.out.println("Static block #2");
    }

    {
        SERIAL_ID_2 = 500;
        System.out.println("Non static block");
    }

    public Person() {
        SERIAL_ID_3 = 700;
        System.out.println("Constructor");
    }

    public Person(String name, String surname) {
        this();
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public static int getKey() {
        return key;
    }

    public static void setKey(int key) {
        Person.key = key;
    }


    //static методы могут обращаться к static полям
    //и к static методам
    public void info() {
        int tempKey = getKey();
        System.out.println("name: " + name + ", key: " + key);
    }

    //Static метод - является методом класса, а не объекта
    public static String doSmt() {
        //static методы не могут обращаться к non-static полям
        //и non static методам и использовать this в static методах
        //name = "BOB";
        //String tempName = this.getName();
        return "Hello static method";
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }


}
