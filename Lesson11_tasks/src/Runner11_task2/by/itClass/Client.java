package by.itClass;

public class Client {
    private String name;
    private BankAccount account;

    public Client() {
    }

    public Client(String name, BankAccount account) {
        this.name = name;
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BankAccount getAccount() {
        return account;
    }

    public void setAccount(BankAccount account) {
        this.account = account;
    }

    public void show() {
        System.out.println(
                "Person" +
                "\nName: " + name +
                "\nAccount number: " + getAccount().numberAccount +
                "\nOpening date: " + getAccount().openingDate
        );
    }

    //Внутренний класс
    public class BankAccount{
        private int numberAccount;
        private String openingDate;
        private int password;

        public BankAccount() {
        }

        public BankAccount(int numberAccount, String openingDate, int password) {
            this.numberAccount = numberAccount;
            this.openingDate = openingDate;
            this.password = password;
        }

        public void displayAccount() {
            System.out.println(
                    "Account Login: " + getName() +
                    "\nAccount number: " + numberAccount +
                    "\nOpening date: " + openingDate +
                    "\nAccount password: " + password
            );
        }
    }



}
