import by.itClass.Client;

public class Runner11_task2 {
    public static void main(String[] args) {
        Client client = new Client();
        Client.BankAccount bankAccount = client.new BankAccount(123548, "01.12.2010", 321);

        Client client1 = new Client();
        client1.setName("Peter");
        client1.setAccount(bankAccount);

        client1.show();
        client1.getAccount().displayAccount();
        System.out.println("---------------------");

        bankAccount = client.new BankAccount(233548, "23.05.2018", 123);
        Client client2 = new Client("Alex", bankAccount);

        client2.show();
        client2.getAccount().displayAccount();

    }
}
