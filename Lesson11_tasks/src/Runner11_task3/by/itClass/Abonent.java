package by.itClass;

public class Abonent {
    private int id;
    private String name;
    private String tariff;
    public PhoneNumber number;

    public Abonent() {
    }

    public Abonent(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public String getNumber() {
        String result = null;
        if (number != null) {
           result = "+".concat(number.codeCountry.toString()).concat(number.codeNet.toString()).concat(number.phone.toString());
        } else {
           result = "У данного пользователя нет зарегистрированного номера!";
        }
        return result;
    }

    public void obtainPhoneNumber(int codeCountry, int codeNet) {
        number = new PhoneNumber();
        number.setCodeCountry(codeCountry);
        number.setCodeNet(codeNet);
        number.generateNumber();
    }

    public void show() {
        System.out.println(
            "Abonent" +
            "\nName: " + name +
            "\nID: " + id +
            "\nTariff plan: " + tariff +
            "\nPhone number: +".concat(number.codeCountry.toString()).
                                concat(number.codeNet.toString()).
                                concat(number.phone.toString())
        );
    }

    //Внутренний класс
    public class PhoneNumber {
        private Integer codeCountry;
        private Integer codeNet;
        private Integer phone;

        public void setCodeCountry(int codeCountry) {
            this.codeCountry = codeCountry;
        }

        public void setCodeNet(int codeNet) {
            this.codeNet = codeNet;
        }

        void generateNumber() {
            phone = (int) (Math.random() * 8888888) + 1111111;
        }
    }



}
