import by.itClass.Abonent;

public class Runner11_task3 {
    public static void main(String[] args) {
        Abonent abonent = new Abonent(1111, "Tom Andersen");
        abonent.setTariff("Free");
        System.out.println(abonent.getNumber());

        abonent.obtainPhoneNumber(375, 29);
        abonent.show();
    }
}
