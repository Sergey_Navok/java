package by.itClass;

public class Math {
    public static class Factorial {
        private int result;
        private int number;

        public Factorial(int result, int number) {
            this.result = result;
            this.number = number;
        }

        public int getResult() {
            return result;
        }

        public int getNumber() {
            return number;
        }
    }

    public static Factorial getFactorial(int number) {
        int result = 1;
        for (int i = 1; i <= number; ++i) {
            result *= i;
        }
        Factorial factorial = new Factorial(result, number);
        System.out.println("The factorial of the number " + factorial.getNumber() + " is equal to " + factorial.getResult());
        return factorial;
    }


}
