package by.itClass;

public class WrapperString {
    private String wrapper;

    public WrapperString(String wrapperString) {
        this.wrapper = wrapperString;
    }

    public String getWrapper() {
        return wrapper;
    }

    public void setWrapper(String wrapper) {
        this.wrapper = wrapper;
    }

    public void replace(char oldChar, char newChar) {
        int index = wrapper.indexOf(oldChar);

        if (index >= 0) {
            char[] charArray = wrapper.toCharArray();
            charArray[index] = newChar;
            wrapper = new String(charArray);
        } else {
            System.out.println("Строка не содержит данного символа");
        }
    }
}
