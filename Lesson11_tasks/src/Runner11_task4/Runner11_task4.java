import by.itClass.WrapperString;

public class Runner11_task4 {
    public static void main(String[] args) {
        WrapperString wrapper1 = new WrapperString("Java");
        //Замена символа, который есть в строке
        wrapper1.replace('a', 'b');
        System.out.println(wrapper1.getWrapper());

        //Замена символа, которого нет в строке
        wrapper1.replace('x', 'b');

        WrapperString wrapper2 = new WrapperString("IT Class") {
            @Override
            public void replace(char oldChar, char newChar){
                int index = getWrapper().lastIndexOf(oldChar);

                if (index >= 0) {
                    char[] charArray = getWrapper().toCharArray();
                    charArray[index] = newChar;
                    String innerWrapper = new String(charArray);
                    setWrapper(innerWrapper);
                } else {
                    System.out.println("Строка не содержит данного символа");
                }
            }
        };
        wrapper2.replace('s', 'x');
        System.out.println(wrapper2.getWrapper());



    }
}
