package by.itClass.entity;

import by.itClass.currency.Byn;
import by.itClass.exception.EmptyNameException;
import by.itClass.exception.NegativeValueException;

public class DiscountPurchase extends Purchase {
    private Byn discount;

    public DiscountPurchase() {
    }

    public DiscountPurchase(String name, double price, int count, double discount) throws NegativeValueException, EmptyNameException {
        super(name, price, count);
        setDiscount(discount);
    }

    public Byn getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = new Byn(discount);
    }

    @Override
    public int getCost() {
        return super.getCost() - discount.getPenny();
    }

    @Override
    protected StringBuilder fieldsToString() {
        return super.fieldsToString().append(discount);
    }
}
