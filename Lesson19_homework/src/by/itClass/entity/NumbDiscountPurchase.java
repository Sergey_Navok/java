package by.itClass.entity;

import by.itClass.exception.EmptyNameException;
import by.itClass.exception.NegativeValueException;

public class NumbDiscountPurchase extends Purchase {
    private static final int MIN_COUNT = 5;
    private int percent;

    public NumbDiscountPurchase() {
    }

    public NumbDiscountPurchase(String name, double price, int count, int percent) throws NegativeValueException, EmptyNameException {
        super(name, price, count);
        this.percent = percent;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    @Override
    public int getCost() {
        int totalCost = super.getCost();
        if(super.getCount() > MIN_COUNT) {
            totalCost -= totalCost * percent / 100;
        }
        return totalCost;
    }

    @Override
    protected StringBuilder fieldsToString() {
        final char PERCENT = '%';
        return super.fieldsToString().append(percent).append(PERCENT);
    }
}
