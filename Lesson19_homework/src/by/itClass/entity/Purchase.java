package by.itClass.entity;

import by.itClass.currency.Byn;
import by.itClass.exception.EmptyNameException;
import by.itClass.exception.NegativeValueException;

public class Purchase {
    private String name;
    private Byn price;
    private int count;

    public Purchase() {
    }

    public Purchase(String name, double price, int count) throws EmptyNameException, NegativeValueException {
        setName(name);
        setPrice(price);
        setCount(count);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws EmptyNameException {
        if (name.isEmpty()) {
            throw new EmptyNameException("Empty purchase name");
        }
        this.name = name;
    }

    public Byn getPrice() {
        return price;
    }

    public void setPrice(double price) throws NegativeValueException {
        if (price < 0) {
            throw new NegativeValueException("price < 0");
        }
        this.price = new Byn(price);
    }

    public int getCount(){
        return count;
    }

    public void setCount(int count) throws NegativeValueException {
        if (count < 0) {
            throw new NegativeValueException("count < 0");
        }
        this.count = count;
    }

    public int getCost() {
        return count * price.getPenny();
    }

    protected StringBuilder fieldsToString() {
        final char DELIMITER = ';';
        final StringBuilder sb = new StringBuilder();
        sb.append(name)
                .append(DELIMITER)
                .append(price)
                .append(DELIMITER)
                .append(count)
                .append(DELIMITER)
                .toString();
        return sb;
    }

    @Override
    public String toString() {
        //append.getCost();
        return fieldsToString().toString();
    }
}
