package by.itClass;

import by.itClass.entity.Purchase;
import by.itClass.exception.TxtLineException;
import by.itClass.utils.PurchasesFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PurchasesList {
    private List<Purchase> purchases;

    public PurchasesList() {
        purchases = new ArrayList<>();
    }

    public PurchasesList(String filename) {
        this();
        try (Scanner sc = new Scanner(new FileReader(filename))) {
            while (sc.hasNextLine()) {
                try {
                    String line = sc.nextLine();
                    Purchase purchase = PurchasesFactory.getKindPurchase(line);
                    purchases.add(purchase);
                } catch (TxtLineException e) {
                    System.err.println(e.getErrLine());
                }

            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

}
