package by.itClass.currency;

public class Byn {
    private static final String OUT_FORMAT = "%d.02%d";
    private static final int COEFFICIENT = 100;
    private int penny;

    public Byn(double price) {
        this.penny = (int) price * COEFFICIENT;
    }

    public int getPenny() {
        return penny;
    }

    @Override
    public String toString() {
        return String.format(OUT_FORMAT, penny / COEFFICIENT, penny % COEFFICIENT);
    }
}
