package by.itClass.exception;

public class TxtLineException extends Exception {
    private String errLine;

    public TxtLineException() {
    }

    public TxtLineException(String errLine, Throwable cause) {
        super(cause);
        this.errLine = errLine;
    }

    public String getErrLine() {
        return errLine;
    }
}