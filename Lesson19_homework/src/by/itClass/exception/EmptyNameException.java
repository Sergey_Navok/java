package by.itClass.exception;

public class EmptyNameException extends Exception {
    public EmptyNameException() {
    }

    public EmptyNameException (String message) {
        super(message);
    }
}
