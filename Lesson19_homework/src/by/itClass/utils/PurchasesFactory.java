package by.itClass.utils;

import by.itClass.entity.Purchase;
import by.itClass.exception.EmptyNameException;
import by.itClass.exception.NegativeValueException;
import by.itClass.exception.TxtLineException;

public class PurchasesFactory {
    private static final String DELIMITER = ";";

    public static Purchase getKindPurchase(String line) throws TxtLineException {
        Purchase purchase = null;
        String[] values = line.split(DELIMITER);

        try {
            String name = values[0];
            double price = Double.parseDouble(values[1]);
            int count = Integer.parseInt(values[2]);

            purchase = new Purchase(name, price, count);
        } catch (EmptyNameException | NegativeValueException | ArrayIndexOutOfBoundsException | NumberFormatException e) {
            throw new TxtLineException(line, e);
        }

        return purchase;
    }
}
