import java.util.regex.Pattern;

public class Runner20_exm1 {
    public static void main(String[] args) {
        //Java, JavaScript, JavaCore
        //Шаблон -> регулярные выражения
        //name@domen.host

        // ab - подразумевоает жесткую последовательность символов
        // | - Логические операции (или)
        // () - Логическая группа
        // [] - Множество символов
        // + - Повторение ближайшего символа слева (квантификатор)
        // * - Повторение символа от 0 до бесконечности
        // ? - Повторение символа 0 или 1 раз
        // {n, m} - повторение символа от n до m раз включительно
        // {n,} - повторение символа от n до есконечности раз
        // {n} - повторение символа ровно n раз
        // . - Любой символ
        // a-z - Диапазон символов

        //String str1 = "Java";
        //String str2 = "JAva";
        //String str3 = "JAvB";
        //String str4 = "JJJJJJJJJJJJJJavaaaaaaaaaaaaa";
        //String str5 = "Jav";
        //String str6 = "Javaaa";
        //String str7 = "Java";
        //String str8 = "JavaaaaaaaaaaaaaG";
        //String str9 = "JavaaaaaaaaaaaaaG";
        String str = "aJvaaaaaaaaaaaaaG";

        //String regex1 = "Java";
        //String regex2 = "J(A|a)va";
        //String regex3 = "J(A|a)v(a|B|C)";
        //String regex3 = "J(A|a)v[aBC]";
        //String regex4 = "J+ava+";
        //String regex5 = "Java+";//вернет true для str5
        //String regex5 = "Java?";//вернет true для str5
        //String regex7 = "Java{2,4}";
        //String regex8 = "Java{2}G";
        //String regex9 = "[vJa]+G";
        String regex = "J[vJa]+G";

        boolean result = Pattern.matches(regex, str);
        System.out.println("isResult1: " + result);

        //
        str = "it_class@gmail.com";
        //regex = ".+@.+\\..+";
        regex = "[a-z_A-Z0-9]@[a-z]+\\.com";

        result = Pattern.matches(regex, str);
        System.out.println("isResult2: " + result);


        //Классы символов
        // \d -> [0-9]
        // \D -> ^[0-9] отрицание, т.е кроме этих символов
        // \w -> [A-Za-z0-9_]
        // \W -> ^[A-Za-z0-9_] отрицание, т.е кроме этих символов
        // \s -> [\n\t\f] пробелы, табуляция, форматирование
        // \S -> ^[\n\t\f] отрицание, т.е кроме пробелов, знаков табуляции, форматирования


        //str1 = "2";
        //str2 = "2";
        str = "Finally";
        //regex1 = "[0-9]";
        //regex1 = "\\d";
        //regex2 = "\\D";
        regex = "\\w+";

        result = Pattern.matches(regex, str);
        System.out.println("isResult3: " + result);
    }
}
