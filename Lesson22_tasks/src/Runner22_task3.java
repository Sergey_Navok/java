import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Runner22_task3 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_task3.txt";
        StringBuilder strBuilder = new StringBuilder();
        String regex = "[A-Za-zА-Яа-я,.:;&?!№ ]";
        Pattern pattern = Pattern.compile(regex);

        try (InputStream inputStream = new FileInputStream(FILE_NAME);
             Scanner scanner = new Scanner(inputStream)) {

            while (scanner.hasNextLine()) {
                Matcher matcher = pattern.matcher(scanner.nextLine());
                strBuilder.append(matcher.replaceAll("")).append("\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(strBuilder);

    }
}
