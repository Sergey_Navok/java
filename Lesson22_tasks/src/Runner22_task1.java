import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Runner22_task1 {
    public static void main(String[] args) {
        String str = "I study JavaSE, JavaME, JavaEE, JavaScript";
        String regex = "Java\\w*";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }

        String newStr = matcher.replaceAll("Programming");
        System.out.println(newStr);

    }
}