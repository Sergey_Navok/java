import java.io.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Runner22_task2 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_task2.txt";
        StringBuilder strBuilder = new StringBuilder();
        String regex = "//[\\wА-Яа-я]*";
        Pattern pattern = Pattern.compile(regex);

        try (InputStream inputStream = new FileInputStream(FILE_NAME);
            Scanner scanner = new Scanner(inputStream)) {

            while (scanner.hasNextLine()) {
                Matcher matcher = pattern.matcher(scanner.nextLine());
                strBuilder.append(matcher.replaceAll("")).append("\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(strBuilder);

    }
}