import jdk.nashorn.internal.objects.annotations.Function;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Runner22_task6 {
    static String REPLACE = "";
    static String regexCheckIndexLine = "index[\\d]+=[\\d]+";
    static String regexReplaceExceptNumber = "[\\D]+";
    static String regexCheckValueLine = "value[\\d]+=([\\d]+\\.?)+";
    static String regexTrimValue = "value";
    static String regexReplaceExceptValueKey = "=.*";
    static String regexReplaceExceptValueItem = ".*=[ A-Za-z]*";
    static StringBuilder strBuilder = new StringBuilder();
    static HashMap<Integer, Double> valuesItemMap = new HashMap();
    static int errLines = 0;

    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_task6.txt";

        try (InputStream inputStream = new FileInputStream(FILE_NAME);
             Scanner myScanner = new Scanner(inputStream)) {

            readFile(myScanner);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(strBuilder);
        System.out.println("errLines = " + errLines);

    }

    private static void readFile(Scanner sc) {
        while (sc.hasNextLine()) {
            createHashMap(sc.nextLine());
        }

        while (sc.hasNextLine()) {
            readLine(sc.nextLine());
        }
    }

    private static void readLine(String str) {
        if (isCorrectLine(str, regexCheckIndexLine)) {
            int index = (int) getValue(str, regexReplaceExceptNumber);
            if (valuesItemMap.get(index) != null) {
                strBuilder.append("+").append(valuesItemMap.get(index));
            } else {
                errLines++;
            }
        }
    }

    private static boolean isCorrectLine(String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

    private static double getValue (String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        double value = Double.parseDouble(matcher.replaceAll(REPLACE));
        return value;
    }

    private static void createHashMap (String str) {
        if (isCorrectLine(str, regexCheckValueLine)) {
            String trim = str.replaceAll(regexTrimValue, REPLACE);
            int key = (int) getValue(trim, regexReplaceExceptValueKey);
            double value = getValue(str, regexReplaceExceptValueItem);
            valuesItemMap.put(key, value);
        }
    }
}