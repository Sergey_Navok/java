package by.itClass.entity;

public class Company {
    private String name;
    private int registrationNumber;
    private int workers;
    private Address address;

    public Company() {
    }

    public Company(String name, int registrationNumber, int employee, Address address) {
        this.name = name;
        this.registrationNumber = registrationNumber;
        this.workers = employee;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(int registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getWorkers() {
        return workers;
    }

    public void setWorkers(int workers) {
        this.workers = workers;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(name).append(";")
                .append(registrationNumber).append(";")
                .append(workers).append(";")
                .append(address)
                .toString();
    }

    //Inner class
    private class Address {
        private String country;
        private String city;
        private String street;
        private String office;

        public Address() {
        }

        public Address(String country, String city, String street, String numberHouse) {
            this.country = country;
            this.city = city;
            this.street = street;
            this.office = numberHouse;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getOffice() {
            return office;
        }

        public void setOffice(String office) {
            this.office = office;
        }

        @Override
        public String toString() {
            return new StringBuilder()
                    .append(country).append(";")
                    .append(city).append(";")
                    .append(street).append(";")
                    .append(office)
                    .toString();
        }
    }
}
