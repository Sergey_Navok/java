package by.itClass.filters;

import javax.servlet.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Objects;

@WebFilter(filterName = "ParamsFilter", urlPatterns = {"/login", "/index.html"})
public class ParamsFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
        System.out.println("Filter init");
    }

    public void destroy() {
        System.out.println("Filter destroy");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        System.out.println("Filter do filter");
        boolean nonNext =  request.getParameterMap()
                .values()
                .stream()
                .flatMap(values -> Arrays.stream(values))
                .anyMatch(param -> Objects.isNull(param) || ((String) param).isEmpty());

        if (nonNext) {
            PrintWriter writer = response.getWriter();
            writer.append("Incorrect parameters");
        } else {
            chain.doFilter(request, response);
        }

    }
}
