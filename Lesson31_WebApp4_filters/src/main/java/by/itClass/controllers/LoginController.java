package by.itClass.controllers;

import by.itClass.beans.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginController", urlPatterns = "/login")
public class LoginController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        /*PrintWriter writer = response.getWriter();

        writer.append("Your login:" + login);
        writer.append("\n");
        writer.append("Your password:" + password);

        if (writer != null) {
            writer.close();
        }*/

        //...
        User user = new User(login, password);

        request.setAttribute("us", user);
        RequestDispatcher rd = request.getRequestDispatcher("main.jsp");
        rd.forward(request, response);


    }
}
