<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IT CLASS</title>
</head>
<body>

    <h2>Hello World!</h2>
    <%!
        private int serialID;

        public int sum (int a, int b) {
            return a + b;
        }
    %>

    <%
        int id = 0;
        int result = sum(9, 8);
    %>

    <p>9+1=${9+1}</p>
    <p>9+1= <%= sum(9, 1) %> </p>

</body>
</html>
