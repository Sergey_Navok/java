<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>IT CLASS</title>
</head>
<body>

<c:import url="header.html"/>
<c:set var="flag" scope="page" value="100"/>
<p>flag=${flag}</p>

<%--<c:remove var="flag" scope="page"/>--%>
<p>flag=${flag}</p>

<c:if test="${not empty flag}">
  <c:out value="not empty flag"/>
</c:if>

<c:choose>
  <c:when test="${1 < 2}">
    <p>TRUE</p>
  </c:when>

  <c:otherwise>
    <p>FALSE</p>
  </c:otherwise>
</c:choose>

<%
  List<String> lengs = Arrays.asList("Java", "C++", "PHP");
  request.setAttribute("list", lengs);
%>

<c:forEach var="item" items="${list}">
  <p>${item}</p>
</c:forEach>

<c:url var="YouTube" value="https://youtube.com" >
  <c:param name="company" value="IT CLASS"/>
  <c:param name="company" value="IT CLASS"/>
  <c:param name="company" value="IT CLASS"/>
</c:url>

<br>
<a href="${YouTube}">YouTube</a>

<c:catch>
  <p> 1 / 0 = <%= 1 / 0%> </p>
</c:catch>

<%--<c:if test="${empty user}">
  <c:redirect url="http://google.com"/>
</c:if>--%>

</body>
</html>
