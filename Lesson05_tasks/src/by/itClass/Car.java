package by.itClass;

public class Car {

    private String model;
    private String type;
    private String color;
    private String producingCountry;

    public Car() {
        this.model = "";
        this.type = "";
        this.color = "";
        this.producingCountry = "";
    }

    public Car (String model, String type, String color, String producingCountry) {
        this.model = model;
        this.type = type;
        this.color = color;
        this.producingCountry = producingCountry;
    }

    public void setModel (String model) {
        this.model = model;
    }

    public String getModel() {
        return this.model;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public void setProducingCountry(String producingCountry) {
        this.producingCountry = producingCountry;
    }

    public String getProducingCountry() {
        return this.producingCountry;
    }

    public void show() {
        System.out.println(
                "Model - " + this.model + ";" +
                "\nType - " + this.type + ";" +
                "\nColor - " + this.color + ";" +
                "\nProducing country - " + this.producingCountry + ".\n");
    }

}
