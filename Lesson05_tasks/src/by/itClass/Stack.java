package by.itClass;
import java.util.Arrays;

public class Stack {

    private int[] myStack;
    private int endStack;
    private int maxSize = 10;
    private int minSize = - 1;

    public Stack() {
        myStack = new int[maxSize];
        endStack = minSize;
    }

    public void push (int item) {
        if (endStack + 1 < maxSize) {
            myStack[endStack + 1] = item;
            endStack ++;
            System.out.println("Значение стека: " + endStack);
        } else {
            System.out.println("Стек заполнен! Удалите значение!");
        }
    }

    public int pop () {
        if (endStack > minSize) {
            int i = myStack[endStack];
            myStack[endStack] = 0;
            endStack --;
            return i;
        } else {
            return -999;
        }
    }

    public void getArray () {
        System.out.println(Arrays.toString(myStack));
    }

}
