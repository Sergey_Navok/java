package by.itClass;

public class BusinessAuto extends Auto {

    public BusinessAuto (String model, int maxSpeed) {
        super(model, maxSpeed);
    }

    public int getCost() {
        return getMaxSpeed() * 250;
    }

    public void updateAuto() {
        setMaxSpeed(getMaxSpeed() + 25);
    }

}
