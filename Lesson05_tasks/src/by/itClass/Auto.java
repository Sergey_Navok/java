package by.itClass;

public class Auto {

    private String model;
    private int maxSpeed;

    public Auto() {
        this.model = "";
        this.maxSpeed = 0;
    }

    public Auto (String model, int maxSpeed) {
        this.model = model;
        this.maxSpeed = maxSpeed;
    }

    public void setModel (String model) {
        this.model = model;
    }

    public String getModel() {
        return this.model;
    }

    public void setMaxSpeed (int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed() {
        return this.maxSpeed;
    }

    public int getCost() {
        return this.maxSpeed * 100;
    }

    public void updateAuto() {
        this.maxSpeed += 10;
    }

    public void show() {
        System.out.println(
                "Model - " + this.getModel() + "; " +
                "Max speed - " + this.getMaxSpeed() + "; " +
                "Cost - " + getCost() + ".");
    }

}
