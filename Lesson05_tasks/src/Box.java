public class Box {

    private double width;
    private double height;
    private double depth;

    public Box () {
        width = 0;
        height = 0;
        depth = 0;
    }

    public Box (double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public void setWidth (double width) {
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setHeight (double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setDepth (double depth) {
        this.depth = depth;
    }

    public double getDepth() {
        return depth;
    }

    public double getVolume() {
        double volume = this.width * this.height * this.depth;
        return volume;
    }

    public void show() {
        System.out.printf("Width: %.1f, height: %.1f, depth: %.1f\n", this.width, this.height, this.depth);
    }
}
