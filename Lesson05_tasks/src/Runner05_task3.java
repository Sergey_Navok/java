import by.itClass.Car;

public class Runner05_task3 {
    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setModel("Lada");
        car1.setType("hatchback");
        car1.setColor("white");
        car1.setProducingCountry("Russia");

        System.out.println(
                "Information about Car1" +
                "\nModel - " + car1.getModel() + ";" +
                "\nType - " + car1.getType() + ";" +
                "\nColor -" + car1.getColor() + ";" +
                "\nProducing country - " + car1.getProducingCountry() + ".\n");

        Car car2 = new Car ("BMW", "sedan", "black", "Germany");
        System.out.println("Information about Car2");
        car2.show();

    }
}
