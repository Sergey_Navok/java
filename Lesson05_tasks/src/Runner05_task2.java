public class Runner05_task2 {
    public static void main(String[] args) {

        Box box1 = new Box (10, 20, 15);
        Box box2 = new Box (3, 6, 9);

        box1.show();
        box2.show();

        System.out.printf("Box 1 volume = %.1f\n", box1.getVolume());
        System.out.printf("Box 2 volume = %.1f\n", box2.getVolume());

    }
}
