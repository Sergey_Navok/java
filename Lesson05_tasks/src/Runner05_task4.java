import by.itClass.Stack;
import java.util.Scanner;

public class Runner05_task4 {
    public static void main(String[] args) {
        Stack stack1 = new Stack();
        Stack stack2 = new Stack();

        System.out.println("STACK 1");
        for (int i = 0; i < 10; i++) {
            stack1.push(i);
        }
        stack1.getArray();

        System.out.println("STACK 2");
        for (int i = 0; i < 12; i++) {
            stack2.push(i);
        }
        stack2.getArray();


        //Сделал сначала вывод из запрос в цикле, потом только прочитал условие...
        //Можно раскомментировать - все работает
        /*
        Stack stack3 = new Stack();
        int fExit = 0;
        int sw;
        Scanner userEnter = new Scanner(System.in);

        do {
            System.out.println("Выберите пункт меню:");
            System.out.println("1. Добавить в стек");
            System.out.println("2. Достать из стека");
            System.out.println("3. Вывести все значения в стеке");
            System.out.println("4. Выход");
            sw = userEnter.nextInt();

            switch (sw) {
                case 1:
                    System.out.println("Введите значение для добавления в стек:");
                    sw = userEnter.nextInt();
                    stack3.push(sw);
                    break;

                case 2:
                    System.out.println("Значение из стека: " + stack3.pop());
                    break;

                case 3:
                    stack3.getArray();
                    break;

                case 4:
                    fExit = 1;
                    System.out.println("Работа завершена!");
                    break;

                default:
                    System.out.println("Неверный пункт меню!");
            }

        } while (fExit != 1);
        */


    }
}
