public class User {

    private int id;
    private String login;

    public User () {
        id = 0;
        login = "test";
    }

    public User (int id, String login) {
        this.id = id;
        this.login = login;
    }

    public void setId (int id) {
        this.id = id;
    }

    public int getId () {
        return id;
    }

    public void setLogin (String login) {
        this.login = login;
    }

    public String getLogin () {
        return login;
    }
}
