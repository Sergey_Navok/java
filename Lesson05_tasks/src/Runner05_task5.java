import by.itClass.Auto;
import by.itClass.BusinessAuto;

public class Runner05_task5 {
    public static void main(String[] args) {

        Auto auto1 = new Auto("ввв", 140);
        BusinessAuto auto2 = new BusinessAuto("Lada", 170);

        auto1.show();
        auto2.show();

        auto1.updateAuto();
        auto1.show();

        auto2.updateAuto();
        auto2.show();
    }
}
