import java.util.Scanner;

public class Runner04_exm2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String str1 = sc.next();
        String str2 = sc.next();
        sc.nextLine();
        System.out.println("str1 " + str1);
        System.out.println("str2 " + str2);


        String line = sc.nextLine();
        System.out.println("line " + line);

        sc.nextLine();
        double number = sc.nextDouble();


        //sc.close();//try-catch-finaly
    }
}
