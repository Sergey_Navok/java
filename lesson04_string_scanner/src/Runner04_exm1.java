import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Runner04_exm1 {
    public static void main(String[] args) {
        String str1 = "Hello Java from IT CLASS!";

        //Метод length() возвращает длину строки
        int len = str1.length();
        System.out.println("len " + len);

        //Регулярные выражения - шаблон для строки, который задает правила поиска
        //test@mail.ru

        //Метод split() разбирает строки на массив подстрок по указанному разделителю
        String[] words = str1.split(" ");
        System.out.println(Arrays.toString(words));

        //Метод toCharArray() представляет строку ввиде массивов символов
        char[] chars = str1.toCharArray();
        System.out.println(Arrays.toString(chars));


        //Метод charAt() возвращает символ по индексу
        char ch = str1.charAt(17);
        System.out.println("ch = " + ch);

        //Сравнение строк
        String str2 = "A";
        String str3 = "G";//"A"

        //Метод equals() вернет true если строки равны по содержимому
        boolean isEqual = str2.equals(str3);
        System.out.println("isEqual " + isEqual);

        //Метод compareTo() сравнивает строки по содержимому и возвращает:
        //int (числовое) значение, говорящее какая из строк больше
        //значение 0 - то строки равны
        //значение > 0 - то строка str2 > str3 (строка на которой вызваем метод - больше чем та, которую передаем)
        //значение < 0 - то строка str2 < str3 (строка на которой вызваем метод - меньше чем та, которую передаем)
        int compare = str2.compareTo(str3);
        System.out.println("compare " + compare);

        //Метод concat() объединяет строки без разделителя
        str1 = str2.concat(str3);
        str1 = str2 + str3 + ';' + "Java";
        //Метод join() класса String позволяет соеденить строки с учетом разделителя
        str1 = String.join(" ", str1, str2, str2);

        //Метод contains() вернет true, если подстрока substr содержится в исходной строке
        boolean isContains = str1.contains("Java");
        System.out.println("isContains " + isContains);

        str1 = "Hello Java";
        boolean isEquals = str1.contentEquals("Java");
        System.out.println("isEquals " + isEquals);

        //Метод endWIth() возвращает true, если исходная строка заканчивается на переданную строку
        str1 = "IT Class.txt";
        boolean isTXT = str1.endsWith(".txt");
        System.out.println("isTXT " + isTXT);

        //Метод getChars() извлекает из строки набор символов в заданных границах
        //и добавляет в массив, начиная с указанного индекса
        chars = new char[10];
        str1.getChars(0, 5, chars, 4);
        System.out.println("str1 " + str1);
        System.out.println(Arrays.toString(chars));

        //Метод indexOf() возвращает индекс для первого найденного
        //совпадения в строке с переданным символом
        int index = str1.indexOf('c');//124
        //str1.lastIndexOf();

        //Метод ...
        //str1.offsetByCodePoints();

        //Метод trim() убирает пробелы до и после строки
        str1 = "    Hello Java    ";
        str2 = str1.trim();
        System.out.println("str1 " + str1);
        System.out.println("str2 " + str2);

    }
}
