public class Kata {
    public static char findMissingLetter(char[] array)
    {
        char[] symbol = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                            'n', 'm', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        int j = 0;

        for (int i = 0; i < symbol.length; i++) {
            if (array[0] == symbol[i]) {
                j = i;
                break;
            }
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i] != symbol[j]) {
                break;
            }
            j++;
        }
        System.out.println(symbol[j]);
        return symbol[j];
    }
}
