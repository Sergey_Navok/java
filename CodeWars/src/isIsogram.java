public class isIsogram {
    public static boolean isIsogram(String str) {
        str = str.toLowerCase();
        System.out.println(str);
        char[] chars = str.toCharArray();
        boolean isIsogram = true;
        for (int i = 0; i < chars.length - 1; i++) {
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i] == chars[j]) {
                    return isIsogram = false;
                }
            }
        }
        return isIsogram;
    }
}