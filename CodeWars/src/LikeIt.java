class Solution {
    public static String whoLikesIt(String... names) {
        String likes = "no one likes this";
        if (names.length > 0) {
            switch (names.length) {
                case (1):
                    likes = names[0] + " likes this";
                    return likes;
                case (2):
                    likes = names[0] + " and " + names[1] + " like this";
                    return likes;
                case (3):
                    likes = names[0] + ", " + names[1] + " and " + names[2] + " like this";
                    return likes;
                default:
                    likes = names[0] + ", " + names[1] + " and " + (names.length - 2) + " others like this";
                    return likes;
            }
        }

        return likes;
    }
}