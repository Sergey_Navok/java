package task2.by.itClass.analyzed;

import task2.by.itClass.annotation.Equal;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AnalyzedClass {
    public boolean equalObjects (Object obj1, Object obj2) {
        boolean result = false;
        //Создаем коллекцию полей для каждого объекта
        List<Field> fieldsObj1 = getAnnotatedFields(obj1);
        List<Field> fieldsObj2 = getAnnotatedFields(obj2);

        if (fieldsObj1.isEmpty() || fieldsObj2.isEmpty()) {
            System.out.printf("У объекта(ов) %s и(или) %s  нет полей с аннотацией\n",
                    obj1.getClass().getSimpleName(),
                    obj2.getClass().getSimpleName());
            return false;
        }

        for (Field field1 : fieldsObj1) {
            for (Field field2 : fieldsObj2) {
                result = isCompareBy(field1, field2, obj1, obj2);
            }
        }

        return result;
    }



    private List<Field> getAnnotatedFields(Object obj) {
        //Создаем коллекцию только тех полей, где есть необходимая аннотация
        List<Field> fields = Arrays.stream(obj.getClass()
                .getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Equal.class))
                .collect(Collectors.toList());

        return fields;
    }


    private boolean isCompareBy(Field field1, Field field2, Object obj1, Object obj2) {
        Equal annotationAttribute1 = field1.getAnnotation(Equal.class);
        Equal annotationAttribute2 = field2.getAnnotation(Equal.class);

        if (annotationAttribute1 != annotationAttribute2){
            //У сравниваемых объектов разные свойства (атрибуты) для одной аннотации
            return false;
        }

        String attribute = annotationAttribute1.compareBy();
        Object fieldValueObj1 = null;
        Object fieldValueObj2 = null;

        field1.setAccessible(true);
        field2.setAccessible(true);

        try {
            fieldValueObj1 = field1.get(obj1);
            fieldValueObj2 = field1.get(obj2);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        /*switch (attribute) {
            case ("reference"): return fieldValueObj1 == fieldValueObj2;
            case ("value"): return fieldValueObj1.equals(fieldValueObj2);
            default: return false;
        }*/

        System.out.println("Сравниваем объекты: " + obj1.getClass().getSimpleName() + " и " + obj2.getClass().getSimpleName());
        switch (attribute) {
            case ("reference"):
                System.out.println("У классов поле " + field1.getName() + " имеет аттрибут Equal со значением " + attribute);
                System.out.printf("При сравнении по ссылкам значение: %s\n", fieldValueObj1 == fieldValueObj2);
                return fieldValueObj1 == fieldValueObj2;

            case ("value"):
                System.out.println("У классов поле " + field1.getName() + " имеет аттрибут Equal со значением " + attribute);
                System.out.println("При сравнении по содержимому значение: " + fieldValueObj1.equals(fieldValueObj2) + "\n");
                return fieldValueObj1.equals(fieldValueObj2);

            default: return false;
        }
    }


}
