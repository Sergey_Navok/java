package task2.by.itClass;

import task2.by.itClass.annotation.Equal;

public class ClassEqual2 {
    @Equal (compareBy = "value")
    private Integer b;
    @Equal (compareBy = "reference")
    private String name;
    private String str2;

    public ClassEqual2() {
    }

    public ClassEqual2(Integer b, String name) {
        this.b = b;
        this.name = name;
    }

    public ClassEqual2(Integer b, String name, String str2) {
        this.b = b;
        this.name = name;
        this.str2 = str2;
    }

    public Integer getB() {
        return b;
    }

    public void setB(Integer b) {
        this.b = b;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClassEqual{");
        sb.append("a=").append(b);
        sb.append(", str1='").append(name).append('\'');
        sb.append(", str2='").append(str2).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
