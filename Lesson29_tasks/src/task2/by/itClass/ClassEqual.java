package task2.by.itClass;

import task2.by.itClass.annotation.Equal;

public class ClassEqual {
    @Equal (compareBy = "reference")
    private Integer a;
    @Equal (compareBy = "value")
    private String str1;
    private String str2;

    public ClassEqual() {
    }

    public ClassEqual(Integer a, String str1) {
        this.a = a;
        this.str1 = str1;
    }

    public ClassEqual(Integer a, String str1, String str2) {
        this.a = a;
        this.str1 = str1;
        this.str2 = str2;
    }

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public String getStr1() {
        return str1;
    }

    public void setStr1(String str1) {
        this.str1 = str1;
    }

    public String getStr2() {
        return str2;
    }

    public void setStr2(String str2) {
        this.str2 = str2;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ClassEqual{");
        sb.append("a=").append(a);
        sb.append(", str1='").append(str1).append('\'');
        sb.append(", str2='").append(str2).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
