import task1.by.itClass.LazyService;
import task1.by.itClass.SimpleService;
import task1.by.itClass.analyzed.AnalyzedClass;

public class Runner29_task1 {
    public static void main(String[] args) {
        AnalyzedClass analyzed = new AnalyzedClass();

        analyzed.inspectService(SimpleService.class);
        analyzed.inspectService(LazyService.class);
        analyzed.inspectService(Object.class);

        analyzed.inspectMethodInformation(SimpleService.class);
        analyzed.inspectMethodInformation(LazyService.class);
        analyzed.inspectMethodInformation(Object.class);
    }
}
