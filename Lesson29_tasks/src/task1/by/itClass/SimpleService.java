package task1.by.itClass;

import task1.by.itClass.annotation.Init;
import task1.by.itClass.annotation.Service;

@Service(name = "")
public class SimpleService {
    @Init (suppressException = true)
    public void initService() {
        System.out.println("init simple service");
    }

    public void dpSmth() {
        System.out.println("smth method");
    }
}
