package task1.by.itClass.analyzed;

import task1.by.itClass.annotation.Init;
import task1.by.itClass.annotation.Service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnalyzedClass {
    public void inspectService (Class<?> service) {
        if (service.isAnnotationPresent(Service.class)) {
            System.out.println("Класс " + service.getSimpleName() + " содержит аннотацию " + Service.class);

            //Получение значения атрибута
            Service annotation = service.getAnnotation(Service.class);
            System.out.println("Атрибут lazyLoad имеет значение = " + annotation.lazyLoad() + "\n");
        }
    }

    public void inspectMethodInformation(Class<?> service) {
        Method[] methods = service.getMethods();

        for (Method method : methods) {
            if (method.isAnnotationPresent(Init.class)) {
                System.out.println("Метод " + method.getName() + " класса " + service.getSimpleName() +
                        " содержит аннотацию " + Init.class);

                //Получение значения атрибута
                Init annotation = method.getAnnotation(Init.class);
                System.out.println("Атрибут suppressException имеет значение = " + annotation.suppressException() + "\n");
            }
        }
    }



    private void getAnnotationValue(Annotation annotation) {

        /*Method[] methods = service.getDeclaredMethods();
            for (Method method : methods) {
                System.out.println(method.getName());
            }*/
    }
}
