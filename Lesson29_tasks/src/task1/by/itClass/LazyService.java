package task1.by.itClass;

import task1.by.itClass.annotation.Init;
import task1.by.itClass.annotation.Service;

@Service(name = "", lazyLoad = true)
public class LazyService {
    @Init
    public void lazyService () throws Exception {
        System.out.println("init lazy service");
    }

}
