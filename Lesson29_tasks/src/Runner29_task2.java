import task2.by.itClass.ClassEqual2;
import task2.by.itClass.analyzed.AnalyzedClass;
import task2.by.itClass.ClassEqual;

public class Runner29_task2 {
    public static void main(String[] args) {
        ClassEqual test1 = new ClassEqual(15, "test");
        ClassEqual test2 = new ClassEqual(new Integer(15), "test");
        ClassEqual2 test3 = new ClassEqual2(15, "test");
        Object test4 = new Object();

        AnalyzedClass analyzed = new AnalyzedClass();
        analyzed.equalObjects(test1, test2);
        analyzed.equalObjects(test1, test4);
        analyzed.equalObjects(test4, test4);
        analyzed.equalObjects(test1, test3);
    }
}
