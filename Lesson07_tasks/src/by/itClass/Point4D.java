package by.itClass;

public class Point4D extends Point3D {
    public double time;

    public Point4D() {
        super(-1,-1,-1);
        time = -1;
    }

    public Point4D (int x, int y, int z, double time) {
        super(x, y, z);
        this.time = time;
    }

    public String show() {
        return  "Point4D{" + "x=" + x + "; y=" + y + "; z=" + z + "; time=" + time + '}';
    }
}
