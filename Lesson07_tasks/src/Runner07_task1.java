import by.itClass.Point2D;
import by.itClass.Point3D;
import by.itClass.Point4D;

public class Runner07_task1 {
    public static void main(String[] args) {
        Point2D point2D_1 = new Point2D(10,16);
        Point3D point3D_1 = new Point3D();
        Point3D point3D_2 = new Point3D(35, 55, 55);
        Point4D point4D_1 = new Point4D();
        Point4D point4D_2 = new Point4D(144, 154, 164, 200);

        System.out.println("Point2D с параметрами: ");
        System.out.println(point2D_1.show());

        System.out.println("Point3D без параметров: ");
        System.out.println(point3D_1.show());

        System.out.println("Point3D с параметрами: ");
        System.out.println(point3D_2.show());

        System.out.println("Point4D без параметров: ");
        System.out.println(point4D_1.show());

        System.out.println("Point4D с параметрами: ");
        System.out.println(point4D_2.show());

    }
}
