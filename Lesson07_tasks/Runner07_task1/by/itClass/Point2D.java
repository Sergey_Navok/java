package by.itClass;

public class Point2D {
    public int x;
    public int y;

    public Point2D() {
    }

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String show() {
         return "Point2D{" + "x=" + x + "; y=" + y + '}';
    }
}
