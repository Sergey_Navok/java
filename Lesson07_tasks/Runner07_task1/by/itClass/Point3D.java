package by.itClass;

public class Point3D extends Point2D {
    public int z;

    public Point3D() {
        super(-1, -1);
        z = -1;
    }

    public Point3D (int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public String show() {
        return  "Point3D{" + "x=" + x + "; y=" + y + "; z=" + z + '}';
    }
}
