package by.itClass;

import java.util.Arrays;

public class Airline {
    private static Airplane[] capacity = null;//Не вижу смысла заводить массив на определенное
    // количество элементов, т.к. его можно весь не заполнить, но при этом пытаться вывести
    // значение пустого элемента

    public static void addPlane(Airplane obj) {
        if (capacity == null) {
            capacity = new Airplane[1];
            capacity[0] = obj;
        } else {
            capacity = Arrays.copyOf(capacity, capacity.length + 1);
            capacity[capacity.length - 1] = obj;
        }
    }

    public static void removePlane(int deleteIndex) {
        boolean isCorrectIndex = deleteIndex > -1 && deleteIndex < capacity.length;

        if (isCorrectIndex) {
            if (deleteIndex > 0) {
                for (int i = deleteIndex; i < capacity.length - 1; i++) {
                    capacity[i] = capacity[i + 1];
                }
                capacity = Arrays.copyOf(capacity, capacity.length - 1);
            } else {
                capacity = null;
            }
        } else {
            System.out.println("Введите корректный элемент массива");
        }
    }

    public static void showPlane(int showElement) {
        boolean isCorrectElement = showElement > -1 && showElement < capacity.length;

        if (isCorrectElement) {
            System.out.println(capacity[showElement].show());
        } else {
            System.out.println("Введите корректный элемент массива");
        }
    }
}
