package by.itClass;

public class Airplane {
    private String name;
    private String type;

    public Airplane() {
    }

    public Airplane(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String show() {
        return "Airplane{" +
                "name='" + name + ";" +
                "type='" + type +
                '}';
    }
}
