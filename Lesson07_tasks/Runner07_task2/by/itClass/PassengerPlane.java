package by.itClass;

public class PassengerPlane extends Airplane {
    private int numberOfSeats;

    PassengerPlane() {
    }

    public PassengerPlane(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public PassengerPlane(String name, String type, int numberOfSeats) {
        super(name, type);
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    @Override
    public String show() {
        return "PassengerPlane{" +
                "name=" + getName() + ";" +
                "type=" + getType() + ";" +
                "numberOfSeats=" + numberOfSeats +
                '}';
    }
}
