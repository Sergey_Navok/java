package by.itClass;

public class CargoPlane extends Airplane {
    private int liftingCapacity;

    CargoPlane() {
    }

    public CargoPlane(int liftingCapacity) {
        this.liftingCapacity = liftingCapacity;
    }

    public CargoPlane(String name, String type, int liftingCapacity) {
        super(name, type);
        this.liftingCapacity = liftingCapacity;
    }

    public int getCapacity() {
        return liftingCapacity;
    }

    public void setCapacity(int liftingCapacity) {
        this.liftingCapacity = liftingCapacity;
    }

    @Override
    public String show() {
        return "CargoPlane{" +
                "name=" + getName() + ";" +
                "type=" + getType() + ";" +
                "capacity=" + liftingCapacity +
                '}';
    }
}
