import by.itClass.Airline;
import by.itClass.Airplane;
import by.itClass.CargoPlane;
import by.itClass.PassengerPlane;

public class Runner07_task2 {
    public static void main(String[] args) {
        Airplane airplane1 = new Airplane("Су-24", "Военный");
        Airplane airplane2 = new Airplane("Миг-24", "Военный");
        PassengerPlane passengerPlane1 = new PassengerPlane("Боинг 747", "Пассажирский", 818);
        CargoPlane cargoPlane1 = new CargoPlane("Ан-225", "Транспортный", 250000);

        Airline.addPlane(airplane1);
        Airline.addPlane(airplane2);
        Airline.addPlane(passengerPlane1);
        Airline.addPlane(cargoPlane1);

        System.out.println("Проверка всех введенных самолетов:");
        Airline.showPlane(0);
        Airline.showPlane(1);
        Airline.showPlane(2);
        Airline.showPlane(3);
        System.out.println("Проверка лишнего индекса:");
        Airline.showPlane(4);
        System.out.println("\nУдаление самолетов под индексом 1 (Миг-24) и вывод значения этого индекса:");
        Airline.removePlane(1);
        Airline.showPlane(1);
        System.out.println("Удаление самолетов под индексом -1:");
        Airline.removePlane(-1);
        System.out.println("Удаление самолетов под индексом 10:");
        Airline.removePlane(10);
    }
}
