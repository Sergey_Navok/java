import by.itClass.Box;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Runner12_exm5 {
    public static void main(String[] args) {
        String[] arr = {"Java", "", "IT", "", "Class", "Web", "", "", "CORE", ""};
        //Вывести true, если в массиве есть в массиве хоть одна пустая строка
        Predicate<String> isEmpty = str -> str.isEmpty();

        for (String item : arr) {
            if (isEmpty.test(item)) {
                System.out.println(true);
                break;
            }
        }

        //Получение объекта Stream из массива элементов arr
        /*
        Stream<String> stream = null;
        stream =  Arrays.stream(arr);
        */
        Stream<String> stream =  Arrays.stream(arr);
        Predicate<String> isEmpty1 = str -> str.isEmpty();
        //Метод any.Math() возвращает true, если было найдено
        //хотя бы одно соответствие Predicate
        boolean result = stream.anyMatch(isEmpty);
        //разкомментировать result = stream.anyMatch((str) -> str.isEmpty());
        System.out.println(result);


        stream = Arrays.stream(arr);
        stream = stream.sorted((s1, s2) -> s1.compareTo(s2));
        Object[] newArr = stream.toArray();

        System.out.println(Arrays.toString(newArr));

        //Столбчатая запись вызова метода Stream API
        Object[] newArr1 = Arrays.stream(arr)
                            .sorted((s1, s2) -> s1.compareTo(s2))
                            .toArray();

        //
        String[] arr1 = {"Java", null, "IT", null, "Class", "Web", null, null, "CORE", null};
        //Вывести все строки, длинна которых >= 5

        for (String item : arr1) {
            if (item != null) {
                if (item.length() >= 5) {
                    System.out.println(item);
                }
            }
        }

        //Метод filter() применяет переданный Predicate и
        //возвращает новй stream с элементами старого stream-а,
        //который удовлетворяет Predicate

        //Метод forEach() перебирает элементы stream-а и применяет
        //действия, переданные consumer-а к каждому элементу
        Arrays.stream(arr1)
                .filter(str -> str != null)
                .filter(str -> str.length() >= 5)
                .forEach(str -> System.out.println(str));

        Box[] boxes = {new Box(4), new Box(1), new Box(3)};
        Arrays.stream(boxes)//stream -> box
                .map(box -> box.getItem())
                .forEach(item -> System.out.println(item));

    }
}
