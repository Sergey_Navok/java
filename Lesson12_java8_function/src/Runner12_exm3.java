import by.itClass.Box;

import java.util.function.Consumer;

public class Runner12_exm3 {
    public static void main(String[] args) {
        //Интерфейс Consumer используется для выполнения какого-либо
        //действия над переданным объектом
        Consumer<Box> consumerByBox = null;
        //Установка item в начальное значение
        consumerByBox = box -> box.setItem(-1);

        Box box = new Box();
        box.setItem(888);
        System.out.println(box);

        consumerByBox.accept(box);
        System.out.println(box);


    }
}
