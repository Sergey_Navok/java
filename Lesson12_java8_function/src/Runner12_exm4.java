import by.itClass.Box;

import java.util.function.Function;

public class Runner12_exm4 {
    public static void main(String[] args) {
        //Интерфейс Function используется для представления одного
        //типа данных в виде другого
        Function<Box, String> functionByBox = null;
        //Представление переданного объекта Box в виде строки

        functionByBox = box -> box.toString();

        Box box = new Box();
        box.setItem(999);
        String boxStr = functionByBox.apply(box);
        System.out.println(boxStr);


    }
}
