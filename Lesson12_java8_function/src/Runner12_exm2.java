import java.util.function.Supplier;

public class Runner12_exm2 {
    public static void main(String[] args) {
        //Интерфейс Supplier используется для генерации новых объектов
        Supplier<String> supplierStr = null;
        //Получение новых строк

        supplierStr = () -> {
            return new String();
        };

        supplierStr = () -> new String();

        String str = supplierStr.get();
        System.out.println(str);

        System.out.println(supplierStr.get());


    }
}
