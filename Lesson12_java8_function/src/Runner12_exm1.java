import java.util.function.Predicate;

public class Runner12_exm1 {
    public static void main(String[] args) {
        //Java8
        //Интерфейс Predicate используется для построения некоторых логических условий
        Predicate<String> predicateByStr = null;
        //Определяем lambda-выражение

        //Проверку на пустоту строку ""
        predicateByStr = (str) -> {
            return str == "";
        };

        boolean result = predicateByStr.test("");
        System.out.println(result);

        String emptyStr = "";
        System.out.println(predicateByStr.test(emptyStr));

        emptyStr = new String();
        System.out.println(predicateByStr.test(emptyStr));


        predicateByStr = str -> str.isEmpty();
        System.out.println(predicateByStr.test(emptyStr));

        //Еще один пример Predicate
        //Определяет положительное число
        Predicate<Integer> predicateByInteger = null;
        predicateByInteger = number -> number > 0;
        System.out.println("\n");
        System.out.println(predicateByInteger.test(10));
        System.out.println(predicateByInteger.test(-10));
        System.out.println(predicateByInteger.test(0));

    }
}
