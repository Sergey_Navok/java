package by.itClass;

public class Calc {
    //Exeption
    public static int getFactorial(int number) {
        int result = 1;
        if (number > 1) {
            for (int i = 2; i <= number; i++){
                result *= i;
            }
        } else {
            //result = -1;
            IllegalArgumentException exp = new IllegalArgumentException("number: " + number+ " < 1");
            throw exp;//Говорим JVM, что в методе происходит ошибка
        }
        return result;
    }


    public static int getFactorial2(int number) {
        int result = 1;
        if (number > 1) {
            for (int i = 2; i <= number; i++){
                result *= i;
            }
        } else {
            try {
                throw new IllegalArgumentException("number: " + number+ " < 1");//Говорим JVM, что в методе происходит ошибка
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }

        }
        return result;
    }

}
