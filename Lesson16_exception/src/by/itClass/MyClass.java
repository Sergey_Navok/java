package by.itClass;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class MyClass {
    public static void read1(String filename) {
        try {
            InputStream in = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    //Ключевое слов throws используется. если мы отказываемся от обработки возникающей ошибки
    //в методе и тем самым говорим, что она будет обработана в вызывающем методе
    public static void read2(String filename) throws FileNotFoundException, IllegalArgumentException, NullPointerException {
        InputStream in = new FileInputStream(filename);
    }

}
