import by.itClass.Calc;

public class Runner16_exm2 {
    public static void main(String[] args) {
        int result = Calc.getFactorial(4);
        System.out.println(result);

        try {
            result = Calc.getFactorial(-1);
        } catch (IllegalArgumentException e) {
            //System.err.println(e.getMessage());
            e.printStackTrace();
        }

        System.out.println(result);

        System.out.println("111");
    }
}
