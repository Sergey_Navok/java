public class Runner16_exm1 {
    public static void main(String[] args) {
        //Exeption - ошибки

        //Конструкция try-catch используется для обработки
        //искоючительных ситуаций при выполннии программы
        System.out.println("1");
        int number = 1;

        try {
            //подключение к БД -> connection
            //код, который работает с connection

            System.out.println("start try");
            int result = 18 / number;//ошибка ArithmeticException
            //result = 18 / (number - 1);// <---new ArithmeticException("by / zero");

            String item = args[-100];//ошибка IndexOutOfBoundsException

            System.out.println("end try");
            //....
        } catch (ArithmeticException e) {
            System.err.println(e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println(e.getMessage());
        }


        try {
            System.out.println("start try");
            int result = 18 / number;//ошибка ArithmeticException
            String item = args[-100];//ошибка IndexOutOfBoundsException
            System.out.println("end try");
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {// | - логическое ИЛИ при обработке ошибок в catch
            System.err.println(e.getMessage());
        }


        System.out.println("2");




    }
}
