import by.itClass.MyClass;

import java.io.FileNotFoundException;

public class Runner16_exm4 {
    public static void main(String[] args) {
        MyClass.read1("file.....");

        try {
            MyClass.read2("file.....");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
