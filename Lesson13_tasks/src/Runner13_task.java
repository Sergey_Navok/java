import by.itClass.classes.*;
import by.itClass.comparators.SortedByIdComparator;
import by.itClass.entity.Ferry;

import java.util.Arrays;
import java.util.Comparator;

public class Runner13_task {
    private static ContainerMaterial material = null;

    public static void main(String[] args) {
        //Создание пустого парома
        Ferry ferry = new Ferry("Дырявый", 30000);

        System.out.println(ferry.toString());
        System.out.println("------------------");

        ferry.setCurrentCarrying(new Tank("Бензин", 20000));
        ferry.setCurrentCarrying(new Platform("Песок", 7500));
        ferry.setCurrentCarrying(new Passenger("Bob"));
        ferry.setCurrentCarrying(new Container("Сахар", material.METAL, 7000));
        ferry.setCurrentCarrying(new Passenger("Alex"));
        ferry.setCurrentCarrying(new Platform("Автомобиль", 1200));
        ferry.setCurrentCarrying(new Container("Телефоны", material.PLASTIC, -1043));

        System.out.println(ferry.toString());

        //Проверить загрузку парома отдельно от вывода содержимого грузов
        ferry.ferryLoad();

        System.out.println("------------------");
        Comparator sortedByIdComparator = new SortedByIdComparator();
        Arrays.sort(ferry.getCurrentCarrying(), sortedByIdComparator);

        System.out.println(ferry.toString());

    }
}