package by.itClass.classes;

import by.itClass.interfaces.GetIdFromSorted;
import by.itClass.interfaces.TotalWeight;

public class Container implements TotalWeight, GetIdFromSorted {
    private static int numberAll = 0;//Глобальный счетчик контейнеров
    private int number;//Номер контейнера
    private ContainerMaterial material;//Материал контейнера выбираем из предложенных
    private String typeOfCargo;//Тип груза, т.е. что содержится в контейнере
    private static final int id = 2;//Поле, используемое для сортировки
    //Размеры контейнера в метрах на основе которых будет считаться масса
    private double length;
    private double width;
    private double height;

    private double weightContainer;//Вес контейнера
    private double weightCargo;//Вес груза

    public Container(String typeOfCargo, ContainerMaterial material, double weightCargo) {
        setNumberAll(numberAll + 1);//По умолчанию счетчик равен 0, при добавлении контейнера он увеличивается
        number = getNumberAll();//Номер контейнера берем по глобальному счетчику
        this.typeOfCargo = typeOfCargo;

        //На основе выбранного типа материала выбирается стандартный размер контейнера и его вес
        this.material = material;
        if(material == ContainerMaterial.METAL) {
            length = 12.192;
            width = 2.438;
            height = 2.591;
            weightContainer = 3900;
        }

        if(material == ContainerMaterial.WOOD) {
            length = 2;
            width = 2;
            height = 2;
            weightContainer = 300;
        }

        if(material == ContainerMaterial.PLASTIC) {
            length = 2;
            width = 2;
            height = 2;
            weightContainer = 50;
        }

        //Если масса груза отрицательная, то его масса равно 0, т.е. контейнер считается пустым
        this.weightCargo = weightCargo;
        if (weightCargo < 1) {
            this.weightCargo = 0;
        }
    }

    public static int getNumberAll() {
        return numberAll;
    }

    public static void setNumberAll(int numberAll) {
        Container.numberAll = numberAll;
    }

    public int getNumber() {
        return number;
    }

    public ContainerMaterial getMaterial() {
        return material;
    }

    public String getTypeOfCargo() {
        return typeOfCargo;
    }

    public void setTypeOfCargo(String typeOfCargo) {
        this.typeOfCargo = typeOfCargo;
    }

    @Override
    public int getId() {
        return id;
    }

    public double getWeightContainer() {
        return weightContainer;
    }

    public double getWeightCargo() {
        return weightCargo;
    }

    public void setWeightCargo(double weightCargo) {
        //Если масса груза отрицательная, то его масса равно 0, т.е. контейнер считается пустым
        this.weightCargo = weightCargo;
        if (weightCargo < 1) {
            this.weightCargo = 0;
        }
    }

    @Override
    public double getTotalWeight() {
        double result = weightContainer + weightCargo;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Контейнер №%d, содержит: %s, вес груза: %.0fкг, общий вес: %.0fкг\n",
                number, typeOfCargo, weightCargo, getTotalWeight());
    }
}