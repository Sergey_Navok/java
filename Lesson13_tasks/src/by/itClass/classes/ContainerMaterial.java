package by.itClass.classes;

public enum ContainerMaterial{
    METAL,
    PLASTIC,
    WOOD;
}