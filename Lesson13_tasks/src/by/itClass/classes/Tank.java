package by.itClass.classes;

import by.itClass.interfaces.GetIdFromSorted;
import by.itClass.interfaces.TotalWeight;

public class Tank implements TotalWeight, GetIdFromSorted {
    private static int numberAll = 0;//Глобальный счетчик всех цистерн
    private int number;//Номер цистерны
    private String typeOfCargo;
    private static final int id = 3;//Поле, используемое для сортировки
    private static final double weightTank = 7000;//Вес цистерны в кг
    private double weightCargo;//Вес груза в цистрене
    //Разеры цистерны в метрах
    private double length = 10.8;
    private double diameter = 2.8;

    public Tank(String typeOfCargo, double weightCargo) {
        setNumberAll(numberAll + 1);//По умолчанию счетчик равен 0, при добавлении контейнера он увеличивается
        number = getNumberAll();//Номер контейнера берем по глобальному счетчику
        this.typeOfCargo = typeOfCargo;

        //Если масса отрицательная, то цистерна считается пустой
        this.weightCargo = weightCargo;
        if (weightCargo < 1) {
            this.weightCargo = 0;
        }
    }

    public static int getNumberAll() {
        return numberAll;
    }

    public static void setNumberAll(int numberAll) {
        Tank.numberAll = numberAll;
    }

    public int getNumber() {
        return number;
    }

    public String getTypeOfCargo() {
        return typeOfCargo;
    }

    public void setTypeOfCargo(String typeOfCargo) {
        this.typeOfCargo = typeOfCargo;
    }

    public int getId() {
        return id;
    }

    public static double getWeightTank() {
        return weightTank;
    }

    public double getWeightCargo() {
        return weightCargo;
    }

    public void setWeightCargo(double weightCargo) {
        //Если масса отрицательная, то цистерна считается пустой
        this.weightCargo = weightCargo;
        if (weightCargo < 1) {
            this.weightCargo = 0;
        }
    }

    @Override
    public double getTotalWeight() {
        double result = weightTank + weightCargo;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Цистерна №%d, содержит: %s, общий вес груза: %.0fкг\n",
                number, typeOfCargo, getTotalWeight());
    }
}