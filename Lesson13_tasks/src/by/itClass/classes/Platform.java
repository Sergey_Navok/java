package by.itClass.classes;

import by.itClass.interfaces.GetIdFromSorted;
import by.itClass.interfaces.TotalWeight;

public class Platform implements TotalWeight, GetIdFromSorted {
    private static int numberAll = 0;//Глобальный счетчик платформ
    private int number;//Номер платформы
    private String typeOfCargo;//Груз, находящийся на платформе
    private static final int id = 3;//Поле, используемое для сортировки
    private double weightCargo;

    public Platform(String typeOfCargo, double weightCargo) {
        setNumberAll(numberAll + 1);//По умолчанию счетчик равен 0, при добавлении груза на платформу он увеличивается
        number = getNumberAll();//Номер платформы берем по глобальному счетчику
        this.typeOfCargo = typeOfCargo;

        //Если масса груза отрицательная, то платформа считается пустой
        this.weightCargo = weightCargo;
        if (weightCargo < 1) {
            this.weightCargo = 0;
        }
    }

    public static int getNumberAll() {
        return numberAll;
    }

    public static void setNumberAll(int numberAll) {
        Platform.numberAll = numberAll;
    }

    public int getNumber() {
        return number;
    }

    public String getTypeOfCargo() {
        return typeOfCargo;
    }

    public void setTypeOfCargo(String typeOfCargo) {
        this.typeOfCargo = typeOfCargo;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public double getTotalWeight() {
        return weightCargo;
    }

    public void setWeightCargo(double weightCargo) {
        //Если масса груза отрицательная, то платформа считается пустой
        this.weightCargo = weightCargo;
        if (weightCargo < 1) {
            this.weightCargo = 0;
        }
    }

    @Override
    public String toString() {
        return String.format("Платформа №%d, содержит: %s, вес груза: %.0fкг\n",
                number, typeOfCargo, getTotalWeight());
    }
}