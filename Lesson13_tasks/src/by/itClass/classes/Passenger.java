package by.itClass.classes;

import by.itClass.interfaces.GetIdFromSorted;
import by.itClass.interfaces.TotalWeight;

public class Passenger implements TotalWeight, GetIdFromSorted {
    private static int numberAll = 0;//Глобальный счетчик билетов
    private static final int id = 1;//Поле, используемое для сортировки
    private String name;
    private int number;//Билет пассажира
    private double weightPassenger;//Средний вес одного пассажира, кг

    public Passenger(String name) {
        this.name = name;
        this.weightPassenger = 80;
        setNumberAll(numberAll + 1);//По умолчанию счетчик равен 0, при добавлении пассажира он увеличивается
        number = getNumberAll();//Билет пассажира берем по глобальному счетчику
    }

    public static int getNumberAll() {
        return numberAll;
    }

    public static void setNumberAll(int numberAll) {
        Passenger.numberAll = numberAll;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public double getTotalWeight() {
        return weightPassenger;
    }

    @Override
    public String toString() {
        return String.format("Пассажир: %s, номер билета: %d\n", name, number);
    }
}