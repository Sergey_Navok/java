package by.itClass.interfaces;

public interface GetIdFromSorted {
    int getId();

    int getNumber();
}
