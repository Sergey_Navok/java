package by.itClass.entity;

import by.itClass.interfaces.TotalWeight;

import java.util.Arrays;

public class Ferry implements TotalWeight {
    private String name;//Название парома
    private double carryingMax;//Грузоподъемность
    private TotalWeight[] currentCarrying;
    private boolean isFerryOverload = false;

    public Ferry(String name, double carryingMax) {
        this.name = name;
        this.carryingMax = carryingMax;
    }

    public Ferry(String name, double carryingMax, TotalWeight[] currentCarrying) {
        this.name = name;
        this.carryingMax = carryingMax;
        this.currentCarrying = currentCarrying;

        if (getTotalWeight() > carryingMax) {
            isFerryOverload = true;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCarryingMax() {
        return carryingMax;
    }

    public void setCarryingMax(double carryingMax) {
        this.carryingMax = carryingMax;
        if (getTotalWeight() > carryingMax) {
            isFerryOverload = true;
        }
    }

    public TotalWeight[] getCurrentCarrying() {
        return currentCarrying;
    }

    public void setCurrentCarrying(TotalWeight currentCarrying) {
        if (this.currentCarrying == null) {
            this.currentCarrying = new TotalWeight[1];
            this.currentCarrying[0] = currentCarrying;
        } else {
            this.currentCarrying = Arrays.copyOf(this.currentCarrying, this.currentCarrying.length + 1);
            this.currentCarrying[this.currentCarrying.length - 1] = currentCarrying;
        }

        if (getTotalWeight() > carryingMax) {
            isFerryOverload = true;
        }
    }

    public void ferryLoad() {
        if (isFerryOverload) {
            System.out.println("Паром перегружен!!!");
        } else {
            System.out.println("Паром не перегружен и может отправится в путь");
        }
    }

    @Override
    public double getTotalWeight() {
        double result = 0;
        if (currentCarrying == null) {
            return result;
        }

        for (int i = 0; i < currentCarrying.length; i++) {
            result+= currentCarrying[i].getTotalWeight();
        }

        if (result > carryingMax) {
            isFerryOverload = true;
        }
        return result;
    }

    @Override
    public String toString() {
        if (isFerryOverload) {
            return String.format("Название парома: <<%s>>, грузоподъемность: %.0fкг, текущая загрузка: %.0fкг\n" +
                            "Текущий груз:\n%s", name, carryingMax, getTotalWeight(), Arrays.toString(currentCarrying));
        } else {
            return String.format("Название парома: <<%s>>, грузоподъемность: %.0fкг, текущая загрузка: паром пуст",
                    name, carryingMax);
        }
    }
}