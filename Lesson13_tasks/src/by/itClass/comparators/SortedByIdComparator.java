package by.itClass.comparators;

import by.itClass.interfaces.GetIdFromSorted;

import java.util.Comparator;

public class SortedByIdComparator implements Comparator<GetIdFromSorted> {
    @Override
    public int compare(GetIdFromSorted obj1, GetIdFromSorted obj2) {
        if (obj1.getId() == obj2.getId()) {
            return obj1.getNumber() - obj2.getNumber();
        }
        return obj1.getId() - obj2.getId();
    }
}
