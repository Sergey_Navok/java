import by.itClass.Role;

import java.util.Arrays;

public class Runner09_exm3 {
    public static void main(String[] args) {
        Role role = null;
        role = Role.ADMIN;
        System.out.println(role);

        //метод values возвращает массив объектов перечисления
        Role[] roles = Role.values();
        System.out.println(Arrays.toString(roles));

        //метод valueOf вовзращает объект перечисления с именем
        //соответствующем значению переданной строки
        String str = "ADMIN";
        role = Role.valueOf(str);//toUpperCase()
        System.out.println(role);
        System.out.println(role.getClass());
    }
}
