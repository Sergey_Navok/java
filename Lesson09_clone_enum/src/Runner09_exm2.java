import by.itClass.Person;

public class Runner09_exm2 {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person person = new Person("Alex");
        person.key = new Object();
        person.setPhone("+375 29 123-45-67");

        Person personCopy = (Person) person.clone();

        System.out.println("person:" + person + "hash: " + person.hashCode());
        System.out.println("personCopy: " + personCopy + "hash: " + personCopy.hashCode());

        System.out.println("person key: " + person.key.hashCode());
        System.out.println("personCopy key: " + personCopy.key.hashCode());

        System.out.println("person phone: " + person.getPhone() + "hash: " + person.getPhone().hashCode());
        System.out.println("personCopy phone: " + personCopy.getPhone() + "hash: " + personCopy.getPhone().hashCode());
    }
}
