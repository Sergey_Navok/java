package by.itClass;

public enum Role {
    ADMIN,
    MODERATOR,
    USER;
    //public static final Enum => name => value
}
