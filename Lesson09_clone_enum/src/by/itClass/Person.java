package by.itClass;


public class Person implements Cloneable {
    public Object key;
    private Phone phone;
    private String name;

    public Person() {
        phone = new Phone();
    }

    public Person(String name) {
        this();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(String number) {
        phone.setNumber(number);
    }

    //Метод finalize вызывается java-ой перед удалением объекта
    //обычно сюда помещают код освобождения занятых ресурсов
    //начиная с Java9 не рекомендуется к использованию, т.к.
    //java не гарантирует, что он 100% будет вызван
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method");
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Person personCopy = (Person) super.clone();
        Phone phoneCopy = (Phone) phone.clone();
        phone = phoneCopy;
        //phone = (Phone) phone.clone();
        return personCopy;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }

}
