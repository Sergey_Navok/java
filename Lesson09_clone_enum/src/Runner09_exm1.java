import by.itClass.Person;

public class Runner09_exm1 {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person person = new Person();
        System.out.println(person);

        person = null;
        //Thread.sleep(3000);
        System.gc();

        person = new Person("Alex");
        Person personCopy = (Person) person.clone();
        System.out.println("person:" + person + "hash: " + person.hashCode());
        System.out.println("personCopy: " + personCopy + "hash: " + personCopy.hashCode());

        System.out.println("equals: " + person.equals(personCopy));
    }
}
