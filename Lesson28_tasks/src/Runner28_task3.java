import by.itClass.NumLen;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Runner28_task3 {
    //Список отрезков
    static List<NumLen> numLens = new ArrayList<>();

    public static void main(String[] args) {
        final String DB_USER = "root";
        final String DB_PASSWORD = "";
        final String DB_URL = "jdbc:mysql://localhost/itclass_task28?ServerTimezone=Europe/Minsk";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            myConnection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            myStatement = myConnection.createStatement();

            //Получаем результирующую таблицу
            myResultSet = myStatement.executeQuery("select * from coordinates");

            //Создаем, сортируем и выводим результат таблицы в консоль
            createNumLenList(myResultSet);
            Collections.sort(numLens);
            numLens.forEach(System.out::println);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (myResultSet != null) {
                try {
                    myResultSet.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (myStatement != null) {
                try {
                    myStatement.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (myConnection != null) {
                try {
                    myConnection.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
    }



    public static void createNumLenList(ResultSet rs) throws SQLException {
        while (rs.next()) {
            double x1 = rs.getDouble(1);
            double x2 = rs.getDouble(2);

            double segmentLength = getLength(x1, x2);
            int numbersOfSegment = 1;//Test value

            numLens.add(new NumLen(segmentLength, numbersOfSegment));
        }
    }

    public static double getLength (double a, double b) {
        double length = 0;

        a = checkPositiveNumber(a);
        b = checkPositiveNumber(b);

        if (a > b) {
            length = a - b;
        } else {
            length = b - a;
        }

        return length;
    }

    public static double checkPositiveNumber (double x) {
        double positive = x;
        if (positive < 0) {
            positive *= -1;
        }

        return positive;
    }

}