import java.sql.*;

public class Runner28_task1 {
    public static void main(String[] args) {
        final String DB_USER = "root";
        final String DB_PASSWORD = "";
        final String DB_URL = "jdbc:mysql://localhost/itclass_task28?ServerTimezone=Europe/Minsk";

        //Запрос для добавления нового пользователя
        String newUser = "INSERT INTO my_table (name, email) VALUES ('test', 'test@mail.ru');";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            myConnection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            myStatement = myConnection.createStatement();

            //Добваление нового пользователя (он уже добавлен, поэтому закомментирован)
            //myStatement.executeUpdate(newUser);

            //Получаем результирующую таблицу
            myResultSet = myStatement.executeQuery("select * from my_table");

            //Выводим результат таблицы в консоль
            printUser(myResultSet);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (myResultSet != null) {
                try {
                    myResultSet.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (myStatement != null) {
                try {
                    myStatement.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (myConnection != null) {
                try {
                    myConnection.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

    }

    public static void printUser(ResultSet rs) throws SQLException {
        while (rs.next()) {
            String name = rs.getString(1);
            String email = rs.getString(2);
            System.out.printf("name: %s, email: %s %n", name, email);
        }
    }
}