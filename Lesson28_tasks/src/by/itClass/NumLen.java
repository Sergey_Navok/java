package by.itClass;

public class NumLen implements Comparable<NumLen> {
    private double segmentLength;
    private int numbersOfSegment;

    public NumLen() {
    }

    public NumLen(double segmentLength, int numbersOfSegment) {
        this.segmentLength = segmentLength;
        this.numbersOfSegment = numbersOfSegment;
    }

    public double getSegmentLength() {
        return segmentLength;
    }

    public void setSegmentLength(double segmentLength) {
        this.segmentLength = segmentLength;
    }

    public int getNumbersOfSegment() {
        return numbersOfSegment;
    }

    public void setNumbersOfSegment(int numbersOfSegment) {
        this.numbersOfSegment = numbersOfSegment;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("NumLen{");
        sb.append("segmentLength=").append(segmentLength);
        sb.append(", numbersOfSegment=").append(numbersOfSegment);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(NumLen obj) {
        double result = segmentLength - obj.getSegmentLength();
        return (int) result;
    }
}
