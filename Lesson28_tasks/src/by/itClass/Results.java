package by.itClass;

public class Results {
    private String login;
    private String test;
    private String date;
    private String mark;

    public Results() {
    }

    public Results(String login, String test, String date, String mark) {
        this.login = login;
        this.test = test;
        this.date = date;
        this.mark = mark;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Results{");
        sb.append("login='").append(login).append('\'');
        sb.append(", test='").append(test).append('\'');
        sb.append(", date='").append(date).append('\'');
        sb.append(", mark=").append(mark);
        sb.append('}');
        return sb.toString();
    }
}
