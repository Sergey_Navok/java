import by.itClass.Results;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Runner28_task2 {
    //Список пользователей
    static List<Results> results = new ArrayList<>();

    public static void main(String[] args) {
        final String DB_USER = "root";
        final String DB_PASSWORD = "";
        final String DB_URL = "jdbc:mysql://localhost/itclass_task28?ServerTimezone=Europe/Minsk";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection myConnection = null;
        Statement myStatement = null;
        ResultSet myResultSet = null;

        try {
            myConnection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            myStatement = myConnection.createStatement();

            //Сортируем по полю "mark" и получаем результирующую таблицу
            myResultSet = myStatement.executeQuery("select * from results group by results.mark");

            //Создаем и выводим результат таблицы в консоль
            createResultList(myResultSet);
            results.forEach(System.out::println);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (myResultSet != null) {
                try {
                    myResultSet.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (myStatement != null) {
                try {
                    myStatement.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (myConnection != null) {
                try {
                    myConnection.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

    }



    public static void createResultList(ResultSet rs) throws SQLException {
        while (rs.next()) {
            String login = rs.getString(1);
            String test = rs.getString(2);
            String date = rs.getString(3);
            String mark = rs.getString(4);

            results.add(new Results(login, test, date, mark));
        }
    }



}