import by.itClass.Department;
import by.itClass.comparators.ByFirstShiftComparator;
import by.itClass.comparators.BySecondShiftComparator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Runner {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass.txt";

        List<Department> departmentList = new LinkedList<>();
        departmentList.addAll(getDepartments(FILE_NAME));

        ListIterator<Department> departmentListIterator = departmentList.listIterator();

        while (departmentListIterator.hasNext()) {
            System.out.println(departmentListIterator.next());
        }
        System.out.println("======================");

        Comparator<Department> departmentComparatorByFirstShift = new ByFirstShiftComparator();
        departmentList.sort(departmentComparatorByFirstShift);

        departmentListIterator = departmentList.listIterator();
        while (departmentListIterator.hasNext()) {
            System.out.println(departmentListIterator.next());
        }
        System.out.println("======================");

        Comparator<Department> departmentComparatorBySecondShift = new BySecondShiftComparator();
        departmentList.sort(departmentComparatorBySecondShift);

        departmentListIterator = departmentList.listIterator();
        while (departmentListIterator.hasNext()) {
            System.out.println(departmentListIterator.next());
        }
    }

    public static LinkedList<Department> getDepartments(final String fileName) {
        final String DELIMITER = ";";
        LinkedList<Department> departments = new LinkedList<>();

        try(Scanner sc = new Scanner(new File(fileName))) {

            while(sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] values = line.split(DELIMITER);

                if (values.length != 3) {
                    departments.add(new Department("Test", 0, 0));
                } else {
                    try {
                        String name = values[0];
                        Integer countEmpFShift = Integer.valueOf(values[1]);
                        Integer countEmpSShift = Integer.valueOf(values[2]);
                        departments.add(new Department(name, countEmpFShift, countEmpSShift));
                    } catch (NumberFormatException e) {
                        departments.add(new Department("Test", 0, 0));
                        e.printStackTrace();
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return departments;
    }
}
