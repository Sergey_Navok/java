package by.itClass;

import by.itClass.exceptions.TxtLineException;

public class Department {
    private String name;
    private Integer countEmployeeFirstShift;
    private Integer countEmployeeSecondShift;

    public Department() {
    }

    public Department(String name, Integer countEmployeeFirstShift, Integer countEmployeeSecondShift) {
        try {
            if (!name.isEmpty()) {
                this.name = name;
            } else {
                this.name = "Test";
                throw new TxtLineException("Name is Empty!", name);
            }
        } catch (NullPointerException | TxtLineException e) {
            this.name = "Test";
            e.printStackTrace();
        }

        try {
            if (countEmployeeFirstShift < 0 || countEmployeeSecondShift < 0) {
                throw new IllegalArgumentException("Num negative");
            } else if (countEmployeeFirstShift == null || countEmployeeSecondShift == null) {
                throw new NullPointerException();
            } else {
                this.countEmployeeFirstShift = countEmployeeFirstShift;
                this.countEmployeeSecondShift = countEmployeeSecondShift;
            }
        } catch (IllegalArgumentException | NullPointerException e) {
            this.countEmployeeFirstShift = 0;
            this.countEmployeeSecondShift = 0;
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        try {
            if (!name.isEmpty()) {
                this.name = name;
            } else {
                throw new TxtLineException("Name is Empty!", name);
            }
        } catch (NullPointerException | TxtLineException e) {
            e.printStackTrace();
        }
    }

    public int getCountEmployeeFirstShift() {
        return countEmployeeFirstShift;
    }

    public void setCountEmployeeFirstShift(Integer countEmployeeFirstShift) {
        try {
            if (countEmployeeFirstShift < 0) {
                throw new IllegalArgumentException("Num negative");
            } else if (countEmployeeFirstShift == null) {
                throw new NullPointerException();
            } else {
                this.countEmployeeFirstShift = countEmployeeFirstShift;
            }
        } catch (IllegalArgumentException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public int getCountEmployeeSecondShift() {
        return countEmployeeSecondShift;
    }

    public void setCountEmployeeSecondShift(Integer countEmployeeSecondShift) {
        try {
            if (countEmployeeSecondShift < 0) {
                throw new IllegalArgumentException("Num negative");
            } else if (countEmployeeSecondShift == null) {
                throw new NullPointerException();
            } else {
                this.countEmployeeSecondShift = countEmployeeSecondShift;
            }
        } catch (IllegalArgumentException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "name: " + name + "; countEmployeeFirstShift: " + countEmployeeFirstShift
                + "; countEmployeeSecondShift: " + countEmployeeSecondShift + ";";
    }
}
