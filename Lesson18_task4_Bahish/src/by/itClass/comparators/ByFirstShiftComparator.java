package by.itClass.comparators;

import by.itClass.Department;

import java.util.Comparator;

public class ByFirstShiftComparator implements Comparator<Department> {

    @Override
    public int compare(Department o1, Department o2) {
        return o1.getCountEmployeeFirstShift() - o2.getCountEmployeeFirstShift();
    }
}
