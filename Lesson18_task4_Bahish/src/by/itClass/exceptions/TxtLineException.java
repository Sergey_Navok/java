package by.itClass.exceptions;

public class TxtLineException extends Exception {
    private String mistake;

    public TxtLineException() {
    }

    public TxtLineException(String message, String mistake) {
        super(message);
        this.mistake = mistake;
    }

    public String getMistake() {
        return mistake;
    }
}
