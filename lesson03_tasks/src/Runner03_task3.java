import java.util.Arrays;
import java.util.Scanner;

public class Runner03_task3 {
    public static void main(String[] args) {
        System.out.printf("Введите строку: ");
        Scanner userEnter = new Scanner(System.in);
        String str = userEnter.nextLine();

        String[] words = str.split(" ");
        //System.out.println(Arrays.toString(words));

        for (int i = 0; i < words.length; i++) {
            System.out.println("В слове " + words[i] + " - " + words[i].length() + " символа(ов)");
        }

        String wordLong = words[0];
        for (int i = 1; i < words.length; i++) {
            if (words[i].length() > wordLong.length()) {
                wordLong = words[i];
            }
        }

        int count = 0;
        char[] chars = wordLong.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (ch == 'v') {
                count ++;
            }
        }

        if (count > 0) {
            System.out.println("Первое самое длинное слово: " + wordLong);
            System.out.println("Содержит в себе " + count + " символ(лов) 'v'");
        } else {
            System.out.println("Первое самое длинное слово: " + wordLong);
            System.out.println("Не содержит в себе символ 'v'");
        }

        count = 0;
        System.out.printf("Слова с четным числом символов: ");
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() % 2 == 0) {
                System.out.printf("%s ", words[i]);
                count ++;
            }
        }

        if (count == 0) {
            System.out.println("!!! таких слов нет !!!");
        }


    }
}
