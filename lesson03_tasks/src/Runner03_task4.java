import java.util.Arrays;

public class Runner03_task4 {
    public static void main(String[] args) {
        String str = "first non second deed third level levels nun";

        String[] words = str.split(" ");
        System.out.println(Arrays.toString(words));

        //Принимиаем за самое длинное и самое короткое слово - первое слово в строке
        String wordLong = words[0];
        String wordSmall = words[0];
        int wordLongIndex = 0;
        int wordSmallIndex = 0;
        for (int i = 1; i < words.length; i++) {
            if (words[i].length() >= wordLong.length()) {
                wordLong = words[i];
                wordLongIndex = i;
            }
        }

        for (int i = 1; i < words.length; i++) {
            if (words[i].length() < wordSmall.length()) {
                wordSmall = words[i];
                wordSmallIndex = i;
            }
        }

        words[wordSmallIndex] = wordLong;
        words[wordLongIndex] = wordSmall;

        System.out.println(Arrays.toString(words));

        char ch = wordSmall.charAt(0);
        //
        //Конкотенируем символ и пустоту, чтобы получить из символа - строку
        String firstSymbol = ch + "";
        System.out.printf("Слова, начинающиеся с символа <<%s>>: ", firstSymbol);
        for (int i = 0; i < words.length; i++) {
            if (words[i].startsWith(firstSymbol)) {
                System.out.printf("%s ", words[i]);
            }
        }

    }
}
