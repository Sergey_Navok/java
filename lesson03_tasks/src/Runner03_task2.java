import java.util.Arrays;

public class Runner03_task2 {
    public static void main(String[] args) {

        /*
        final String DELIMETER = " ";
        String minPol = str;
        String maxPol = "";

        String[] words = str.plit(DELIMETER);
        for String word : words) {
            cahr[] cahrs = words.toCharArray();
            final LENGTH = chars.length;
            boolean isPol = false;
            for (int int i = 0; i < LENGTH / 2; i++) {
                if (chars[i] != chars[LENGTH - i - 1]) {
                    break;
                }
                isPol = true;
            }
            if (isPol) {
                minPol = (minPol.length() > LENGTH) ? word : minPol;
                maxPol = (maxPol.length() <= LENGTH) ? word : maxPol;
            }
            isPol = false;
        }

        */

        String str = "first non second deed third level levels nun no";

        String[] words = str.split(" ");
        System.out.println(Arrays.toString(words));

        //Принимаем за самое длинное и самое короткое слово - первое слово в строке
        String wordLong = words[0];
        String wordSmall = words[0];
        for (int i = 1; i < words.length; i++) {
            if (words[i].length() >= wordLong.length()) {
                wordLong = words[i];
            }
        }
        System.out.println("Последнее самое длинное слово: " + wordLong);

        for (int i = 1; i < words.length; i++) {
            if (words[i].length() < wordSmall.length()) {
                wordSmall = words[i];
            }
        }
        System.out.println("Первое самое короткое слово: " + wordSmall);

        //Распечатать самое первое длинное и короткое последнее симметричное слово
        boolean isPalindrome = false;
        String palindrome = " ";
        for (int i = 0; i < words.length; i++) {
            //Разбиваем первое слово на массив символов
            char[] chars = words[i].toCharArray();
            //Бежим до середины массива символов и сравниваем символы
            for (int j = 0; j < chars.length / 2; j++) {
                if (chars[j] == chars[chars.length - 1 - j]) {
                    isPalindrome = true;
                    palindrome = palindrome + " " + words[i];
                } else {
                    isPalindrome = false;
                }
            }
        }

        int count = 0;
        String[] wordsPalindrome = palindrome.split(" ");
        for (int i = 0; i < wordsPalindrome.length - 1; i++) {
            if (wordsPalindrome[i].length() > wordsPalindrome[i + 1].length()) {
                count = i;
            }
        }

        if (palindrome != " ") {
            System.out.println("Первое длинное симметричное слово: " + wordsPalindrome[count]);
        } else {
            System.out.println("В строке нет симметричных слов!");
        }

        count = 0;
        for (int i = 1; i < wordsPalindrome.length ; i++) {
            if (wordsPalindrome[i].length() <= wordsPalindrome[i - 1].length()) {
                count = i;
            }
        }

        if (palindrome != " ") {
            System.out.println("Последнее короткое симметричное слово: " + wordsPalindrome[count]);
        } else {
            System.out.println("В строке нет симметричных слов!");
        }


    }
}
