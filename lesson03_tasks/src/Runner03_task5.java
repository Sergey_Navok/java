import java.util.Arrays;

public class Runner03_task5 {
    public static void main(String[] args) {
        String str = "Hello from 1+2 I7*9T 3 / 5 Class";
        String[] operatorsChars = {"/", "+", "-", "*"};
        String[] operators = {"split", "fold", "minus", "multiply"};
        String[] numberChars = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String[] number = {"zero", "one", "two", "tree", "four", "five", "six", "seven", "eight", "nine"};

        String[] words = str.split(" ");
        System.out.println(Arrays.toString(words));

        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < operatorsChars.length; j++) {
                if (words[i].contains(operatorsChars[j])) {
                    words[i] = words[i].replace(operatorsChars[j], operators[j]);
                }
            }
        }
        System.out.println(Arrays.toString(words));

        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < numberChars.length; j++) {
                if (words[i].contains(numberChars[j])) {
                    words[i] = words[i].replace(numberChars[j], number[j]);
                }
            }
        }
        System.out.println(Arrays.toString(words));

        //Здесь вопрос: почему я не могу заменить слово, которое есть в массиве
        //на другое слово из этого массива либо из другого массива, который
        //содержит в себе точно такие же слова, но объявлены не из String Pool?
        //Единственный вариант, который сработал: заменить слово на символ, а
        //потом заменить его на нужное слово
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < operators.length; j++) {
                if (words[i].contains(operators[j])) {
                    words[i] = words[i].replace(operators[j], operatorsChars[j]);
                }
            }
        }

        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < operatorsChars.length; j++) {
                if (words[i].contains(operatorsChars[j])) {
                    words[i] = words[i].replace(operatorsChars[j], operators[operators.length - 1 - j]);
                }
            }
        }
        System.out.println(Arrays.toString(words));

    }
}