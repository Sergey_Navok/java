import java.util.Arrays;

public class Runner03_task1 {
    public static void main(String[] args) {
        String str = "Hello World from ITClass on off if";

        String[] words = str.split(" ");
        System.out.println(Arrays.toString(words));

        System.out.println("Введенная строка состоит из: " + words.length + " слов");

        int symbol = 3;
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() < symbol) {
                System.out.println("Слово №" + i + " содержит менее " + symbol + " символов: " + words[i]);
            }
        }


    }
}
