import by.itClass.beans.Box;

import java.util.Arrays;
import java.util.Comparator;

public class Runner11_exm1 {
    public static void main(String[] args) {
        //Вложенные классы
        Box[] boxes = {new Box(4),
                        new Box(1),
                        new Box(3)};

        //Анонимные вложенные классы
        Box instance = new Box() {//Начало определения анонимного класса
            @Override
            public String toString() {
                return "Anonymous class: " + this.getClass().getName();
            }
        };//Конец определения анонимного класса

        System.out.println(instance);

        String str = new Box() {
            @Override
            public String toString() {
                return "Anonymous class: " + this.getClass().getName();
            }
        }.toString();

        System.out.println(str);

        Comparator comparator = new Comparator<Box>() {
            @Override
            public int compare(Box o1, Box o2) {
                return o1.getId() - o2.getId();
            }
        };

        Arrays.sort(boxes, comparator);

        System.out.println(Arrays.toString(boxes));

        Comparator<Box> comparator2 = (Box box1, Box box2) -> {
          return box1.getId() - box2.getId();
        };


    }
}
