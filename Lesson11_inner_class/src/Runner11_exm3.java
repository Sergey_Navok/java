import by.itClass.beans.OuterClass2;
import by.itClass.beans.OuterClass3;

public class Runner11_exm3 {
    public static void main(String[] args) {
        OuterClass2 outerObject = new OuterClass2();
        OuterClass2.InnerClass innerObject = outerObject.new InnerClass();

        OuterClass3 outerClass3 = new OuterClass3();
        System.out.println(outerClass3.doSmth("Hello java core!"));
    }
}
