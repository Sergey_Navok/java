import by.itClass.beans.OuterClass1;

//Статический импорт выполняется к static классам и static полям
/*import static by.itClass.OuterClass.StaticClass;
import static by.itClass.OuterClass.staticField;*/

//Импорт всех static полей и методов
import static by.itClass.beans.OuterClass1.*;

public class Runner11_exm2 {
    public static void main(String[] args) {
        //Создание объекта вложенного static класса за границами
        //внешнего класса
        OuterClass1.StaticClass staticObject = new OuterClass1.StaticClass();

        StaticClass staticObj = new StaticClass();

        //
        System.out.println(OuterClass1.staticField);
        System.out.println(staticField);



    }
}
