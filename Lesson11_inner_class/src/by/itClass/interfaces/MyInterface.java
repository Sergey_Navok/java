package by.itClass.interfaces;

//Функциональный интрефейс - интерфейс, который содержит один
//без реализации
@FunctionalInterface
public interface MyInterface {
    int doSmth(int a, int b);
    //Можно добавлять методы, которые зарание уже имеют
    //реализацию, как например метод toString() - реализуется
    //в Object
    @Override
    String toString();
}
