package by.itClass.interfaces;

public interface Printable {
    default void print() {
        System.out.println("Printable");
    };
}
