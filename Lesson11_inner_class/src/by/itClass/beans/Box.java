package by.itClass.beans;

public class Box {
    private int id;

    public Box() {
    }

    public Box(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Box{" +
                "id=" + id +
                '}';
    }
}
