package by.itClass.beans;

//public, default - применяется к классу внешнего уровня
public class OuterClass1 {
    private String privateField;
    int defaultField;
    public static String staticField = "IT CLASS";
    private StaticClass staticClass;

    public OuterClass1() {
        //Создание объекта вложенного static класса
        //в рамках внешнего класса
        staticClass = new StaticClass();
    }

    //К non-static полям можно обращаться только через объект
    //вложенного static класса, при этом методы get/set
    //использовать не нужно
    public void print() {
        System.out.println(staticClass.field1);
    }

    //Для обращения к static полям вложенного static класса
    //можно обращаться через имя этого вложенного класса
    //вне зависимости от модификатора доступа к этому полю
    static void show() {
        System.out.println(StaticClass.field3);
    }

    //К вложенным static классам может применяться любой
    //модификатор доступа
    public static class StaticClass {
        private int field1;
        protected int field2;
        static int field3;

        //Во вложенном static классе можно обращаться
        //только к static полям и static методам внешнего класса
        public void doSmth() {
            System.out.println(staticField);
        }
    }


}
