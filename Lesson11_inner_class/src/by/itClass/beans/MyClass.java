package by.itClass.beans;

import by.itClass.interfaces.Printable;

public class MyClass implements Printable{
    @Override
    public void print() {
        Printable.super.print();
    }
}
