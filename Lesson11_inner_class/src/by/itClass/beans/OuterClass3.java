package by.itClass.beans;

public class OuterClass3 {
    private String privateField;
    int defaultField;
    public static String staticField = "IT CLASS";

    public String doSmth(String message) {

        //Локальный класс доступен только в рамках метода
        //Создать объект этого класса можно только в рамках
        //метода, в котором он объявлен
        class Logger {
            private int id;

            public Logger() {
            }

            public Logger(int id) {
                this.id = id;
            }

            public String log(String param) {
                System.out.println(this.getClass().getName());
                return privateField + "; " + staticField + ": " + message + "; " + param + ";" + id;
            }


        }
        //Создание объекта локального класса
        Logger logger = new Logger(100);
        return logger.log("IT CLASS");
    }
}
