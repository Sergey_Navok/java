package by.itClass.beans;

public class OuterClass2 {
    private String privateField;
    int defaultField;
    public static String staticField = "IT CLASS";

    public void print() {

    }

    //Вложенный внутренний класс
    //Может применяться любой из модификаторов доступа
    public class InnerClass {
        private int field1;
        protected int field2;
        int field3;
        //Внутренний класс не может содерать static полей
        //static int field3;

        public void doSmth() {
            System.out.println(privateField);
            System.out.println(defaultField);
            System.out.println(staticField);
        }
    }

}
