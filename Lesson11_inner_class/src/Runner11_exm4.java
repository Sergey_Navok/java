import by.itClass.interfaces.MyInterface;

public class Runner11_exm4 {
    public static void main(String[] args) {
        //Java 8
        //Функциональный интерфейс
        /*MyInterface func = new MyInterface() {
            @Override
            public int doSmth() {
                return 0;
            }
        };*/

        //Анонимный класс, определенный выше можно
        //заменить lambda-выражением

        //Базовая конструкция lambda-выражения
        //(int a, int b, ...) -> {
        // реализация метода из функционального интерфейса
        // return result;
        // }

        MyInterface lambda1 = (int a1, int b1) -> {
            return a1 + b1;
        };

        System.out.println(lambda1.getClass().getName());

        int result = lambda1.doSmth(4, 5);
        System.out.println(result);

        MyInterface lambda2 = (a2, b2) -> a2 + b2;
        System.out.println(lambda2.getClass().getName());

        //(a) -> {}
        //a -> {}
        //a -> ...
        //() -> ...
    }
}
