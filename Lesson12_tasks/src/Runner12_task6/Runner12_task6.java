import by.itClass.Car;

import java.util.Arrays;
import java.util.Comparator;

public class Runner12_task6 {
    public static void main(String[] args) {
        Car[] cars = {new Car("VW", 1111, 1999),
                        new Car("LADA", 8888, 2010),
                        new Car("BMW", 0, 2019),
                        new Car("Audi", 9001, 2021),
                        new Car("Mercedes", 1001, 2021),
                        new Car()};

        System.out.println("Вывести элементы массива:");
        Arrays.stream(cars).forEach(item -> System.out.println(item));

        System.out.println("Вывести элементы массива c non-null и не пустыми номерами:");
        Arrays.stream(cars).filter(item -> item != null)
                            .filter(item -> item.getNumber() > 0)
                            .forEach(item -> System.out.println(item));

        System.out.println("Создать новый массив c non-null и не пустыми номерами, " +
                            "отсортированный по убыванию номеров и до опеределенного года:");
        int yearOfReleaseTo = 2000;
        int yearOfReleaseFrom = 2000;

        Object[] stringNumber =  Arrays.stream(cars).filter(item -> item != null)
                .filter(item -> item.getNumber() > 0)
                .filter(item -> item.getYear() > yearOfReleaseFrom)
                .sorted(new SortedByNumber())
                .toArray(Object[]::new);

        Arrays.stream(stringNumber).forEach(item -> System.out.println(item));
    }

    public static class SortedByNumber implements Comparator<Car> {
        @Override
        public int compare(Car car1, Car car2) {
            return car1.getNumber() - car2.getNumber();
        }
    }
}
