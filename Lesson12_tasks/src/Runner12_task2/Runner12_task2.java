import by.itClass.interfaces.StringOperator;

public class Runner12_task2 {
    public static void main(String[] args) {
        //Удалить пробелы в строке
        StringOperator str = value -> {
            String[] words = value.split(" ");
            String result = "";
            for (String item : words) {
                result += item;
            }
            return result;
        };
        System.out.println(str.doIt("Java in IT Class"));


        //Преобразовать строку к верхнему регистру
        str = value -> value.toUpperCase();
        System.out.println(str.doIt("java"));


        //Перевернуть строку
        str = value -> {
            char[] chars = value.toCharArray();
            for (int i = 0; i < chars.length / 2; i++) {
                char temp = chars[i];
                chars[i] = chars[chars.length - 1 - i];
                chars[chars.length - 1 - i] = temp;
            }
            return new String(chars);
        };
        System.out.println(str.doIt("java"));
        System.out.println(str.doIt("java core"));
    }
}
