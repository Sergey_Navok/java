package by.itClass.interfaces;

@FunctionalInterface
public interface StringOperator {
    String doIt(String string);
}
