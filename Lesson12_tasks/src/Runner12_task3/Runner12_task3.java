import java.util.Arrays;

public class Runner12_task3 {
    public static void main(String[] args) {
        int[] arr = {9, 12, -3, 15, -6, 11, 4, -7, 7, 123, 97, 2689, 3541};

        System.out.println("Вывести все четные числа в массиве");
        Arrays.stream(arr)
                .filter(item -> item %2 == 0)
                .forEach(item -> System.out.println(item));

        System.out.println("Вывести все положительные числа в массиве");
        Arrays.stream(arr)
                .filter(item -> item > 0)
                .forEach(item -> System.out.println(item));

        System.out.println("Вывести все простые числа в массиве");
        Arrays.stream(arr)
                .filter(item -> isPrimeInt(item) == true)
                .forEach(item -> System.out.println(item));

    }

    public static boolean isPrimeInt(int num) {
        //Если число меньше 0, то оно не является простым
        if (num < 0) {
            return false;
        }

        //Если число больше 0 и меньше 3, то оно простое
        if (num > 0 && num < 4) {
            return true;
        }

        //Если число делится без остатка на 2 или 3 - число не является простым
        if ((num % 2 == 0) || (num % 3 == 0)) {
            return false;
        }

        int count = 5;
        while (Math.pow(count, 2) <= num) {
            if (num % count == 0 || num % (count + 2) == 0) {
                return false;
            }

            count += 6;
        }
        return true;
    }

}
