package by.itClass;

public class Tree {
    private String breed;
    private Integer height;

    public Tree() {
    }

    public Tree(String breed, Integer height) {
        this.breed = breed;
        this.height = height;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "breed='" + breed + '\'' +
                ", height=" + height +
                '}';
    }
}
