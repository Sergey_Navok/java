import by.itClass.Tree;

import java.util.Arrays;
import java.util.Comparator;

public class Runner12_task4 {
    public static void main(String[] args) {
        Tree[] trees = {new Tree("Ясень",5),
                        new Tree("Сосна", 5),
                        new Tree("Береза", 3),
                        new Tree("Липа",9)};

        System.out.println("Вывод объектов в массиве:");
        Arrays.stream(trees).forEach(item -> System.out.println(item));

        System.out.println("Вывод отсортированных объектов:");
        Arrays.stream(trees).sorted(new SortedByHeightTree())
                            .forEach(item -> System.out.println(item));
    }


    static class SortedByHeightTree implements Comparator<Tree>{
        @Override
        public int compare(Tree tree1, Tree tree2) {
            boolean isEqualsHeight = tree1.getHeight() == tree2.getHeight();

            if (isEqualsHeight) {
                return tree1.getBreed().compareTo(tree2.getBreed());
            } else {
                return tree1.getHeight().compareTo(tree2.getHeight());
            }
        }
    }
}
