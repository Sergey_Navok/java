package by.itClass.interfaces;

@FunctionalInterface
public interface Factorial {
    int getFactorial(int value);
}
