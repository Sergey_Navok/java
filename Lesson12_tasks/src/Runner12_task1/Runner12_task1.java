import by.itClass.interfaces.Factorial;

public class Runner12_task1 {
    public static void main(String[] args) {
        //Lambda для расчета факториала
        Factorial factorial = number -> {
            int result = 1;
            for (int i = 1; i <= number; ++i) {
                result *= i;
            }
            return result;
        };

        System.out.println(factorial.getFactorial(4));
        
        
    }
}
