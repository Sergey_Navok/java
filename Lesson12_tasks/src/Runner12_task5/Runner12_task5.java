import by.itClass.Auto;

import java.util.Arrays;
import java.util.Comparator;

public class Runner12_task5 {
    public static void main(String[] args) {
        Auto[] cars = {new Auto("Tesla", 250, new Auto.Engine(555, "electric")),
                        new Auto("VW", 250, new Auto.Engine(197, "petrol")),
                        new Auto("Buggati", 330, new Auto.Engine(756, "petrol")),
                        new Auto("VW", 170, new Auto.Engine(987, "diesel")),
                        new Auto("LADA", 190, new Auto.Engine(183, "petrol")),
                        };

        System.out.println("Вывести элементы массива: ");
        Arrays.stream(cars).forEach(item -> System.out.println(item));

        System.out.println("Вывести элементы массива, используя простой метод сортировки: ");
        Arrays.stream(cars).sorted(new SortedByCarModel())
                            .forEach(item -> System.out.println(item));

        System.out.println("Вывести элементы массива, используя сложный метод сортировки: ");
        Arrays.stream(cars).sorted(new SortedByCarModelByEngine())
                .forEach(item -> System.out.println(item));
    }

    static class SortedByCarModel implements Comparator<Auto> {
        @Override
        public int compare(Auto car1, Auto car2) {
            return car1.getModel().compareTo(car2.getModel());
        }
    }

    static class SortedByCarModelByEngine implements Comparator<Auto> {
        @Override
        public int compare(Auto car1, Auto car2) {
            boolean isEqualModel = car1.getModel() == car2.getModel();

            if(isEqualModel) {
                return car1.getEngine().getType().compareTo(car2.getEngine().getType());
            } else {
                return car1.getModel().compareTo(car2.getModel());
            }
        }
    }



}
