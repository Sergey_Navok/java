package by.itClass;

public class Auto {
    private String model;
    private int maxSpeed;
    private Engine engine;

    public Auto() {
    }

    public Auto(String model, int maxSpeed, Engine engine) {
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.engine = engine;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", engine code=" + engine.getCode() +
                ". engine type=" + engine.getType() +
                '}';
    }



    public static class Engine {
        private int code;
        private String type;

        public Engine() {
        }

        public Engine(int code, String type) {
            this.code = code;
            this.type = type;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
