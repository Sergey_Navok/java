<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cars Page</title>
</head>
<body>

<h2>Cars</h2>

<table>
    <th>
    <td>id</td>
    <td>model</td>
    <td>color</td>
    <td>year</td>
    </th>

    <c:forEach var="car" items="${cars}">
        <tr>
            <td>${car.id}</td>
            <td>${car.model}</td>
            <td>${car.color}</td>
            <td>${car.year}</td>
        </tr>
    </c:forEach>

</table>

</body>
</html>
