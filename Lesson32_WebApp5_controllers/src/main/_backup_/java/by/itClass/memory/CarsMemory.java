package by.itClass.memory;

import by.itClass.beans.Car;

import java.util.ArrayList;
import java.util.List;

public class CarsMemory {
    private static List<Car> cars = new ArrayList<Car>();

    static {
        cars.add(new Car(1, "Mercedes" , "Black", 2019));
        cars.add(new Car(1, "BMW" , "White", 2020));
        cars.add(new Car(1, "LADA" , "Red", 2018));
    }

    public static boolean save(Car car) {
        return cars.add(car);
    }

    public static List<Car> getAll() {
        return cars;
    }

}
