package by.itClass.controllers;

import by.itClass.beans.Car;
import by.itClass.constants.Constant;
import by.itClass.memory.CarsMemory;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CarController", urlPatterns = {"/cars"})
public class CarController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Car> cars = CarsMemory.getAll();

        request.setAttribute(Constant.CARS_ATTR, cars);

        request.getRequestDispatcher(Constant.CARS_JSP)
                .forward(request, response);
    }
}
