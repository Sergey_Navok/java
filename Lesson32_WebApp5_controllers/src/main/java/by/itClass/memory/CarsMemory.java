package by.itClass.memory;

import by.itClass.beans.Car;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

public class CarsMemory {
    private static int next_id;
    private static List<Car> cars = new ArrayList<Car>();

    static {
        cars.add(new Car(1, "Mercedes" , "Black", 2019));
        cars.add(new Car(2, "BMW" , "White", 2020));
        cars.add(new Car(3, "LADA" , "Red", 2018));

        next_id = 4;
    }

    public static boolean save(String model, String color, String year) {
        Car car = new Car(next_id++, model, color, year);

        return cars.add(car);
    }

    public static List<Car> getAll() {
        return cars;
    }

}
