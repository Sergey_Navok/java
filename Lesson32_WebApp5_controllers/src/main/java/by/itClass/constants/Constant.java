package by.itClass.constants;

public class Constant {
    public static final String CREATE_CONT = "/create";
    public static final String CARS_CONT = "/cars";

    public static final String MODEL_LABEL = "model";
    public static final String COLOR_LABEL = "color";
    public static final String YEAR_LABEL = "year";

    public static final String CARS_ATTR = "cars";
    public static final String MESSAGE_ATTR = "message";

    public static final String CARS_JSP = "/cars.jsp";
    public static final String CREATE_JSP = "/create.jsp";
    public static final String INDEX_JSP = "/index.jsp";

    public static final String CAR_IS_CREATE = "Car is create!";
    public static final String CAR_IS_NOT_CREATE = "Car is not create: ";
}
