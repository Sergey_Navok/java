package by.itClass.controllers;

import by.itClass.constants.Constant;
import by.itClass.memory.CarsMemory;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "CreateCarController", urlPatterns = Constant.CREATE_CONT)
public class CreateCarController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String model = request.getParameter(Constant.MODEL_LABEL);
        String color = request.getParameter(Constant.COLOR_LABEL);
        String year = request.getParameter(Constant.YEAR_LABEL);

        RequestDispatcher rd = null;

        try {
            CarsMemory.save(model, color, year);
            request.setAttribute(Constant.MESSAGE_ATTR, Constant.CAR_IS_CREATE);
            rd = request.getRequestDispatcher(Constant.INDEX_JSP);
        } catch (NumberFormatException e) {
            request.setAttribute(Constant.MESSAGE_ATTR, Constant.CAR_IS_NOT_CREATE + e.getMessage());
            rd = request.getRequestDispatcher(Constant.CREATE_JSP);
            System.err.println(e.getMessage());
        }

        rd.forward(request, response);

    }
}
