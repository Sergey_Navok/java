<%@ page import="by.itClass.constants.Constant" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Car</title>
</head>
<body>

<h2>New Car</h2>

<c:import url="message.jsp"/>

<form method="post" action="<c:url value='<%= Constant.CREATE_CONT%>' />">
    <input type="text" name="<%= Constant.MODEL_LABEL %>">
    <input type="text" name="<%= Constant.COLOR_LABEL %>">
    <input type="text" name="<%= Constant.YEAR_LABEL %>">

    <input type="submit" value="Create">
</form>

</body>
</html>
