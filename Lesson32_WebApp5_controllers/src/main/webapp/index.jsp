<%@ page import="by.itClass.constants.Constant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IT CLASS</title>
</head>
<body>

<h2>Cars page</h2>

<c:import url="message.jsp"/>

<table>
    <th>
        <td>
            <a href="<c:url value='<%= Constant.CARS_CONT%>'/>">View all cars</a>
        </td>

        <td>
            <a href="<c:url value='<%= Constant.CREATE_JSP%>' />">Create new car</a>
        </td>
    </th>
</table>


</body>
</html>