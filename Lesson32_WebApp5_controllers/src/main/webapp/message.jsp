<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not empty message}">
    <p style="font-weight: bold; color: red">
            ${message}
    </p>
</c:if>

<c:choose>
    <c:when test="">

    </c:when>

    <c:otherwise>

    </c:otherwise>
</c:choose>
