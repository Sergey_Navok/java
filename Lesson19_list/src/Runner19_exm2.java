import by.itClass.Person;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.PriorityQueue;
import java.util.Queue;

public class Runner19_exm2 {
    public static void main(String[] args) {
        //PriorityQueue - коллекция в которой элементы сортируются по приоритетам,
        //задаваемые с помощью имплементации интерфейса Comparable
        Queue<Person> queue = new PriorityQueue<>();
        queue.add(new Person(5, "name1"));
        queue.add(new Person(1, "name2"));
        queue.add(new Person(10, "name3"));
        queue.add(new Person(2, "name4"));

        queue.stream().forEach(System.out::println);


        //ArrayDeque - определяет динамический массив, который работает по
        //принципу двунаправленной очереди
        Deque<String> deque = new ArrayDeque();
        deque.addFirst("IT");
        deque.addLast("CLASS");

        String item = deque.getLast();
        System.out.println(item);

    }
}