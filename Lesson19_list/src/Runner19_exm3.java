import by.itClass.Person;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Runner19_exm3 {
    public static void main(String[] args) {
        Person person = new Person(1, "name1");

        Set<Person> set1 = new HashSet<>();
        set1.add(person);
        set1.add(new Person(7, "name2"));
        set1.add(new Person(10, "name3"));
        set1.add(new Person(13, "name4"));

        System.out.println(set1);
        person.setId(42);
        set1.remove(person);
        System.out.println(set1);


        Set<Person> set2 = new TreeSet<>();
        set2.add(person);
        set2.add(new Person(7, "name2"));
        set2.add(new Person(10, "name3"));
        set2.add(new Person(13, "name4"));

        System.out.println("\n");

        set2.forEach(System.out::println);

    }
}
