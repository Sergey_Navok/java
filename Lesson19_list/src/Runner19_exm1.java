import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class Runner19_exm1 {
    public static void main(String[] args) {
        //LinkedList может работать по принципу очереди
        Queue<String> queue1 = new LinkedList();
        queue1.add("String");

        //LinkeList может работать по принципу двунаправленой очереди,
        //т.е. может добавлять/удалять элементы как в начало, так и в конец
        Deque<String> deque1 = new LinkedList<>();
        deque1.addFirst("IT");
        deque1.addLast("CLASS");

        System.out.println(deque1);





    }
}
