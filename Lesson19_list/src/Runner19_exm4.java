import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

public class Runner19_exm4 {
    public static void main(String[] args) {
        Map<Integer, String> map1 = new Hashtable<>();
        //Все ключи должны быть уникальны
        map1.put(4, "CLASS");//Entry
        map1.put(2, "IT");
        map1.put(1, "JAVA");
        String oldValue = map1.put(1, "JAVA 1.8");
        System.out.println("oldValue: " + oldValue);

        System.out.println(map1);

        Set<Map.Entry<Integer, String> > entries = map1.entrySet();

        for (Map.Entry<Integer, String> entry : entries) {
            Integer key = entry.getKey();
            String value = entry.getValue();

            System.out.printf("key: %d, value: %s\n", key, value);
        }

        Set<Integer> keys =  map1.keySet();
        System.out.println(keys);

        Collection<String> values = map1.values();
        System.out.println(values);

    }
}
