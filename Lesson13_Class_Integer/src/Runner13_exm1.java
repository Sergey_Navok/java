import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Runner13_exm1 {
    public static void main(String[] args) {
        String[] arr = {"Java", "", "IT", "", "Class", "Web", null, "", "", "CORE", ""};

        Predicate<String> isNull = (str) -> str != null;
        Predicate<String> isNull2 = Objects::isNull;

        Arrays.stream(arr)
                .filter(isNull);

        Arrays.stream(arr)
                .filter(str -> str != null);

        //:: - оператор взятия ссылки на метод
        //Взять ссылку можно только на тот метод, который
        //схож по сигнатруе (набор параметров и возвращаемый
        //тип с методом функционального интерфейса)
        Arrays.stream(arr)
                .filter(Objects::nonNull);

        ArrayList list1 = Arrays.stream(arr)
                .filter(Objects::nonNull)
                .filter(Runner13_exm1::nonEmptyString)//взятие ссылки на static метод
                .collect(Collectors.toCollection( () -> new ArrayList<>() ));

        System.out.println(list1);

        ArrayList list2 = Arrays.stream(arr)
                .filter(Objects::nonNull)
                .filter(Runner13_exm1::nonEmptyString)//взятие ссылки на static метод
                .collect(Collectors.toCollection(ArrayList::new));

        System.out.println(list2);

        Supplier<ArrayList> supplier1 = () -> new ArrayList();
        Supplier<String> supplier2 = () -> new String();
    }

    //Static method
    private static boolean nonEmptyString(String str) {
        return !str.isEmpty();
    }


}
