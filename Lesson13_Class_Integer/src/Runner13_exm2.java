public class Runner13_exm2 {
    public static void main(String[] args) {
        //Классы оболчки
        Integer iObj1 = new Integer(1);//Deplicated (не советуется к применению)
        //Автоупаковка (Boxing)
        //1. срабатывает при присвоениипримитива ссылочной переменной
        Integer iObj2 = 101;//Integer.valueOf(101) -> new Integer(101);
        doSmth(iObj1);
        //2. срабатывает при передаче примитива в параметры методы,
        //ожидающего получить объект
        doSmth(989);

        //Автораспаковка (Unboxing)
        //1. срабатывает при присвоении объекта в переменную примитивного типа
        int number1 = new Integer(200);
        int number2 = iObj1;
        //2. срабатывает при передаче объекта в параметры методы,
        //ожидающего получить примитив
        m1(new Integer(888));
        m1(iObj2);

        int result1 = 100 * number1 + iObj1 + new Integer(10) + number2;
        //Integer result2 = 100 * number1 + iObj1 + new Integer(10) + number2;

        Integer[] arr = {1, 4, 5, 6, 7};

        String numberStr = "777";
        int age = Integer.parseInt(numberStr);
        System.out.println(age);

        Integer iObj3 = Integer.valueOf(numberStr);
        System.out.println(iObj3 + ";" + iObj3.getClass());


    }

    private static void doSmth(Integer number) {
        //...
    }

    private static void m1(int number) {
        //...
    }
}
