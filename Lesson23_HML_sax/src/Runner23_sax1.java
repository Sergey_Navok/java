import by.itCalss.entity.User;
import by.itCalss.sax.Handler;
import by.itCalss.sax.UserHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;

public class Runner23_sax1 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_1.xml";

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();

            //Handler handler = new Handler();
            UserHandler handler = new UserHandler();

            parser.parse(new FileInputStream(FILE_NAME), handler);

            User user = handler.getUser();
            System.out.println(user);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

    }
}
