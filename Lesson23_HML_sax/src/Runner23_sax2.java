import by.itCalss.entity.User;
import by.itCalss.sax.ListUserHandler;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class Runner23_sax2 {
    public static void main(String[] args) {
        //XML в Java

        final String FILE_NAME = "src/itClass_2.xml";

        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();

            ListUserHandler handler = new ListUserHandler();

            parser.parse(new FileInputStream(FILE_NAME), handler);

            List<User> users = handler.getUser();
            users.forEach(System.out::println);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }


    }
}
