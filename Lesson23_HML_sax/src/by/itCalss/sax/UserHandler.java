package by.itCalss.sax;

import by.itCalss.entity.User;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UserHandler extends DefaultHandler {
    private User user;
    private UserTags tag;

    public User getUser() {
        return user;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        tag = UserTags.valueOf(qName.toUpperCase());
        if (tag == UserTags.USER) {
            user = new User();
            String id = attributes.getValue(0);
            user.setId(id);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        tag = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length).trim();

        if (!value.isEmpty()) {
            switch (tag) {
                case LOGIN: user.setLogin(value);
                case PASSWORD: user.setPassword(value);
                case EMAIL: user.setEmail(value);
            }
        }

    }



}
