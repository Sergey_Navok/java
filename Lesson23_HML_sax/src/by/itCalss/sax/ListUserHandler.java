package by.itCalss.sax;

import by.itCalss.entity.User;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class ListUserHandler extends DefaultHandler {
    private List<User> users;
    private User user;
    private UserTags tag;

    public User getUser() {
        return user;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        tag = UserTags.valueOf(qName.toUpperCase());
        if (tag == UserTags.USERS) {
            users = new ArrayList<>();
        }
        if (tag == UserTags.USER) {
            user = new User();
            user.setId(attributes.getValue(0));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        UserTags endTag = UserTags.valueOf(qName.toUpperCase());
        if (endTag == UserTags.USER) {
            users.add(user);
            user = null;
        }
        tag = null;

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length).trim();

        if (!value.isEmpty()) {
            switch (tag) {
                case LOGIN: user.setLogin(value);
                case PASSWORD: user.setPassword(value);
                case EMAIL: user.setEmail(value);
            }
        }

    }
    }
}
