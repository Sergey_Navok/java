public class Runner02_task6 {
    public static void main(String[] args) {
        int size1 = 5;
        int size2 = 5;
        int i = 0;
        int j = 0;
        int sum = 0;
        int count = 0;
        double avg = 0;
        int[][] array =  new int[size1][size2];

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                array[i][j] = (int) ((Math.random() * 50) - 26);
            }
        }

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                System.out.printf("[%d]", array[i][j]);
            }
            System.out.printf("\n");
        }

        int j2 = 1;
        for (i = 0; i < size1; i++) {
            for (j = j2; j < size2; j++){
                sum += array[i][j];
                count ++;
            }
            j2++;
        }

        avg = (double) sum / count;
        System.out.println("Среднее арифметическое всех чисел НАД главной диагональю: " + avg);

        sum = 0;
        count = 0;
        j2 = size1 - 1;
        for (i = 0; i < j2; i++) {
            for (j = 0; j < size2; j++){
                sum += array[i][j];
                count ++;
            }
            j2--;
        }

        avg = (double) sum / count;
        System.out.println("Среднее арифметическое всех чисел НАД побочной диагональю: " + avg);


    }
}
