import java.util.Arrays;
import java.util.Scanner;

public class Runner02_task4 {
    public static void main(String[] args) {
        int size1 = 3;
        int size2 = 4;
        int s = 0;
        int k = 0;
        int[][] array =  new int[size1][size2];

        Scanner userEnter = new Scanner(System.in);

        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++){
                System.out.printf("Введите [%d][%d] элемент массива: ", i+1, j+1);
                array[i][j] = userEnter.nextInt();
            }
        }

        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++){
                System.out.printf("[%d]", array[i][j]);
            }
            System.out.printf("\n");
        }

        System.out.printf("Введите какую строку заменить на 5: ");
        s = userEnter.nextInt();
        s -= 1;

        if (s >= 0 && s < size1) {
            for (int j = 0; j < size2; j++){
                array[s][j] = 5;
            }
        }

        System.out.printf("Введите какой столбец заменить на 10: ");
        k = userEnter.nextInt();
        k -= 1;

        if (k >= 0 && k < size2) {
            for (int i = 0; i < size1; i++){
                array[i][k] = 10;
            }
        }

        userEnter.close();

        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++){
                System.out.printf("[%d]", array[i][j]);
            }
            System.out.printf("\n");
        }

    }
}
