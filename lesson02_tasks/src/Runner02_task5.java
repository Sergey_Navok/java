public class Runner02_task5 {
    public static void main(String[] args) {
        int size1 = 5;
        int size2 = 5;
        int i = 0;
        int j = 0;
        int sum = 0;
        int max = 0;
        int min = 0;
        int[][] array =  new int[size1][size2];

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                array[i][j] = (int) ((Math.random() * 10) - 5);
            }
        }

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                System.out.printf("[%d]", array[i][j]);
            }
            System.out.printf("\n");
        }

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                sum += array[i][j];
            }
        }

        System.out.println("Сумма всех элементов массива: " + sum);

        sum = 0;
        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                sum += array[i][j];
            }
            System.out.printf("Сумма всех элементов %d строки: %d\n", i+1, sum);
            sum = 0;
        }

        for (j = 0; j < size1; j++) {
            for (i = 0; i < size2; i++){
                sum += array[i][j];
            }
            System.out.printf("Сумма всех элементов %d столбца: %d\n", j+1, sum);
            sum = 0;
        }

        //Здесь завел дополнительные перменные для того чтобы запомнить индексы
        //макс и мин числа
        int i1 = 0;
        int i2 = 0;
        int j1 = 0;
        int j2 = 0;
        max = array[0][0];
        min = array[0][0];
        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                    i1 = i;
                    j1 = j;
                }

                if (array[i][j] < min) {
                    min = array[i][j];
                    i2 = i;
                    j2 = j;
                }
            }
        }

        //Поменял местами макс и мин число
        array[i1][j1] = min;
        array[i2][j2] = max;

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                System.out.printf("[%d]", array[i][j]);
            }
            System.out.printf("\n");
        }

        //Занулил дополнительные переменные для индексов
        i1 = 0;
        i2 = 0;
        j1 = 0;
        j2 = 0;
        min = 0;

        //Бежим по массиву, если в нем есть 0 или отрицательные значения - запоминаем расположение элемента
        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++) {
                if (array[i][j] == 0) {
                    i1 = i;
                    j1 = j;
                }

                if (array[i][j] < min) {
                    min = array[i][j];
                    i2 = i;
                    j2 = j;
                }
            }
            //Если какой-то из индексов изменился, то меняем местами отрицательное значение и 0
            if ((i1 > 0 || j1 > 0) && (i2 > 0 || j2 > 0)) {
                array[i1][j1] = min;
                array[i2][j2] = 0;
                System.out.printf("В строке %d были заменены элементы!\n", i+1);
            } else {
                System.out.printf("Заданных элементов в строке %d нет!\n", i+1);
            }

            i1 = 0;
            i2 = 0;
            j1 = 0;
            j2 = 0;
            min = 10;
        }

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                System.out.printf("[%d]", array[i][j]);
            }
            System.out.printf("\n");
        }

    }
}
