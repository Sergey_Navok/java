import java.util.Arrays;
import java.util.Scanner;

public class Runner02_task3 {
    public static void main(String[] args) {
        int[] array = null;
        int i; //Завел переменную i - потому что в коде используется несколько раз
        int counter = 0;
        int sum = 0;
        double avg = 0;

        Scanner userEnter = new Scanner(System.in);
        System.out.print("Введите размерность массива: ");
        int size = userEnter.nextInt();

        array = new int[size];

        for(i = 0; i < size; i++) {
            System.out.printf("Введите %d) элемент массива: ", i+1);
            array[i] = userEnter.nextInt();
        }
        userEnter.close();

        System.out.println(Arrays.toString(array));

        //По условию задачи сказано: вывести массив в обратном порядке, а не перезаписать/создать новый массив
        //Решил выводить в стоку - потому что так читабельнее. Поэтому используется if/else для определения
        //последнего элемента массива
        for (i = size - 1; i >= 0; i--) {
            if (i != 0) {
                System.out.printf("%d, ", array[i]);
            } else {
                System.out.printf("%d", array[i]);
            }
        }

        //Определение среднего арифметического кратного 5
        for (i = 0; i < size; i++) {
            if (array[i] % 5 == 0 && (array[i] > 0 || array[i] < 0)) {
                sum += array[i];
                counter ++;
            }
        }

        if (sum == 0) {
            System.out.println("\nМассив не содержит чисел кратных 5");
        } else {
            avg = (double) sum / counter;
            System.out.println("\nСреднее арифметическое всех чисел кратных 5 равно: " + avg);
        }

        //Определение среднего арифметического не кратных 10
        sum = 0;
        counter = 0;
        for (i = 0; i < size; i++) {
            if (array[i] % 10 != 0 && (array[i] > 0 || array[i] < 0)) {
                sum += array[i];
                counter ++;
            }
        }

        if (sum == 0) {
            System.out.println("\nМассив содержит только числа кратные 10");
        } else {
            avg = (double) sum / counter;
            System.out.println("\nСреднее арифметическое всех чисел не кратных 10 равно: " + avg);
        }

        //Создание нового массива из элементов старого
        int[] array2 = new int[array.length];
        counter = 0;

        for (i = 0; i < size; i++) {
            int j = 0;
            if (array[i] % 3 == 0 && array[i] > 0) {
                array2[j] = array[i] * -1;
                j++;
                counter ++;
            }

            if (array[i] % 2 == 0 && array[i] > 0) {
                array2[j] = array[i];
                j++;
                counter ++;
            }
        }

        array2 = Arrays.copyOf(array2, counter);

        System.out.println(Arrays.toString(array2));

        Arrays.sort(array2);

        System.out.println(Arrays.toString(array2));

    }
}
