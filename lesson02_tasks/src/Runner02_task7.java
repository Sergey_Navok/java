import java.util.Arrays;

public class Runner02_task7 {
    public static void main(String[] args) {
        int size1 = 7;
        int size2 = 7;
        int i = 0;
        int j = 0;
        int sum = 0;
        int count = 0;
        double avg = 0;
        int[][] array =  new int[size1][size2];
        double[] array2 = new double[size2];
        int[] array3 = new int[size2];
        int[] array4 = new int[size1];

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                array[i][j] = (int) ((Math.random() * 150) - 51);
            }
        }

        for (i = 0; i < size1; i++) {
            for (j = 0; j < size2; j++){
                System.out.printf("[%d]", array[i][j]);
            }
            System.out.printf("\n");
        }

        for (j = 0; j < size1; j++) {
            for (i = 0; i < size2; i++){
                sum += array[i][j];
                count ++;
            }
            avg = (double) sum / count;
            array2[j] = avg;
            sum = 0;
            count = 0;
        }
        System.out.println("Массив из среднего арифметического столбцов: " + Arrays.toString(array2));

        for (j = 0; j < size1; j++) {
            for (i = 0; i < size2; i++){
                sum += array[i][j];
            }
            array3[j] = sum;
            sum = 0;
        }
        System.out.println("Массив из суммы элементов столбцов: " + Arrays.toString(array3));

        for (i = 0; i < size1; i++) {
            sum = array[i][0];
            count = array[i][size1 - 1];
            array4[i] = sum * count;
        }
        System.out.println("Массив из произведения элементов строк: " + Arrays.toString(array4));

    }
}
