import java.util.Arrays;

public class Runner02_task1 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, -3, 5, -5, 7};
        int evenSum = 0;
        int oddSum = 0;
        int maxNegative = 0;
        int indexNegative = 0;

        System.out.println(Arrays.toString(array));

        for (int value : array) {
            if (value % 2 != 0) {
                oddSum += value;
            } else {
                evenSum += value;
            }
        }

        System.out.printf("Сумма всех четных чисел в массиве равна: %d", evenSum);
        System.out.printf("\nСумма всех нечетных чисел в массиве равна: %d", oddSum);

        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0 || array[i] < maxNegative) {
                maxNegative = array[i];
                indexNegative = i;
            }
        }

        if (maxNegative < 0) {
            System.out.printf("\nНаименьшее из отрицательных значений: %d, находится по индексу: %d", maxNegative, indexNegative);
        } else {
            System.out.println("\nМассив не содержит отрицательных чисел!");
        }

    }
}
