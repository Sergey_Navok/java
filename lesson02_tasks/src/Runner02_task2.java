import java.util.Arrays;

public class Runner02_task2 {
    public static void main(String[] args) {
        double[] array = {-4, -3, -2, -1, 0, 1, 2, 3, 4, 5};

        System.out.println(Arrays.toString(array));

        for (int i = 0; i<array.length; i++) {
            if (array[i] >= 0) {
                array[i] /= 2;
            } else {
                array[i] = i;
            }
        }

        System.out.println(Arrays.toString(array));

    }
}
