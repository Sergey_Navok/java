import by.itClass.Figure;
import by.itClass.Rectangle;

public class Runner06_exm2 {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(18, 7);
        System.out.println(rectangle1);

        Rectangle rectangle2 = new Rectangle(19, 21, "White");
        System.out.println(rectangle2);

        System.out.println("Rectangle: " + rectangle2.draw());

        Figure figure = new Rectangle();
        System.out.println("Figure: " + figure.draw());

    }
}
