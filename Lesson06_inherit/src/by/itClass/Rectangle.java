package by.itClass;

//extends задает наследование классов
public class Rectangle extends Figure {
    private double width;
    private double height;

    public Rectangle() {

    }

    public Rectangle(double width, double height) {
        //Конструкция this() вызывает конструктор текущего класса,
        //который соответствует количству и типу параметров в this()
        //Использется только в конструкторе
        this();
        this.width = width;
        this.height = height;
    }


    public Rectangle(double width, double height, String color) {
        //Конструкция super() вызывает конструктор родительского класса
        //при этом будет вызван тот конструктор, который соответствует
        //колличеству и типу переданных параметров в super
        //Использется только в конструкторе
        super(color);
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    //Переопределение метода draw() нельзя изменять типы, количество
    //параметров, возвращаемый тип метода, нельзя сужать модификатор
    //доступа, но можно расширить модификатор доступа (есть нюанс с Exception)
    @Override
    public String draw() {
        return "Rectangle";
    }

    @Override
    public String toString() {
        //super - является псевдоссылкой на родительский класс
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                ", color=" + super.toString() +
                '}';
    }
}
