package by.itClass;

public class Figure {
//public final class Figure { //final - запрещает наследование от класса
    protected String color;
    //protected final String color; //final - для поля своего рода константа

    public Figure() {
    }

    public Figure(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    //protected String draw() {
    //public final String draw() { //final - запрещает переопределение методов
    public String draw() {
        return "Figure";
    }

    //Абстрактный метод позволяет реализовать механизм познего связывания
    //public abstract String doSmth();

    @Override
    public String toString() {
        return "Figure{" +
                "color='" + color + '\'' +
                '}';
    }
}
