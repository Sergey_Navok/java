import by.itClass.Figure;
import by.itClass.Rectangle;

public class Runner06_exm1 {
    public static void main(String[] args) {
        Figure figure1 = null;
        figure1 = new Figure();
        Rectangle rectangle1 = new Rectangle();
        //Можно приводить наследника к родителю
        figure1 = (Figure) rectangle1;
        figure1 = new Rectangle();

        //Нельзя делать приведение типов от родителя к наследнику
        rectangle1 = (Rectangle) new Figure();//Будет ошибка на этапе компиляции

        //Разрешено приводить тип родителя к наследнику только в одном случае:
        //если до этого было приведение от наследника к родителю!
        rectangle1 = (Rectangle) figure1;

    }
}
