import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Runner14_exm2_InputStream {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/inputStream.txt";

        InputStream fileIn = new FileInputStream(FILE_NAME);
        int byteFromFile = fileIn.read();//1 байт -> int; -1 - если нечего считывать
        System.out.println("byte:" + byteFromFile);
        char ch = (char) byteFromFile;
        System.out.println("char:" + ch);

        //Чтение данных из файла
        int bytes = -1;
        while ( (bytes = fileIn.read()) != -1){
            ch = (char) bytes;
            System.out.print(ch);
        }

    }
}
