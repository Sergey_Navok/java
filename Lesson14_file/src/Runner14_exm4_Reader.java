import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Runner14_exm4_Reader {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/reader.txt";

        Reader reader = new FileReader(FILE_NAME);
        int byteFromFile = reader.read();

        System.out.println("byteFromFile:" + byteFromFile);
        System.out.println("char:" + (char) byteFromFile);
        reader.close();



    }
}
