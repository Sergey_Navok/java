import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Runner14_exm3 {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/myFile.txt";
        //Класс File используется для пресдтавления файлов/папок
        //в виде java-объектов и последующей их обработки
        File myFile = new File(FILE_NAME);

        //
        boolean isExist = myFile.exists();
        System.out.println("isExist:" + isExist);

        boolean isCreate = myFile.createNewFile();
        System.out.println("isCreate:" + isCreate);

        OutputStream outFile = new FileOutputStream(myFile);
        outFile.write("Hello".getBytes());
        outFile.close();

        File myDir = new File("src/mydir");
        myDir.mkdir();

    }
}
