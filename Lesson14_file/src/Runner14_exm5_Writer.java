import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Runner14_exm5_Writer {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/writer.txt";
        Writer writer = new FileWriter(FILE_NAME);
        writer.write("Привет! Hello JAVA!");
        writer.close();
    }
}
