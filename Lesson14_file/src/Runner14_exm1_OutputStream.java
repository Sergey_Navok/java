import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

public class Runner14_exm1_OutputStream {
    public static void main(String[] args) throws IOException {
        //Работа с файлами. Чтение и запись данных в файле
        final String FILE_NAME = "src/outputStream.txt";
        //final String FILE_NAME2 = "D://..............";
        System.out.println("IT Class");
        System.err.println("Java");

        OutputStream fileOut = null;
        fileOut = new FileOutputStream(FILE_NAME, true);//поставить true чтобы файл не перезаписывался
        //OutputStream fileOut = new FileOutputStream(FILE_NAME);

        //Запись данных в файл
        String message = "\nHello JAVA from IT Class";
        byte[] messageBytes = message.getBytes();
        System.out.println(Arrays.toString(messageBytes));

        fileOut.write(messageBytes);
        fileOut.write("\nПривет".getBytes());
        fileOut.close();
    }
}
