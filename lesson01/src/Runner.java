public class Runner {
    //В классе может быть создан только один метод main с однотипной сигнатурой (модификатор доступа, параметры и т.д.)
    public static void main(String[] args) { //Метод main является точкой входа в программу, автогенерация main => "tab"
        //Метод print выводит в консоль сообщение без перевода каретки на новую строку
        System.out.print("Hello World from IT Class!");
        System.out.println("Hello World from IT Class!");
        //Метод println выводит в консоль сообщение c переводом каретки на новую строку
        System.out.print("Hello World from IT Class!");
        //Форматированный вывод
        //%d - используется для int
        //%f - используется для double
        //%s - используется для символов
        System.out.printf("\nInt value: %d, %d", 82, 15);
        System.out.printf("\nFloat value: %.3f", 7.9315);
        System.out.printf("\nString value: %s", "Hello");
    }
}
