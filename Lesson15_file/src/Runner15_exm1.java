import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Runner15_exm1 {
    public static void main(String[] args) throws IOException {
        //Класс Scanner
        final String FILE_NAME = "src/itclass_01.txt";
        InputStream in = new FileInputStream(FILE_NAME);

        //System.out.println((char) in.read());
        //System.out.println((char) in.read());

        //char ch1 = (char) in.read();
        //char ch2 = (char) in.read();

        Scanner sc = new Scanner(in);
        sc.useDelimiter(";");

        if (sc.hasNextInt()) {
            int num = sc.nextInt();
            System.out.println("number:" + num);
        } else {
            String strNum = sc.next();
            System.out.println(strNum);
        }

        String str = sc.next();
        System.out.println(str);

        double dub = sc.nextDouble();
        System.out.println(dub);

    }
}
