import by.itClass.Person;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class Runner15_exm3 {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/itclass_03.txt";
        PrintStream ps = new PrintStream(new FileOutputStream(FILE_NAME));

        Person alex = new Person(1, "Alex", 21, 185.4);

        ps.println(alex);
        ps.printf("Name:%s, age:%d", alex.getName(), alex.getAge());
    }
}
