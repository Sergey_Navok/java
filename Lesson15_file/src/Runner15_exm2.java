import by.itClass.Person;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Runner15_exm2 {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "src/itclass_02.txt";
        final String DELIMITER = ";";
        Person[] persons = new Person[3];
        int i = 0;

        Scanner sc = new Scanner(new FileReader(FILE_NAME));

        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] values = line.split(DELIMITER);
            int id = Integer.valueOf(values[0]);
            String name = values[1];
            int age = Integer.valueOf(values[2]);
            double height = Double.valueOf(values[3]);

            Person person = new Person(id, name, age, height);
            persons[i++] = person;
        }

        System.out.println(Arrays.toString(persons));

    }
}
