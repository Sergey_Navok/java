package by.itClass;

public class Person {
    private int id;
    private String name;
    private int Age;
    private double height;

    public Person() {
    }

    public Person(int id, String name, int age, double height) {
        this.id = id;
        this.name = name;
        Age = age;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", Age=" + Age +
                ", height=" + height +
                '}';
    }
}
