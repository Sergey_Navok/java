import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Runner26_exm1 {
    public static void main(String[] args) {
        final String DB_USER = "root";
        final String DB_PASSWORD = "";
        final String DB_URL = "jdbc:mysql://localhost/itclass?serverTimezone=Europe/Minsk";


        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection cn = null;
        Statement st = null;//PreparedStatement - используется параметризированные параметры

        try {
            cn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            st = cn.createStatement();
            st.executeUpdate("insert into users(login, password) values ('test', 't123')");

            //st.executeQuery();//Описывает результирующую таблицу

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }



    }
}