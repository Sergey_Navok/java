import java.sql.*;

public class Runner27_exm2 {
    public static void main(String[] args) {
        //Минимальный набор параметров для подключения к БД через JDBC
        //JDBC - java data base conectity
        final String DB_USER = "root";
        final String DB_PASSWORD = "";
        //Любой URL для подключения к БД через JDBC должен начинаться со слова "jdbc",
        //а далее нужно обязательно указать БД, с которой будем работать,
        //чтобы подтянуть правильный драйвер
        final String DB_URL = "jdbc:mysql://localhost/itclass?serverTimezone=Europe/Minsk";


        try {
            //Явная загрузка класса драйвера для подключения а БД в память JVM
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection cn = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            //Получение подключения к БД по заданному URL
            cn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            //Statement используется для работы с SQL запросами
            st = cn.createStatement();
            rs = st.executeQuery("select * from users");

            //Результат исполнения select запроса приходит в виде результирующей таблицы,
            //которая описывается объектом ResultSet. Метод next() проверяет наличие очередной
            //в результирующую таблицу и перевод ResultSet на новую строку из которой можно
            //получить данные по названию столца или его номеру (нумерация начинаеся с 1)
            while (rs.next()) {
                int id = rs.getInt("id");
                int id2 = rs.getInt(1);
                String login = rs.getString("login");
                System.out.printf("id: %d, login: %s\n", id, login);
                //Далее созадем объект
                //User user = new User(id, login);
                //ORM-библиотеки позволяют сопоставлять поля с теми столбцами, что уже есть
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (st != null) {
                try {
                    st.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }




    }
}