import java.sql.*;
import java.util.Arrays;
import java.util.List;

public class Runner27_exm1 {
    public static void main(String[] args) {
        final String DB_USER = "root";
        final String DB_PASSWORD = "";
        final String DB_URL = "jdbc:mysql://localhost/itclass?serverTimezone=Europe/Minsk";


        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        List<String> logins = Arrays.asList(
                "login1",
                "login2",
                "login3",
                "login4"
        );

        Connection cn = null;
        //PreparedStatement - работает с параметризированными запросами
        PreparedStatement pst = null;

        try {
            cn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            pst = cn.prepareStatement("insert into users(login) values(?)");

            for (String login : logins) {
                System.out.println("old:" + pst);
                pst.setString(1, login);
                System.out.println("new:" + pst);

                pst.executeUpdate();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }

            if (cn != null) {
                try {
                    cn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }




    }
}
