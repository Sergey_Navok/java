import java.util.ArrayList;
import java.util.List;

public class Runner18_exm1 {
    public static void main(String[] args) {
        //Collection API

        List<String> names = new ArrayList<>();

        System.out.println(names);

        names.add("CLASS");
        System.out.println(names);

        names.add(0, "IT");
        System.out.println(names);

        //ArrayList<String> list2 = new ArrayList<>();//Менее рационален, т.к. лучше использовать интерфейс родителя

        String item = names.get(0);
        System.out.println(item);

        names.remove("IT");

        //list -> isRemove



    }
}
