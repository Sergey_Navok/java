import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Runner21_task4 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_task4.txt";
        StringBuilder strBuilder = new StringBuilder();

        try (InputStream inputStream = new FileInputStream(FILE_NAME);
             Scanner scanner = new Scanner(inputStream)) {

            while (scanner.hasNextLine()) {
                String str = scanner.nextLine();
                String[] chars = str.split(" ");
                int a = Integer.parseInt(chars[0]);
                String operator = chars[1];
                int b = Integer.parseInt(chars[2]);

                int result = Calc(a, operator, b);
                strBuilder.append("result(")
                        .append(a)
                        .append(operator)
                        .append(b)
                        .append(") = ")
                        .append(result);
                System.out.println(strBuilder);
                strBuilder = new StringBuilder();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    private static int Calc(int a, String operator, int b) {
        int result = 0;
        switch (operator) {
            case ("+"):
                result = a + b;
                break;

            case ("-"):
                result = a - b;
                break;

            case ("/"):
                result = a / b;
                break;

            case ("*"):
                result = a * b;
                break;
        }

        return result;
    }
}
