import java.io.*;
import java.util.Scanner;

public class Runner21_task3 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_task3.txt";
        StringBuilder strBuilder = new StringBuilder();

        try (InputStream inputStream = new FileInputStream(FILE_NAME);
             Scanner scanner = new Scanner(inputStream)) {

            String[] words = scanner.nextLine().split(" ");

            for (String word : words) {
                if (word.length() % 2 == 0) {
                    strBuilder.append(word).append(" ");
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(strBuilder);
        System.out.println("Длина strBuilder: " + strBuilder.length());
        System.out.println("Вместимость strBuilder: " + strBuilder.capacity());
        System.out.println(strBuilder.reverse());
    }
}