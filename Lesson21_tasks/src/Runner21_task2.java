public class Runner21_task2 {
    public static void main(String[] args) {
        StringBuilder strBuilder = new StringBuilder
                ("Lorem ipsum dolor sit amet, consectetur test adipiscing elit, sed eiusmod tempor");

        System.out.println(strBuilder);

        strBuilder.trimToSize();

        String str = strBuilder.toString();
        String[] words = str.split(" ");
        String minWord = words[0];
        String maxWord = words[0];

        for (int i = 1; i < words.length; i++) {
            if (words[i].length() <= minWord.length()) {
                minWord = words[i];
            }

            if (words[i].length() > maxWord.length()) {
                maxWord = words[i];
            }
        }

        System.out.println("Первое длинное слово: " + maxWord);
        System.out.println("Последнее короткое слово: " + minWord);

        strBuilder.delete(strBuilder.indexOf(maxWord), strBuilder.indexOf(maxWord) + maxWord.length() + 1);
        System.out.println(strBuilder);

        strBuilder.delete(strBuilder.indexOf(minWord), strBuilder.indexOf(minWord) + minWord.length() + 1);
        System.out.println(strBuilder);

    }
}
