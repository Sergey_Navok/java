public class Runner21_task1 {
    public static void main(String[] args) {
        StringBuilder strBuilder = new StringBuilder();
        String str = "Lorem ipsum dolor sit amet, consectetur test adipiscing elit, sed eiusmod tempor";
        String[] words = str.split(" ");
        String minLengthWord = words[0];

        for (int i = 1; i < words.length; i++) {
            if (minLengthWord.length() > words[i].length()) {
                minLengthWord = words[i];
            }
        }

        System.out.println(str);
        System.out.println("Самое короткое слово:" + minLengthWord);

        char[] charsMinLengthWord = minLengthWord.toCharArray();
        String lastLetter = String.valueOf(charsMinLengthWord[charsMinLengthWord.length - 1]);
        System.out.println("Последняя буква самого короткого слова: " + lastLetter);

        String regex = lastLetter + "[\\w]+";

        for (String value : words) {
            if (value.matches(regex)) {
                strBuilder.append(value).append(" ");
            }
        }

        System.out.println("Слова, которые начинаются с последней буквы самого короткого слова:");
        System.out.println(strBuilder);
        System.out.println("Длина strBuilder: " + strBuilder.length());
        System.out.println("Вместимость strBuilder: " + strBuilder.capacity());
    }
}