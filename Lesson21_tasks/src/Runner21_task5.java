import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Runner21_task5 {
    public static void main(String[] args) {
        final String FILE_NAME = "src/itClass_task5.txt";
        final String DELIMITER = ";";
        int errLines = 0;
        StringBuilder strBuilder = new StringBuilder();
        List<Double> buffer = new ArrayList<>();

        try (InputStream inputStream = new FileInputStream(FILE_NAME);
             Scanner scanner = new Scanner(inputStream)) {

            while (scanner.hasNextLine()) {
                String str = scanner.nextLine();
                String[] chars = str.split(DELIMITER);

                try {
                    int a = Integer.parseInt(chars[0]);
                    double b = Double.parseDouble(chars[a]);

                    buffer.add(b);
                } catch (ArrayIndexOutOfBoundsException | NullPointerException | NumberFormatException e) {
                    errLines++;
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        double result = buffer.get(0) + buffer.get(1) + buffer.get(2);

        strBuilder.append("result(")
                .append(buffer.get(0)).append(" + ")
                .append(buffer.get(1)).append(" + ")
                .append(buffer.get(2)).append(") = ")
                .append(result);

        System.out.println(strBuilder);
        System.out.println("Error lines = " + errLines);

    }
}