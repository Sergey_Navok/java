public class Runner03_exm2 {
    public static void main(String[] args) {
        //Класс String - работа со строками
        System.out.println("Hello World!");
        String str1 = null; //null - значение по умолчанию для ссылочных типов
        //Значение созданного оъекта строки изменить нельзя. Любое изменение ведет к созданию
        //нового объекта со значением результата изменения
        str1 = new String("IT CLASS!");//Конструктор с одним параметром
        System.out.println(str1);

        //Использование других конструкторов для создания другой строки
        //Конструктор по умолчанию создает пустую строку
        String str2 = new String();
        System.out.println("str2 = " + str2);
        char[] chars = {'J', 'A', 'V', 'A'};
        str2 = new String(chars);
        System.out.println("str2 = " + str2);
        //byte[] -> code ASCII
        //str2 = new String(byte[], from, len)

        String str3 = "JAVA";
        String str4 = "JAVA";

        //Для ссылочных типов == выполняет сравнение ссылок, а не значение!!!
        boolean isEqual = (str2 == str3);
        System.out.println("str1==str2: " + isEqual);
        isEqual = (str3 == str4);
        System.out.println("str3==str4: " + isEqual);

        System.out.println(str3.hashCode());
        //str3 = null;
        str4 = null;
        System.runFinalization();// Принудительный запуск сборщика мусора, но не всегда используется. Не желательно использовать

        String str5 = "JAVA";
        System.out.println("str5= " + str5);
        System.out.println(str5.hashCode());

        //Сравнение объектов строки по значению
        isEqual = str2.equals(str3);
        System.out.println("str2.equals(str3): " + isEqual);
        str3 = "jAvA";
        System.out.println("str2.equals(str3): " + str2.equalsIgnoreCase(str3));

    }
}
