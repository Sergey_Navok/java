import java.util.Arrays;

public class Runner03_exm1 {
    public static void main(String[] args) {
        int[] arr1 = {1, 6, 8, 2, 10, -5, -1, 17};

        //Методы утилитного класса Arrays
        System.out.println(arr1);
        //hashCode() -> native -> C++;

        //Метод toString представляет массив в виде строки
        System.out.println(Arrays.toString(arr1));

        //Метод copyOf() создает копию массива с указанной размерностью
        int[] arr2 = Arrays.copyOf(arr1, 25);
        System.out.println(Arrays.toString(arr2));

        //Метод equals() сравнивает на равенство два массива с точки зрения содержимого
        //и вернет true если наборы и порядок содержимого совпадает
        int temp = arr1[1];
        arr1[1] = arr1[4];
        arr1[4] = temp;
        boolean isArrEqual1 = Arrays.equals(arr1, arr2);
        System.out.println("isArrEquals = " + isArrEqual1);

        //Arrays.copyOfRange()

        //Двемерный массив
        int[][] arr3 =  new int[2][2];
        arr3[0][0] = 1;
        arr3[1][1] = 10;
        System.out.println(Arrays.toString(arr3));

        //Метод sort() сортирует массив чисел по возрастанию
        Arrays.sort(arr1);
        System.out.println(Arrays.toString(arr1));
    }
}
